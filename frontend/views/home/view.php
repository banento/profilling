<?php 

use yii\helpers\Url;
use yii\web\View;
use common\components\Helper;

?>


  <!-- Content Wrapper. Contains page content -->
  <!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->
   <div class="content-header" style="padding-top:60px !important">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title"><?= $this->title ?></h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="/home"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item active" aria-current="page"><?= $news_title ?></li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>  

    <!-- Main content -->
    <section class="content" style="padding-top:20px !important">
      <div class="container">
      <div class="row">
        <div class="col-12">

    	<div class="box">
			<div class="box-header with-border">
			  <h4 class="box-title"><?= $news_title ?></h4>			 
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-sm-3">
						<?php if($news_img): ?>
							<img src="<?= $news_img?>" class="img-fluid" alt="" /></div>
						<?php else: ?>
						<img src="https://cdn.pixabay.com/photo/2017/06/10/07/22/news-2389226_1280.png" class="img-fluid" alt="" /></div>
						<?php endif; ?>
					<div class="col-sm-9">
						<div id="news_content">
							<?= $news_content?>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- Lorem, ipsum dolor sit amet consectetur adipisicing elit. A possimus harum excepturi alias porro tenetur, natus nobis qui neque officiis sint, vitae odit? Molestias quibusdam voluptatem, modi in, consequuntur unde? -->
				</div>
			</div>
			<!-- /.box-body -->
		</div>

          
        </div>
        <!-- /.col-->
      </div>
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->



<input type="hidden" name="base_url" id="base_url" value="<?= Url::base(true) ?>/">


	<!-- popper -->
	<?php $this->registerJsFile('@web/assets/vendor_components/popper/dist/popper.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Bootstrap 4.0-->
	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<?php $this->registerJsFile('@web/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
	<?php $this->registerJsFile('@web/js/news_view.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	

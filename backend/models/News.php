<?php

namespace backend\models;

use Yii;
use yii\db\Query;
use yii\db\Expression;


/**
 * This is the model class for table "news".
 *
 * @property int $id_news
 * @property string $news_id
 * @property string|null $news_title
 * @property string|null $unique_id_msg
 * @property string|null $news_content
 * @property string|null $news_tags
 * @property string|null $created_date
 * @property string|null $created_by
 * @property string|null $created_ip
 * @property string|null $updated_date
 * @property string|null $updated_by
 * @property string|null $updated_ip
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id'], 'required'],
            [['created_date', 'updated_date'], 'safe'],
            [['created_by', 'updated_by'], 'string', 'max' => 25],
            [['news_title'], 'string', 'max' => 150],
            [['news_tag','news_img'], 'string', 'max' => 250],
            [['news_content'], 'string'],
            [['created_ip'], 'string', 'max' => 20],
            [['news_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'News Id',
            'news_title' => 'News Title',
            'news_content' => 'News Content',
            'news_tag' => 'News Tags',
            'news_img' => 'News Img',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'updated_date' => 'Updated Date',
            'updated_by' => 'Updated By'
        ];
    }


    function get_news_datatable($order_clm, $order_dir, $start, $ofsite, $search){        

        $where = "1=1";
        if (!empty($search))
        {
            $where .=" AND ( news_title LIKE '%" . $search . "%') ";
        }
        $query = new Query();
        $data = $query->select("*")
                ->from('news')                
                ->where($where)               
                ->orderBy($order_clm." ".$order_dir)
                ->limit($ofsite)
                ->offset($start)
                ->all();

        $result = $data;

        $total =  (new Query())->select("count(*) as total")->from('news')->where($where)->one();
        $totalFiltered = (isset($total["total"]) && $total["total"]) ? $total["total"] : 0;

        return ['num_rows' => $totalFiltered, 'rows' => $result];

    }


    public function getNewsProfiling($nik="", $limit=5)
    {

        $query = new Query();
        $where = "1=1";
        if($nik){
            $where = ['p.nik' => $nik];
        }
        $get_profiling = $query->select(" CASE WHEN disease_id > 0 THEN
(select name from m_disease where disease_id = pd.disease_id)
else other end  diagnosa ")
                ->from('profiling p')
                ->join('INNER JOIN', 'profiling_disease pd', 'p.nik = pd.nik')               
                ->where($where)
                ->all();

        $keyword = array();

        if($get_profiling){
            foreach ($get_profiling   as $key => $value) {
                $keyword[] = $value['diagnosa']; 
            }
            $keyword = implode(",", $keyword);
        }

        $news_data = array();
        $news_id_to_remove = array(0);
        $result['related_news'] = "";
        $result['latest_news'] = "";
        $result['collective_news'] = "";


        if($keyword){

            // $news_by_keyword = array();

            $query = new Query();
            $get_news_by_keyword = $query->select("news_id, news_title, news_img")
                ->from('news')
                ->where( "(MATCH (news_title) AGAINST ('".$keyword."' IN NATURAL LANGUAGE MODE)
                            OR
                             MATCH (news_content) AGAINST ('".$keyword."' IN NATURAL LANGUAGE MODE)
                            OR
                             MATCH (news_tag) AGAINST ('".$keyword."' IN NATURAL LANGUAGE MODE))")
                ->orderBy("updated_date desc")
                ->limit($limit)
                ->all();

            foreach ($get_news_by_keyword   as $key => $value) {
                $news_data[] = $value;
                $news_id_to_remove[] = $value['news_id'];
            }
            $result['related_news'] = $news_data;

            // $get_news = (count($get_news_by_keyword) < $limit ) ? true : false;
        }

        // if($get_news){

            $query = new Query();
            $get_news = $query->select("news_id, news_title, news_img")
                ->from('news')
                ->orderBy("updated_date desc")
                ->limit($limit)
                ->all();

            $result['latest_news'] = $get_news;

            foreach ($get_news   as $key => $value) {
                if ( !in_array($value['news_id'], $news_id_to_remove) ){
                    $news_data[] = $value;
                }
            }

        // }


        $collective = array();
        for ($i=0; $i < $limit; $i++) {

            if($i == count($news_data) ){
                break;
            }
            $collective[] = $news_data[$i];
        }

        $result['collective_news'] = $collective;

        return $result;
    }
}

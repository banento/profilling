<?php

namespace frontend\controllers;

use Yii;
use frontend\models\News;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\components\RestHelper;
use backend\models\UploadForm;

class NewsController extends Controller
{
    public $title = "Profiling - NEWS";

    private $success = 'Success';
    private $failed = 'Failed';

    public function beforeAction($action)
    {
        if(parent::beforeAction($action)){
            if (!Yii::$app->session->get('isLoggedin')){

                Yii::$app->session->setFlash('error', "Error, please sign in to start your session.");
                
                $this->redirect(['login/index']);
                return false;
            }
            
            return true;
            
        }
    }

    public function actionIndex()
    {        
        Yii::$app->view->title = $this->title;

        return $this->render('index');
    }

    public function actionCreate()
    {        
        Yii::$app->view->title = $this->title;

        return $this->render('create');
    }

    public function actionView()
    {        
        $input = Yii::$app->request->get();

        $id = $input['id'];

        $news = News::findOne(['news_id' => $id]);
        Yii::$app->view->title = $news['news_title'];

        $data = array(
                        'news_id'      => $news['news_id'],
                        'news_title'   => $news['news_title'],
                        'news_content' => $news['news_content'],
                        'news_img'     => ($news['news_img']) ? substr($news['news_img'], strpos($news['news_img'], 'uploads')) : ''
                    );

        return $this->render('view', $data);

    }

    public function actionEdit()
    {        
        Yii::$app->view->title = $this->title;
        $input = Yii::$app->request->get();

        $id = $input['id'];

        $news = News::findOne(['news_id' => $id]);

        $data = array(
                        'news_id'      => $news['news_id'],
                        'news_title'   => $news['news_title'],
                        'news_content' => $news['news_content'],
                        'news_tag'     => $news['news_tag'],
                        'news_img'     => ($news['news_img']) ? substr($news['news_img'], strpos($news['news_img'], 'uploads')) : ''
                    );

        return $this->render('edit', $data);

    }

    public function actionSave()
    {

        $input = Yii::$app->request->post();

        $news_title = $input['news_title'];
        $news_content = $input['news_content'];
        $nik = Yii::$app->session->get('nik');
        
        $connection = News::getDb();
        $transaction = $connection->beginTransaction();
        
        try{    

            $news = new News();
            $news->news_title = $input['news_title'];
            $news->news_content = $news_content;
            $news->created_by = $input['news_content'];
            $news->news_tag = $input['news_tag'];
            $news->created_date = date('Y-m-d H:i:s');
            $news->updated_date = date('Y-m-d H:i:s');
            $news->created_ip = $_SERVER['REMOTE_ADDR'];


            if (isset($_FILES['news_img']['name']) && $_FILES['news_img']['name']) {
                $model = new UploadForm();
                $model->file = \yii\web\UploadedFile::getInstanceByName('news_img');
             
                if ($model->validate()) {                   
                    $path_dir = "news";
                    $upload = $model->doupload($path_dir);
                    if ($upload !== false) {
                        $news->news_img = "/".$upload['path']."/".$upload['file_name'];
                    }
                }
            }
            $news->save(false);

            $transaction->commit();
            $res_status = $this->success;
            $message = "Successfully create data";

        } catch (\Exception $e) {
            $transaction->rollBack();
            $res_status = $this->failed;
            if(YII_DEBUG){
                $message = $e->getMessage();
            } else{
                $message = "Failed to create data";
            }
        }
        $result = ['status' => $res_status, 'message' => $message];

        RestHelper::send_response(200, $result ); 
    }

    public function actionUpdate()
    {

        $input = Yii::$app->request->post();
        $news_id = $input['news_id'];
        $news_title = $input['news_title'];
        $news_content = $input['news_content'];
        $news_tag = $input['news_tag'];
        $nik = Yii::$app->session->get('nik');
        
        $connection = News::getDb();
        $transaction = $connection->beginTransaction();
        
        try{    

            $news = new News();
            $news = News::findOne(['news_id' => $news_id]);
            $old_image = ($news->news_img) ? substr($news->news_img, strpos($news->news_img, 'uploads')) : '';
            $news->news_title = $input['news_title'];
            $news->news_content = $news_content;
            $news->news_tag = $news_tag;
            $news->updated_by = $nik;
            $news->updated_date = date('Y-m-d H:i:s');

            if (isset($_FILES['news_img']['name']) && $_FILES['news_img']['name']) {
                $model = new UploadForm();
                $model->file = \yii\web\UploadedFile::getInstanceByName('news_img');
             
                if ($model->validate()) {                   
                    $path_dir = "news";
                    $upload = $model->doupload($path_dir);
                    if ($upload !== false) {
                        $news->news_img = "/".$upload['path']."/".$upload['file_name'];
                        if($old_image){
                            $path_file = Yii::getAlias('@webroot')."/".$old_image;          
                            if(file_exists($path_file)) unlink($path_file);
                        }
                    }
                }
            }
            $news->save(false);

            $transaction->commit();
            $res_status = $this->success;
            $message = "Successfully update data";

        } catch (\Exception $e) {
            $transaction->rollBack();
            $res_status = $this->failed;
            if(YII_DEBUG){
                $message = $e->getMessage();
            } else{
                $message = "Failed updated data";
            }
        }
        $result = ['status' => $res_status, 'message' => $message];

        RestHelper::send_response(200, $result ); 
    }



    public function actionList()
    {        

        $input = Yii::$app->request->get();

        $result = array("data" => array(), "draw" => 0, "recordsFiltered" => 0, "recordsTotal" => 0 );
        $rawLength      = $input["length"];
        $start          = $input["start"];
        $search         = $input["search"];
        $order          = $input["order"];
        $columns        = $input["columns"];

        $order_clm      = $columns[$order[0]["column"]]["data"];
        $order_dir      = $order[0]["dir"];
        $draw           = $input["draw"];
        $ofsite         = $rawLength;

        $data = News::get_news_datatable( $order_clm, $order_dir, $start, $ofsite, $search["value"] );

        $no = $start+1;
        foreach($data["rows"] as $keyVal => $value){
            $dataRaw = array();
            foreach($columns as $k => $rawCol){
                $dataRaw[$rawCol["data"]] = "";

                if(array_key_exists($rawCol["data"], $value)){
                    $row_data = $value[$rawCol["data"]];
                    if($rawCol["data"] != "no"){
                        $dataRaw[$rawCol["data"]] = $row_data;
                    }
                }
            }

            $dataRaw["no"]      = $no;
            $dataRaw["news_id"] = $value['news_id'];
            $dataRaw["updated_date"] = $value['updated_date'];
            array_push($result["data"], $dataRaw);
            $no++;
        }

        $result["draw"]             =  $draw;
        $result["recordsTotal"]     =  $data["num_rows"];
        $result["recordsFiltered"]  =  $data["num_rows"];

        RestHelper::send_response(200, $result);

    }



    public function actionDelete()
    {
        $get = Yii::$app->request->post();

        $news = News::findOne( ['news_id' => $get['news_id'] ]);
        $news->delete();

        $result = ['status' => $this->success, 'message' => 'success delete record'];

        RestHelper::send_response(200, $result);
    }
}

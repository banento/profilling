<?php

namespace frontend\models;

use Yii;
use yii\db\Query;
use yii\db\Expression;


/**
 * This is the model class for table "news".
 *
 * @property int $id_news
 * @property string $news_id
 * @property string|null $news_title
 * @property string|null $unique_id_msg
 * @property string|null $news_content
 * @property string|null $created_date
 * @property string|null $created_by
 * @property string|null $created_ip
 * @property string|null $updated_date
 * @property string|null $updated_by
 * @property string|null $updated_ip
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id'], 'required'],
            [['created_date', 'updated_date'], 'safe'],
            [['created_by', 'updated_by'], 'string', 'max' => 25],
            [['news_title'], 'string', 'max' => 150],
            [['news_tag','news_img'], 'string', 'max' => 250],
            [['news_content'], 'string'],
            [['created_ip'], 'string', 'max' => 20],
            [['news_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'News Id',
            'news_title' => 'News Title',
            'news_content' => 'News Content',
            'news_tag' => 'News Tags',
            'news_img' => 'News Img',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'updated_date' => 'Updated Date',
            'updated_by' => 'Updated By'
        ];
    }


    function get_news_datatable($order_clm, $order_dir, $start, $ofsite, $search){

        $sql = "SELECT  * from news where 1=1";

        if (!empty($search))
        {
            $sql .=" AND ( news_title LIKE '%" . $search . "%') ";
        }
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        $totalFiltered = count($data);

        $sql .= " ORDER BY $order_clm $order_dir LIMIT " . $start . " ," . $ofsite. " ";

        $result = Yii::$app->db->createCommand($sql)->queryAll();

        return ['num_rows' => $totalFiltered, 'rows' => $result];

    }
}

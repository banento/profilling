<?php

namespace backend\controllers\api;

use Yii;
use yii\web\HttpException;
use yii\base\ErrorException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\db\Query;
use backend\models\ApiUser;
use backend\models\News;

use yii\rest\ActiveController;
use backend\components\RestHelper;

class NewsController extends Controller
{


    private $STATUS_INACTIVE = 'N';
    private $STATUS_ACTIVE = 'Y';
    private $success = 'success';
    private $failed = 'failed';
    private $status_code = 200;


    public function behaviors()
    {
        return [
            'basicAuth' => [
                'class' => \yii\filters\auth\HttpBasicAuth::className(),
                'auth' => function ($username, $password) {
                    return ApiUser::authenticateLogin($username, $password);
                },
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost:8080/', 'http://localhost:8080/'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['X-Wsse'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
            ]
        ];

    }

    public function beforeAction($action)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->controller->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex(){

        $get = Yii::$app->request->get();

        $headerData = Yii::$app->request->headers;
        if($headerData->has('xusernik')){
            $nikHeader = $headerData->get('xusernik');
        }
        if(!isset($nikHeader)) {
            $return['message'] = 'undefine parameter';
            return $return;
        }

        $res_status = $this->failed;
        $res_msg    = "";
        $data       = "";

        if(isset($get['id'])){

            $id = (int) $get['id'];

            if($id > 0){

                $news = News::findOne(['news_id' => $id]);

                if($news){

                    $path = \Yii::getAlias('@'.str_replace("/frontend/","frontend/", $news->news_img));
                    $base64 = "";
                    if (file_exists($path)) {
                        $imagedata = file_get_contents($path);
                        $base64 = base64_encode($imagedata);
                    }

                    $data = array(
                                           "id"      => $news->news_id,
                                           "title"   => $news->news_title,
                                           "img"     => $base64,
                                           "content" => $news->news_content
                                    );
                    $res_status = $this->success;
                }
                else{
                    $res_msg = "No data found";
                }

            }else{
                $res_msg = "No Id Selected";
            }

        }else{
            $newsList = array();
            $news = News::find()->orderBy(['news_id' => SORT_DESC])->all();
            foreach ($news as $row){

                $path = \Yii::getAlias('@frontend'.str_replace("/frontend","", $row->news_img));
                $base64 = "";
                if (file_exists($path)) {
                    $content = @file_get_contents($path);
                    if($content !== FALSE) {
                        $base64 = base64_encode($content);
                    }
                }
                $newsList[] = array(
                                       "id"      => $row->news_id,
                                       "title"   => $row->news_title,
                                       "img"     => $base64,
                                       "content" => $row->news_content
                                );
            }

            if($newsList){
                $res_status = $this->success;
                $data = $newsList;
            }
            else{
                $res_msg = "No data found";
            }

        }

        RestHelper::send_response(200, [
                                        'status'   => $res_status,
                                        'messages' => $res_msg,
                                        'data'     => $data
                                        ] );
    }

    public function actionProfiling(){

        $get = Yii::$app->request->get();

        $headerData = Yii::$app->request->headers;
        if($headerData->has('xusernik')){
            $nikHeader = $headerData->get('xusernik');
        }
        $post = Yii::$app->request->get();
        if (isset($post['nik'])) {
            $nikBody = $post['nik'];
        }
        if(!isset($nikHeader)||!isset($nikBody)) {
            $return['message'] = 'undefine parameter';
            return $return;
        }
        if ($nikHeader != $nikBody) {
            Yii::$app->response->statusCode = 401;
            $return['message'] = 'unauthorized request';
            return $return;
        } else {
            $nik = $nikHeader;
        }

        $res_status = $this->failed;
        $data       = "";

        $news = new News();
        $news_profiling =  $news->getNewsProfiling($nik);

        if($news_profiling){
            $res_status = $this->success;
            $data   = array();

            foreach ($news_profiling as $key => $value) {

                    $row = array();
                if($value){
                    

                    foreach ($value as $v) {
                        $path = \Yii::getAlias('@frontend'.str_replace("/frontend","", $v['news_img']));
                        $base64 = "";
                        if (file_exists($path)) {
                            $content = @file_get_contents($path);
                            if($content !== FALSE) {
                                $base64 = base64_encode($content);
                            }
                        }
                        $row[] = array(
                                        "id"      => $v['news_id'],
                                        "title"   => $v['news_title'],
                                        "img"     => $base64
                                );
                    }
                }
                
                $data[$key] = $row;
            }
            $res_msg = "";
        }
        else{
            $res_msg = "No data found";
        }



        RestHelper::send_response(200, [
                                        'status'   => $res_status,
                                        'messages' => $res_msg,
                                        'data'     => $data
                                        ] );
    }

}

$(()=>{


  $(document.body).on("click",".dialog-modal",function(){
      var url = $(this).attr("data-url");
      var title = $(this).attr("data-title");
      show_dialog(url,title);
      return false;
  });



})

function show_dialog(url,title)
{
  $('#modal_loading').show(200)
    $("#modal_title").html(title);	
    $("#dialog_content").html('');	
    $("#dialog_content").load(url,function(){
        $('#modal_loading').hide(200)
    });
    $("#modal_dialog").modal("show");
}

function toast_notif(type, message, title="", time=3500){

    let heading = '';

    switch (type) {
      case "success":
            heading = "Success";
        break;
      case "error":
            heading = "Failed";
        break;
      case "warning":
            heading = "Warning";
          break;
      default:
            heading = "Info";
        break;
    }

    heading = (title !== "") ? title : heading;

    $.toast({
      heading: heading,
      text: message,
      position: 'top-right',
      loaderBg:'#ff6849',
      icon: type,
      hideAfter: time,
      stack: 6
    });

}

function customLoading(action){
  let html = '';

    if($("#customLoading").length == 0) {

      html =   `<div id="customLoading" class="customLoading hide">;
                      <div class="customLoadingContainer">
                <div class="spinner-border text-white" role="status" style="width: 3rem; height: 3rem;">
                  <span class="sr-only">Loading...</span>
                </div>
            </div</div>`;
      $("body").append(html);
    }

    if(action == "show"){
      $("#customLoading").removeClass('hide');
      
      setTimeout(function() {
        $("#customLoading").addClass('hide');
      }, 120000);
    }
    else{
          $("#customLoading").addClass('hide');
    }

  }
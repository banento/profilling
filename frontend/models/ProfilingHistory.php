<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profiling_history".
 *
 * @property int $profiling_id_h
 * @property string|null $nik
 * @property string|null $notes
 * @property string|null $attachment
 * @property string|null $created_by
 * @property string|null $created_date
 * @property int|null $status 0 = non aktif, 1 = aktif
 */
class ProfilingHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profiling_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profiling_id_h'], 'required'],
            [['profiling_id_h', 'status'], 'integer'],
            [['notes', 'attachment'], 'string'],
            [['created_date'], 'safe'],
            [['nik', 'created_by'], 'string', 'max' => 20],
            [['profiling_id_h'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'profiling_id_h' => 'Profiling Id H',
            'nik' => 'Nik',
            'notes' => 'Notes',
            'attachment' => 'Attachment',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'status' => 'Status',
        ];
    }
}

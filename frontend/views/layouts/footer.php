
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
		  
		  <li class="nav-item">
			<label>&copy; 2022 Telkomsel. All Rights Reserved.</label>
		  </li>
		</ul>
    </div>
  </footer>
const base_url = $("#base_url").val();
console.log(base_url);
/*
$(function () {
    "use strict";

});*/


let table_news = null;

function getDataNews(){
	if(table_news != null){
	  table_news.destroy();
	}
	table_news = $('#table_news').DataTable( {
	  "processing": true,
	  "serverSide": true,
	  "ajax": {
				"url": base_url + "news/list",
				"type": "GET",
	  },
	  "language"     : {
	      "emptyTable"  : "<span class ='label label-danger'>Data not found!</span>",  
	      "infoEmpty"   : "Data Empty",
	      "processing"  : `<div class="spinner-border" role="status">
			                  <span class="sr-only">Loading...</span>
			                </div>`,
	      "search"      : "_INPUT_"
	  },
	  "columns": [
	        { "data": "news_id"},
	        { "data": "updated_date"},
	        { "data": "no", "width": "5%", "class":"text-center"},
	        { "data": "news_title"},
	        { 
	          "data": "news_title",
	          "width": "20%",
	          "class":"text-center",
	          "render": function (data) {
	          	return `
	          			<button alt="1" class="btn-view-news btn btn-info waves-effect waves-l ight btn-xs" title="Click to view ${data}"><i class="fa fa-eye"></i></button>
	          			<button alt="1" class="btn-edit-news btn btn-warning waves-effect waves-l ight btn-xs" title="Click to edit ${data}"><i class="fa fa-pencil"></i></button>
	          			<button alt="1" class="btn-delete-news  btn btn-danger waves-effect waves-light btn-xs" title="Click to delete ${data}"><i class="fa fa-trash"></i></button>`;
	          }
	        }
	  ],
	  "drawCallback": function( settings, start, end, max, total, pre ) {
            $(".DTFC_LeftBodyLiner").css('overflow','hidden');
	  },
	  "order":         [[1,'desc']],
	  "paging":         true,
	  "columnDefs": [
	      {
	      	"targets": [ 2, 4],
	        "orderable": false

	      },
	      {
	          "targets": [ 0, 1],
	          "visible": false
	      }
	  ],
	  "scrollY"          : 520, 
	  "scrollCollapse"   : true,
	  "scrollX"         : false,
      "bAutoWidth" : true

    });

	$('input[type="search"]').attr('placeholder','Search here...').addClass('form-control input-sm m-0');


}



$(document).ready(function(){

  getDataNews();


  /* Create */
  $('#btn-create-news').on( 'click', function () {
    	window.location.href = base_url + 'news/create';
	});


  /* View */
	$('#table_news tbody').on( 'click', 'button.btn-view-news', function () {
			let news_id = table_news.row( $(this).parents('tr') ).data().news_id;
    	window.location.href = base_url + 'news/view/' + news_id;
	});


  /* Edit */
	$('#table_news tbody').on( 'click', 'button.btn-edit-news', function () {
			let news_id = table_news.row( $(this).parents('tr') ).data().news_id;
    	window.location.href = base_url + 'news/edit/' + news_id;
	});

  /* Delete */
	$('#table_news tbody').on( 'click', 'button.btn-delete-news', function () {

			let news_id = table_news.row( $(this).parents('tr') ).data().news_id;

      swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this record!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: true 
        }, function(isConfirm){   
            if (isConfirm) {     
                 $.ajax({
						      url       : base_url + 'news/delete/',
						      type      : 'DELETE',
						      data        : { news_id: news_id },
						      dataType : 'json',
						      success : function(result){
						        if(result.status == 'Success'){
		              		table_news.ajax.reload(null, false);
						          toast_notif('success', result.message);
						        }
						        else{
						          toast_notif('error', result.message);
						        }
						      },
									error	: function (xhr, status, error) {
					          toast_notif('error', xhr.responseText);
									}
						    });
            }
      });
	});
	/* End delete */


});
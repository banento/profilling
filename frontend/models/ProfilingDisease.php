<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "profiling_disease".
 *
 * @property int $pd_id
 * @property string|null $nik
 * @property int|null $disease_id
 */
class ProfilingDisease extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profiling_disease';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['disease_id'], 'integer'],
            [['nik'], 'string', 'max' => 50],
            [['other'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pd_id' => 'Pd ID',
            'nik' => 'Nik',
            'disease_id' => 'Disease ID',
            'other' => 'Other',
        ];
    }
}

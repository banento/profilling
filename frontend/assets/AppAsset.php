<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/custom.css',
        //login css
        'assets/vendor_components/bootstrap/dist/css/bootstrap.min.css',
        'assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css',
        'css/bootstrap-extend.css',
        'css/master_style.css',
        'css/skins/_all-skins.css',
        'assets/vendor_components/datatable/datatables.min.css',
        'assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css',
        // 'assets/vendor_components/morris.js/morris.css',
        'assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css',
        // 'assets/vendor_components/datatable/datatables.min.css'
        'assets/vendor_plugins/iCheck/all.css'
    ];
    public $js = [
        //login js
        // 'assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js',
        'assets/vendor_components/popper/dist/popper.min.js',
        'assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',
        //home js
        'assets/vendor_components/jquery-ui/jquery-ui.js',
        'assets/vendor_components/moment/min/moment.min.js',
        'assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js',
        'assets/vendor_components/bootstrap/dist/js/bootstrap.js',
        'assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js',
        'assets/vendor_components/datatable/datatables.min.js',
        'assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js',
        'assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js',
        'assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js',
        'assets/vendor_components/perfect-scrollbar-master/perfect-scrollbar.jquery.min.js',
        'assets/vendor_components/fastclick/lib/fastclick.js',
        // 'assets/vendor_components/datatable/datatables.min.js',
        // 'assets/vendor_components/CryptoJS v3.1.2/rollups/aes.js',
        // 'assets/vendor_components/chart.js-master/Chart.min.js',
        // 'assets/vendor_components/raphael/raphael.min.js',
        // 'assets/vendor_components/morris.js/morris.min.js',
        // // HOME & Chat js plugin
        'js/template.js',
        // 'js/pages/dashboard.js',
        'js/demo.js',
        'js/global.js',
        // 'js/socket.io-2.3.0.js',
        // 'js/pages/app-chat.js',
        'assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.js',
        'assets/vendor_plugins/iCheck/icheck.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}

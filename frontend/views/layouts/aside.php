<?php 
use yii\helpers\Url;
?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">
      
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">

        <!-- <li class="active"> -->
        <li class="">
          <a href="<?= Url::toRoute('home/index') ?>">
            <i class="mdi mdi-view-dashboard"></i><span>Home</span>
          </a>
        </li>
 
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-contacts"></i>
            <span>Profiling</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Url::toRoute('profiling/create') ?>"><i class="mdi mdi-toggle-switch-off"></i>Create</a></li>
            <li><a href="<?= Url::toRoute('profiling/index') ?>"><i class="mdi mdi-toggle-switch-off"></i>Profiling</a></li>
            <li><a href="<?= Url::toRoute('profiling/mcu_upload') ?>"><i class="mdi mdi-toggle-switch-off"></i>Upload MCU</a></li>
            <li><a href="<?= Url::toRoute('profiling/mcu') ?>"><i class="mdi mdi-toggle-switch-off"></i>List MCU</a></li>
          </ul>
        </li>
      
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-newspaper"></i>
            <span>News</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= Url::toRoute('news/create') ?>"><i class="mdi mdi-toggle-switch-off"></i>Create</a></li>
            <li><a href="<?= Url::toRoute('news/index') ?>"><i class="mdi mdi-toggle-switch-off"></i>List News</a></li>
          </ul>
        </li>
      </ul>
    </section>
  </aside>
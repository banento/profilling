<?php

namespace backend\controllers\api;

use Yii;
use yii\web\HttpException;
use yii\base\ErrorException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\db\Query;
use backend\models\ApiUser;
use common\models\LoginForm;
use backend\components\RestHelper;

class LoginController extends \yii\web\Controller
{
    private $success = 'success';
    private $failed = 'failed';
    private $code_success = 200;
    private $code_failed = 500;
   
  
    public function beforeAction($action)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->controller->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $data = Yii::$app->request->post();
        $res_status["status"] = $this->failed;
        $res_status["message"] = "The server encountered an error processing your request.";
        $res_status["data"] = [];
        if(isset($data['username']) && isset($data['password'])){
            if($data['username'] && $data['password']){
                $ldap = new LoginForm();
                $ldap->username = $data['username'];
                $ldap->password = $data['password'];
                $login =$ldap->login();        
                $res_status = $login;
            } else {
                $res_status["message"] = "Username dan Password tidak boleh kosong.";
            }

        } else {
            $res_status["message"] = "Parameter Username atau Password tidak ditemukan.";
        }
        
       
        RestHelper::send_response($this->code_success, $res_status);
    }

}

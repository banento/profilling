<?php

namespace backend\controllers\api;

use Yii;
use yii\web\HttpException;
use yii\base\ErrorException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\db\Query;
use backend\models\ApiUser;
use backend\models\ChatFiles;
use yii\web\UploadedFile;
use backend\models\UploadForm;

use yii\rest\ActiveController;
use backend\components\RestHelper;

class FileController extends Controller
{



    private $STATUS_INACTIVE = 'N';
    private $STATUS_ACTIVE = 'Y';
    private $success = 'success';
    private $failed = 'failed';
    private $status_code = 200;


    public function behaviors()
    {
        return [
            'basicAuth' => [
                'class' => \yii\filters\auth\HttpBasicAuth::className(),
                'auth' => function ($username, $password) {
                    return ApiUser::authenticateLogin($username, $password);
                },
            ],
        ];
    }

    public function beforeAction($action)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->controller->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionUpload(){

        if (Yii::$app->request->isPost) {

            $data = Yii::$app->request->post();
            $unique_id_msg = $data['unique_id_msg'];
            $model = new UploadForm();

            if (isset($_FILES['file'])) {
                $model->file = \yii\web\UploadedFile::getInstanceByName('file');

                if ($model->validate()) {
                    $upload = $model->upload();
                    if ($upload == false) {
                        RestHelper::send_response(400, $model->getErrors());
                    } else {

                        $chatFile = new ChatFiles();
                        $chatFile->name_file = $upload['file_name'];
                        $chatFile->loc_file = $upload['path'];
                        $chatFile->file_type = $upload['file_type'];
                        $chatFile->unique_id_msg = $unique_id_msg;
                        $chatFile->created_date = date("Y-m-d H:i:s");
                        $chatFile->status = "Y";
                        $chatFile->unique_id_file = uniqid();
                        $chatFile->save();

                        $result = ['status' => 'success', 'file_name' => $upload['file_name'] ];
                        RestHelper::send_response(200, $result);
                    }
                }
            } else {
                RestHelper::send_response(204, 'Nothing to execute');
            }
        }
    }
    public function actionDelete()
    {
        $data = Yii::$app->request->post();
        $unique_id_file = $data['unique_id_file'];

        $connection = ChatRoomMember::getDb();
        $transaction = $connection->beginTransaction();

        try{
            $file = ChatFiles::findOne(['unique_id_file'=>$unique_id_file]);
            $file->status = $this->STATUS_INACTIVE;
            $file->save();

            $transaction->commit();
            $res_status = $this->success;
            $message = "File sukses dihapus";

        } catch (\Exception $e) {
            $transaction->rollBack();
            $res_status = $this->failed;
            $this->status_code = 400;

            
            if(YII_DEBUG){
                $message = $e->getMessage();
            } else{
                $message = "File gagal dihapus";
            }
        }

        $result =  ['status' => $res_status, "message" => $message];

        RestHelper::send_response(200, $result);

    }
}

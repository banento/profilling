<?php
namespace backend\controllers;

use Yii;
use yii\web\HttpException;
use yii\base\ErrorException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\db\Query;
use backend\models\ApiUser;
use backend\models\ChatMemberTo;
use backend\models\ChatMessages;
use backend\models\ChatFiles;
use backend\models\ChatReply;
use backend\models\ChatRoom;
use backend\models\ChatRoomMember;
use backend\models\ChatStatusRead;
use common\models\Employee;

/**
 * Api controller
 */
class ApichatController extends Controller
{
	private $STATUS_INACTIVE = 'N';
    private $STATUS_ACTIVE = 'Y';
    private $success = 'success';
    private $failed = 'failed';

    public function beforeAction($action)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		Yii::$app->controller->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	public function behaviors()
    {
		return [
			'basicAuth' => [
				'class' => \yii\filters\auth\HttpBasicAuth::className(),
				'auth' => function ($username, $password) {
                    return ApiUser::authenticateLogin($username, $password);
				},
			],
		];
	}

	public function actionIndex()
	{
		echo "Hello";
	}
	
	public function actionTest(){
		$data = Yii::$app->request->post();
		$nik = $data['nik'];
		
		return $nik;
	}

	public function actionGetchat()
	{
		$data = Yii::$app->request->post();
		$from = $data['from'];
		$to = (isset($data['to']) && $data['to']) ? $data['to'] : "";
		
		if(!$to){
			$to = $this->getTo();
		}		

		$res_status = $this->failed;
		$message = "Chat tidak ditemukan";
		$chat = [];
		$room = $this->getRoom($from, $to);

		$model = new ChatMessages();
		$chat = $model->listChat($room);
		if($chat){
			$res_status = $this->success;
			$message = "Berhasil";
		}
		return ['status'=>$res_status,"message"=>$message,"data"=>$chat];
	}
	
	public function actionSend()
	{
		$data = Yii::$app->request->post();		
		$from = $data['from'];
		$to = (isset($data['to']) ? $data['to'] : $this->getTo());
		$chat = $data['chat'];
		$is_file = (isset($data['is_file'])?$data['is_file']:0);
		$is_reply = (isset($data['is_reply'])?$data['is_reply']:0);
		$unique_id_msg = (isset($data['unique_id_msg'])?$data['unique_id_msg']:0);
		$tgl = date("Y-m-d H:i:s");		
		$chat_reply = NULL;

		//define var for reply chat
		if($is_reply == 1){
			$chat_reply = $chat;
			$chat = $this->getChatbyUniq($unique_id_msg);
		}

		$connection = ChatRoomMember::getDb();
		$transaction = $connection->beginTransaction();
		try{	

			//get N cek room=================================
			$room = $this->getRoom($from, $to, $tgl);

			//get chat reply
			if($is_reply==1){
				$msg = $this->getChatbyUniq($unique_id_msg);
			}
			
			$msg = new ChatMessages();
			$msg->unique_id_msg = uniqid();
			$msg->messages = ($is_file==1 ? NULL : $chat);
			$msg->created_by = $from; //nik
			$msg->created_date = $tgl;
			$msg->ip_address = $_SERVER['REMOTE_ADDR'];
			$msg->status = $this->STATUS_ACTIVE;
			$msg->unique_id_room = $room;
			$msg->is_file = ($is_file==1?1:0);
			$msg->is_reply = ($is_reply==1?1:0);
			$msg->save();

			$status = new ChatStatusRead();
			$status->unique_id_msg = $msg->unique_id_msg;
			$status->id_member = $to; //nik
			$status->is_sent = 1;
			$status->is_read = 0;
			$status->created_by = $from; //nik
			$status->created_date = $tgl;
			$status->created_ip = $_SERVER['REMOTE_ADDR'];
			$status->status = $this->STATUS_ACTIVE;
			$status->unique_id_room = $room;
			$status->save();

			if($is_reply==1){
				$reply = new ChatReply();
				$reply->unique_id_reply = uniqid();
				$reply->unique_id_msg = $msg->unique_id_msg;
				$reply->reply_msg = $chat_reply;
				$reply->created_date = $tgl;
				$reply->created_by = $from;
				$reply->created_ip = $_SERVER['REMOTE_ADDR'];
				$reply->unique_id_room = $room;
				$reply->status = $this->STATUS_ACTIVE;
				$reply->save(); 
			}

			$transaction->commit();
			$res_status = $this->success;
			$message = "Pesan terkirim";

		} catch (\Exception $e) {
			$transaction->rollBack();
			$res_status = $this->failed;
			if(YII_DEBUG){
				$message = $e->getMessage();
			} else{
				$message = "Pesan gagal terkirim";
			}
		}

		return ['status'=>$res_status,"from"=>$from,"to"=>$to,"message"=>$message];
	}

	public function actionDeletemsg()
	{
		$data = Yii::$app->request->post();
		$unique_id_msg = $data['unique_id_msg'];

		$connection = ChatRoomMember::getDb();
		$transaction = $connection->beginTransaction();

		try{
			//update chat_messages
			$chat = ChatMessages::findOne(['unique_id_msg'=>$unique_id_msg]);
			$chat->status = $this->STATUS_INACTIVE;
			$chat->save();

			//update chat_reply
			$reply = ChatReply::findOne(['unique_id_msg'=>$unique_id_msg]);
			$reply->status = $this->STATUS_INACTIVE;
			$reply->save();
			
			//update chat_messages
			$file = ChatFiles::findOne(['unique_id_msg'=>$unique_id_msg]);
			$chat->status = $this->STATUS_INACTIVE;
			$chat->save();

			$transaction->commit();
			$res_status = $this->success;
			$message = "Pesan sukses dihapus";

		} catch (\Exception $e) {
			$transaction->rollBack();
			$res_status = $this->failed;
			if(YII_DEBUG){
				$message = $e->getMessage();
			} else{
				$message = "Pesan gagal dihapus";
			}
		}

		return ['status'=>$res_status,"message"=>$message];
	}

	public function actionStatusread()
	{
			$data = Yii::$app->request->post();

			$dataupdate = ["is_read"=>1,"updated_by"=>$data['nik'],"updated_date"=>date('Y-m-d H:i:s'),"updated_ip"=>$_SERVER['REMOTE_ADDR'],"status"=>$this->STATUS_ACTIVE];
			$status = ChatStatusRead::updateAll($dataupdate,['unique_id_room'=>$data['unique_id_room'],'id_member'=>$data['nik']]);

			if($status){
				return ['status'=>$this->success,"data"=>"status read update"];
			}
			else{
				return ['status'=>$this->failed,"data"=>"failed to update"];
			}

	}

	public function actionListroom()
	{
		$data = Yii::$app->request->post();
		
	}

	public function actionGetdoctor()
	{
		$doc = ChatMemberTo::findOne(["status"=>$this->STATUS_ACTIVE]);
		if($doc){
			return ['status'=>$this->success,"data"=>$doc];
		}
		else{
			return ['status'=>$this->failed,"data"=>"No data found"];
		}
	}

	private function getTo()
	{
		$to = ChatMemberTo::findOne(["status"=>$this->STATUS_ACTIVE]);

		return $to['nik'];
	}

	private function getRoom($from, $to, $tgl="")
	{
		$tgl = ($tgl) ? $tgl : date("Y-m-d H:i:s");
		$val_member = $from.",".$to;
		//get uniq room with the same pattern 1,5 = 5,1
		$arr = array($from,$to);
		sort($arr);
		$uniq_room = substr(md5($arr[0].$arr[1]),0,10);
			
		$member = new ChatRoomMember();
		$xsist = $member->find()->where(["unique_id_room"=>$uniq_room, "status"=> $this->STATUS_ACTIVE])->one();
		
		if($xsist){
			$unique_id_room = $xsist["unique_id_room"];
		} else {
			$member->unique_id_room = $uniq_room;
			$member->member = $val_member; // nik
			$member->created_by = $from;
			$member->created_date = $tgl;
			$member->created_ip = $_SERVER['REMOTE_ADDR'];
			$member->status = $this->STATUS_ACTIVE;
			$member->save();
			
			$from_name = $this->getMember($from);
			$to_name = $this->getMember($to);

			$room = new ChatRoom();	
			$room->room_type = 2;
			$room->unique_id_room = $uniq_room;
			$room->room_name = $from_name.",".$to_name; // nama member
			$room->created_by = $from;
			$room->created_date = $tgl;
			$room->created_ip = $_SERVER['REMOTE_ADDR'];
			$room->last_modify_chat = $tgl;
			$room->status = $this->STATUS_ACTIVE;
			$room->save();

			$unique_id_room = $member->unique_id_room;
		}

		return $unique_id_room;

	}

	private function getMember($nik)
	{
		$member = Employee::findOne(["nik"=>$nik, "status"=>'AKTIF']);
		return $member['nama'];
	}
	
	private function getChatbyUniq($uniq)
	{
		$res = ChatMessages::findOne(['unique_id_msg'=>$uniq,'status'=>$this->STATUS_ACTIVE]);
		return $res['messages'];
	}


}

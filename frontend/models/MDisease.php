<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_disease".
 *
 * @property int $disease_id
 * @property string|null $type
 * @property string|null $name
 * @property string|null $notes
 * @property int|null $status
 */
class MDisease extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_disease';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notes'], 'string'],
            [['status'], 'integer'],
            [['type', 'name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'disease_id' => 'Disease ID',
            'type' => 'Type',
            'name' => 'Name',
            'notes' => 'Notes',
            'status' => 'Status',
        ];
    }
}

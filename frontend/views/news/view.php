<?php 

use yii\helpers\Url;
use yii\web\View;
use common\components\Helper;

?>


  <!-- Content Wrapper. Contains page content -->
  <!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->
   <div class="content-header" style="padding-top:60px !important">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title"><?= $this->title ?></h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
            	<li class="breadcrumb-item"><a href="<?= Url::base() . "/home/index" ?>"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item"><a href="<?= Url::base(); ?>/news">List News</a></li>
							<li class="breadcrumb-item active" aria-current="page"><?= $news_title ?></li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>  

    <!-- Main content -->
    <section class="content" style="padding-top:20px !important">
      <div class="container">
      <div class="row">
        <div class="col-12">

    	<div class="box">
			<div class="box-header with-border">
			  <h4 class="box-title"><?= $news_title ?></h4>
			  <a href="<?= Url::base(); ?>/news/edit/<?= $news_id ?>" class="pull-right btn btn-warning mr-1 btn-sm w-100">
                  <i class="fa fa-pencil"></i> Edit </a>
			</div>
			<div class="box-body">
						<?php
              $path = \Yii::getAlias('@webroot');
							if(@file_exists($path."/".$news_img)){
								$news_img = Url::base() . "/" . $news_img;
							}else{
								$news_img = "https://cdn.pixabay.com/photo/2017/06/10/07/22/news-2389226_1280.png";
							}
						?>
				
				<div class="row">
					<div class="col-12">
						<div id="news_content" class="p-5">
							<div class="text-center">
							<img src="<?= $news_img ?>" class="img-fluid" alt="News image" style="max-height: 400px;width: auto;object-fit:cover;"/>
							</div>
							<?= $news_content?>
						</div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
        </div>
        <!-- /.col-->
      </div>
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->


<input type="hidden" name="base_url" id="base_url" value="<?= Url::base(true) ?>/">


	<!-- popper -->
	<?php $this->registerJsFile('@web/assets/vendor_components/popper/dist/popper.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Bootstrap 4.0-->
	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<?php $this->registerJsFile('@web/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
	<?php $this->registerJsFile('@web/js/news_view.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	

const base_url = $("#base_url").val();

$(document).ready(function(){



	 $('#news_content').summernote({
          height: 300,
          minHeight: null,
          maxHeight: null,
          focus: true ,
          toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['fontname', ['fontname']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture']],
          ['view', ['fullscreen', 'codeview']],
        ]
    });

	$('#change_img').on('click', function(e) {
		$("#old_img").addClass("d-none");
		$("#upd_img").removeClass("d-none");
	});

	$('#discard_change').on('click', function(e) {
		$("#old_img").removeClass("d-none");
		$("#upd_img").addClass("d-none");
	});

	/* Ajax Save */
	$('#news_form').on('submit', function(e) {
	  if (!e.isDefaultPrevented()) {

	    /*$.ajax({
	      url       : base_url+ "news/update",
	      type      : 'POST',
	      enctype   : 'multipart/form-data',
	      data        : $('#news_form').serialize(),*/

	      
		let form_data = new FormData($('#news_form')[0]);
		$.ajax({
			url       : base_url+ "news/update",
			type      : 'POST',
			enctype   : 'multipart/form-data',
			data: form_data,
			dataType 		: 'json',
		    beforeSend  : function(){
			    								customLoading('show');
			                  },
			processData: false,
			contentType: false,
	      success	: function(result){
	        if(result.status == 'Success'){
		    	customLoading('hide');
	          toast_notif('success', result.message);

	          setTimeout(function() {
	          	window.location.href = base_url + 'news';
			      }, 1500);
	        }
	        else{
	          toast_notif('error', result.message);
	        }
	      },
				error	: function (xhr, status, error) {
          toast_notif('error', xhr.responseText);
				}
	    });
	  }
	  e.preventDefault();
	});

});
<?php 

use abei2017\emoji\Emoji;
use yii\helpers\Url;
use yii\web\View;
use common\components\Helper;

?>
  <!-- Bootstrap extend-->
   <?php $this->registerCssFile('@web/css/bootstrap-extend.css'); ?>
  <!-- Bootstrap select -->
  <?php $this->registerCssFile('@web/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css'); ?>
  <!-- Bootstrap tagsinput -->
  <?php $this->registerCssFile('@web/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'); ?>
  <!-- Summernote -->
  <?php $this->registerCssFile('@web/assets/vendor_plugins/summernote/summernote.min.css'); ?>

  <!-- Content Wrapper. Contains page content -->
  <!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->
   <div class="content-header" style="padding-top:60px !important">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="page-title"><?= $this->title ?></h3>
        <div class="d-inline-block align-items-center">
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?= Url::base() . "/home/index" ?>"><i class="mdi mdi-home-outline"></i></a></li>
              <li class="breadcrumb-item"><a href="<?= Url::base(); ?>/news">List News</a></li>
              <li class="breadcrumb-item active" aria-current="page">Create News</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>  

    <!-- Main content -->
    <section class="content" style="padding-top:20px !important">
      <div class="row">
        <div class="col-12">          
     
          <div class="box">
            <div class="box-header">
              <h4 class="box-title">Create News<br>
                <!-- <small>Bootstrap html5 editor</small> -->
              </h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form id="news_form" role="form">
                <div class="form-group m-b-20">
                  <label class="font-size-16">Title</label>
                  <input type="text" class="form-control" name="news_title" id="news_title" placeholder="Enter news title" required>
                </div>
                <div class="form-group m-b-20">
                  <label class="font-size-16">Img/Thumbnail</label>
                  <div class="controls">
                    <input type="file" name="news_img" id="news_img" class="form-control" required="" accept=".jpg,.png"aria-invalid="false"> <div class="help-block"></div>
                  </div>
                </div>
                <!-- <textarea id="news_content" name="news_content" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea> -->

                <div class="form-group m-b-20">
                <label class="font-size-16">Contents</label>
                <textarea id="news_content" name="news_content" required></textarea>
                </div>


                <div class="form-group m-b-20">
                  <label class="font-size-16">Tag</label>
                  <div class="tags-default">
                    <input type="text" value="" data-role="tagsinput" placeholder="add tags" name="news_tag" id="news_tag" required/>
                  </div>
                </div>

              <div class="pt-20">
                <button type="submit" class="btn btn-success btn-outline">
                  <i class="ti-save-alt"></i> Submit
                </button>
                <a href="<?= Url::base(); ?>/news" class="btn btn-warning btn-outline mr-1">
                  <i class="ti-trash"></i> Discard
                </a>
              </div>
              </form>
            </div>
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->



<input type="hidden" name="base_url" id="base_url" value="<?= Url::base(true) ?>/">


	<!-- popper -->
	<?php $this->registerJsFile('@web/assets/vendor_components/popper/dist/popper.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Bootstrap 4.0-->
	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Superieur Admin for demo purposes -->
	<?php $this->registerJsFile('@web/js/demo.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

  <!-- Bootstrap 4.1-->
  <?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

  <!-- Summmernote -->
  <?php $this->registerJsFile('@web/assets/vendor_plugins/summernote/summernote.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

  <!-- Bootstrap Select -->
  <?php $this->registerJsFile('@web/assets/vendor_components/bootstrap-select/dist/js/bootstrap-select.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  <!-- Bootstrap tagsinput -->
	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  <?php $this->registerJsFile('@web/js/news_create.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

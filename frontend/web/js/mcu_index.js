const base_url = $("#base_url").val();

/*
$(function () {
    "use strict";

});*/


let table_mcu = null;

function getDataMcu(){	
	if(table_mcu != null){
	  table_mcu.destroy();
	}
	table_mcu = $('#table_mcu').DataTable( {
	  "processing": true,
	  "serverSide": true,
	  "ajax": {
				"url": base_url + "profiling/mcu_list",
				"type": "POST",
				"data" :{
					"_csrf-frontend" : getToken(),
					// "_formdata" : getFormData()
				} 
	  },
	  "language"     : {
	      "emptyTable"  : "<span class ='label label-danger'>Data not found!</span>",  
	      "infoEmpty"   : "Data Empty",
	      "processing"  : `<div class="spinner-border" role="status">
			                  <span class="sr-only">Loading...</span>
			                </div>`,
	      "search"      : "_INPUT_"
	  },
	  "columns": [	       
	        { "data": "mh_id", "width": "5%", "class":"text-center"},			
	        { "data": "filename_ori"},
	        { "data": "description"},	       
	        { "data": "created_date"},	       
	        // { 
			// 	"data": "profiling_group",
			// 	"render": function (data){
			// 		console.log(data[0].type);
			// 		var html = "dd";
			// 		if(data){						

			// 			html = "<ul>";
			// 			for(var i=0; i < data.length; i++ ){
			// 				html += `<li>${data[i].type_view} : `;
			// 							html += `<ul>`;
			// 							for(var a=0; a < data[i].detail.length; a++ ){
			// 								html += `<li>${data[i].detail[a].name} </li>`;
			// 							}
			// 							html += `</ul>`;
			// 						html += `</li>`;
			// 			}
			// 			html += "</ul>"
			// 		}
			// 		 return html;
			// 	}
			// },
			{ 
				"data": "mh_id",
				"width": "10%",
				"class":"text-center",
				"render": function (data) {
					return `<button alt="1" class="btn-view-mcu btn btn-info waves-effect waves-light btn-xs" title="Click to view ${data}"><i class="fa fa-eye"></i></button>`;
				}
			},
	  ],
	  "drawCallback": function( settings, start, end, max, total, pre ) {
            $(".DTFC_LeftBodyLiner").css('overflow','hidden');
	  },
	  "paging":         true,
	  "columnDefs": [
	      {
	      	"targets": [ 0 ],
	        "orderable": false

	      },
	      {
	          "targets": [ 0],
	          "visible": false
	      }
	  ],
	  "scrollY"          : true, 
	  "scrollCollapse"   : true,
	  "scrollX"         : true,
      "bAutoWidth" : true

    });

	$('input[type="search"]').attr('placeholder','Search here...').addClass('form-control input-sm m-0');
	// $(".dataTables_filter").hide();

}

function getToken(){
	var csrfToken = $('meta[name="csrf-token"]').attr("content");
	return csrfToken;
}

function getFormData(){	
	var formData = $('#formFilterProfiling input').serializeArray();
	return formData;
}

function exportData(){

	var url = base_url+ "export/excel";   	
	var csrfToken = $('meta[name="csrf-token"]').attr("content");

	$.ajax({
		url : url,
		type:"post",              
		dataType:"json",
		data : {_formData: $("#formFilterProfiling").serializeArray(), '_csrf-frontend' : csrfToken},
		error: function (request, status, error) {
				toast_notif(error,"warning");                       
			},
		success:function(data){                                  
			if(data && data.status=="ok"){
				var $filename = data.filename;
				var $a = $("<a>");
				$a.attr("href",data.file);
				$("body").append($a);
				$a.attr("download",$filename+".xlsx");
				$a[0].click();
				$a.remove();
			} 		
		}
	}); 
	
}

$(document).ready(function(){

  
  getDataMcu();

  $('#btn_search').click(function() {
		// table_mcu.ajax.reload();
		getDataMcu()
	});
	
	$('#btn_reset').click(function() {
		$(".opt-filter").val("");
		getDataMcu();
	});
	
	$('#btn_export').click(function() {		
		exportData();
	});

  /* Create */
  $('#btn-upload-mcu').on( 'click', function () {
    	window.location.href = base_url + 'profiling/mcu_upload';
	});


  /* View */
	$('#table_mcu tbody').on( 'click', 'button.btn-view-mcu', function () {
			let mh_id = table_mcu.row( $(this).parents('tr') ).data().mh_id;
    	window.location.href = base_url + 'profiling/mcu_view/' + mh_id;
	});


  /* Edit */
	// $('#table_mcu tbody').on( 'click', 'button.btn-edit-profiling', function () {
	// 		let mh_id = table_mcu.row( $(this).parents('tr') ).data().mh_id;
    // 	window.location.href = base_url + 'profiling/edit/' + mh_id;
	// });

  /* Delete */
	// $('#table_mcu tbody').on( 'click', 'button.btn-delete-profiling', function () {

	// 		let mh_id = table_mcu.row( $(this).parents('tr') ).data().mh_id;

    //   swal({   
    //         title: "Are you sure?",   
    //         text: "You will not be able to recover this record!",   
    //         type: "warning",   
    //         showCancelButton: true,   
    //         confirmButtonColor: "#DD6B55",   
    //         confirmButtonText: "Yes, delete it!",   
    //         closeOnConfirm: true 
    //     }, function(isConfirm){   
    //         if (isConfirm) {     
    //              $.ajax({
	// 					      url       : base_url + 'profiling/delete/',
	// 					      type      : 'DELETE',
	// 					      data        : { mh_id: mh_id },
	// 					      dataType : 'json',
	// 					      success : function(result){
	// 					        if(result.status == 'Success'){
	// 	              		table_mcu.ajax.reload(null, false);
	// 					          toast_notif('success', result.message);
	// 					        }
	// 					        else{
	// 					          toast_notif('error', result.message);
	// 					        }
	// 					      },
	// 								error	: function (xhr, status, error) {
	// 				          toast_notif('error', xhr.responseText);
	// 								}
	// 					    });
    //         }
    //   });
	// });
	/* End delete */

});
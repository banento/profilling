<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile file attribute
     */

    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, pdf, xls, xlsx, doc, docx']
        ];
    }

    public function upload($folder_name=false)
    {

        $uploadPath = \Yii::getAlias('@frontend/web/uploads');

        if($folder_name){
            $uploadPath = $uploadPath . "/" . $folder_name;
        }

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }
        
        $file_name = \Yii::$app->security->generateRandomString() . '.' . $this->file->extension;
        if ( $this->file->saveAs($uploadPath . '/' . $file_name) ) {
            return [
                    'file_name' => $file_name,
                    'file_type' => '.'.$this->file->extension, 
                    'path'=> substr($uploadPath, strpos($uploadPath, 'frontend'))
                 ];
        } else {
            return false;
        }
    }

    public function upload_profile_pic()
    {

        $uploadPath = \Yii::getAlias('@frontend/web/uploads/profile/');

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }
        
        $file_name = \Yii::$app->security->generateRandomString() . '.' . $this->file->extension;
        if ( $this->file->saveAs($uploadPath . '/' . $file_name) ) {
            return [
                    'file_name' => $file_name,
                    'file_type' => '.'.$this->file->extension, 
                    'path'=> substr($uploadPath, strpos($uploadPath, 'frontend'))
                 ];
        } else {
            return false;
        }
    }

    public function doupload($dir)
    {

        $uploadPath = \Yii::getAlias('@frontend/web/uploads/') . $dir;

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }
        
        $file_name = \Yii::$app->security->generateRandomString() . '.' . $this->file->extension;
        if ( $this->file->saveAs($uploadPath . '/' . $file_name) ) {
            return [
                    'file_name' => $file_name,
                    'file_type' => '.'.$this->file->extension, 
                    'path'=> substr($uploadPath, strpos($uploadPath, 'frontend'))
                 ];
        } else {
            return false;
        }
    } 
    
    public function doupload_mcu($dir)
    {

        $uploadPath = \Yii::getAlias('@frontend/web/uploads/') . $dir;

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }
        
        if(strtolower($this->file->extension) == "xls" || strtolower($this->file->extension) == "xlsx") {
            $file_name = \Yii::$app->security->generateRandomString() . '.' . $this->file->extension;
            if ( $this->file->saveAs($uploadPath . '/' . $file_name) ) {
                return [
                        'file_name' => $file_name,
                        'file_type' => '.'.$this->file->extension, 
                        'path'=> substr($uploadPath, strpos($uploadPath, 'frontend'))
                    ];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
<?php 

use yii\helpers\Url;
use yii\web\View;
use common\components\Helper;


?>
  <!-- Bootstrap extend-->
   <?php $this->registerCssFile('@web/css/bootstrap-extend.css'); ?>
  <!-- Bootstrap select -->
  <?php $this->registerCssFile('@web/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css'); ?>
  <!-- Bootstrap tagsinput -->
  <?php $this->registerCssFile('@web/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'); ?>
  <!-- Summernote -->
  <?php $this->registerCssFile('@web/assets/vendor_plugins/summernote/summernote.min.css'); ?>

  <!-- Content Wrapper. Contains page content -->
  <!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->
   <div class="content-header" style="padding-top:60px !important">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="page-title"><?= $this->title ?></h3>
        <div class="d-inline-block align-items-center">
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?= Url::base() . "/home/index" ?>"><i class="mdi mdi-home-outline"></i></a></li>
              <li class="breadcrumb-item"><a href="<?= Url::base(); ?>/news">List News</a></li>
              <li class="breadcrumb-item active" aria-current="page">Edit News</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>  

    <!-- Main content -->
    <section class="content" style="padding-top:20px !important">
      <div class="row">
        <div class="col-12">          

          <div class="box">
            <div class="box-header">
              <h4 class="box-title">Edit News<br>
                <!-- <small>Bootstrap html5 editor</small> -->
              </h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form id="news_form" role="form">
                <div class="form-group">
                  <label class="font-size-16">Title</label>
                  <input type="hidden" class="form-control" name="news_id" id="news_id" value="<?= $news_id ?>">
                  <input type="text" class="form-control" name="news_title" id="news_title" value="<?= $news_title ?>" required>
                </div>
                <div class="form-group m-b-20">
                  <label class="font-size-16">Contents</label>
                  <textarea id="news_content" name="news_content" required><?= $news_content ?></textarea>
                </div>

                <div class="form-group m-b-20">
                  <label class="font-size-16">Tag</label>
                  <div class="tags-default">
                    <input type="text" value="<?= $news_tag ?>" data-role="tagsinput" placeholder="add tags" name="news_tag" id="news_tag" required/>
                  </div>
                </div>

                <div class="form-group m-b-20">
                  <label class="font-size-16">Img/Thumbnail</label>
                  <div id="old_img" class="old_img">
                    <?php
                      $path = \Yii::getAlias('@webroot');
                      if(@file_exists($path."/".$news_img)){
                        $news_img = Url::base() . "/" . $news_img;
                      }else{
                        $news_img = "https://cdn.pixabay.com/photo/2017/06/10/07/22/news-2389226_1280.png";
                      }
                    ?>
                    <img src="<?= $news_img ?>" class="img-fluid" alt="News image" style="max-height: 100px;width: auto;object-fit:cover;"/>
                    <a href="javascript:void(0)" class="btn btn-info btn-outline btn-sm d-block w-150 mt-10" id="change_img">Change Image</a>
                  </div>  
                  <div id="upd_img" class="controls d-none">
                    <input type="file" name="news_img" id="news_img" class="form-control" accept=".jpg,.png"aria-invalid="false"> <div class="help-block"></div>
                    <a href="javascript:void(0)" class="btn btn-warning btn-outline btn-sm d-block w-150 mt-20" id="discard_change">Discard Change</a>
                  </div>
                </div>
              <div class="pt-20">
                <button type="submit" class="btn btn-success btn-outline">
                  <i class="ti-save-alt"></i> Save
                </button>
                <a href="<?= Url::base(); ?>/news/<?= $news_id?>" class="btn btn-warning btn-outline mr-1">
                  <i class="ti-trash"></i> Discard
                </a>
              </div>
              </form>
            </div>
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->



<input type="hidden" name="base_url" id="base_url" value="<?= Url::base(true) ?>/">


	<!-- popper -->
	<?php $this->registerJsFile('@web/assets/vendor_components/popper/dist/popper.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

  <!-- Bootstrap 4.1-->
  <?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

  <!-- Summmernote -->
  <?php $this->registerJsFile('@web/assets/vendor_plugins/summernote/summernote.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

  <!-- Bootstrap Select -->
  <?php $this->registerJsFile('@web/assets/vendor_components/bootstrap-select/dist/js/bootstrap-select.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  <!-- Bootstrap tagsinput -->
  <?php $this->registerJsFile('@web/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  <?php $this->registerJsFile('@web/js/news_edit.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

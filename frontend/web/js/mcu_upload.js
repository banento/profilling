const base_url = $("#base_url").val();

$(document).ready(function(){	

		/* Ajax Save */
	$('#mcu_form').on('submit', function(e) {
	  if (!e.isDefaultPrevented()) {
	  	let form_data = new FormData($('#mcu_form')[0]);
	    $.ajax({
	      url       : base_url+ "profiling/mcusave",
	      type      : 'POST',
	      enctype   : 'multipart/form-data',
    		data: form_data,
			dataType 		: 'json',
		    beforeSend  : function(){
			    								customLoading('show');
			                  },
			processData: false,
			contentType: false,
			success	: function(result){
				if(result.status == 'Success'){
					customLoading('hide');
				toast_notif('success', result.message);
				setTimeout(function() {
					window.location.href = base_url + 'profiling/mcu';
					}, 1500);
				}
				else{
				toast_notif('error', result.message);
				}				
			},
			error	: function (xhr, status, error) {
				console.log(error)
      			toast_notif('error', "Proses gagal");
			}
	    });
	  }
	  e.preventDefault();
	});


	
	$("#file_mcu").on("change", ()=>{	
		
		var filenya = $("#file_mcu")[0].files[0];
		var fname = '', fsize = 0, extnya = '';
		var arrExt 	= [".xls", ".xlsx"];
	
		if(typeof(filenya) == 'undefined'){
			console.log('undefined')
		}else{
			fname 	= filenya.name;
			fsize	= (filenya.size / 1024 / 1024).toFixed(2);
			extnya 	= fname.substr(fname.lastIndexOf(".")).toLowerCase();
		
			if(fname && $.inArray(extnya, arrExt) == -1){
				$("#file_mcu").val("");
				toast_notif('warning', "File type not allowed");			
			} 
			
			// if(fsize > 5120){ // 5mb		
			if(fsize > 5){ // 5mb
				$("#file_mcu").val("");
				toast_notif('warning', "Maximal size 5mb");			
			}
		}
		

	})



	
});
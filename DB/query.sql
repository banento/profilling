CREATE TABLE `profiling`.`profiling`  (
  `profiling_id` int NOT NULL AUTO_INCREMENT,
  `nik` varchar(20) NULL,
  `dignosa` varchar(100) NULL,
  `notes` text NULL,
  `attachment` text NULL,
  PRIMARY KEY (`profiling_id`)
);

ALTER TABLE `profiling`.`profiling` 
ADD COLUMN `user_created` varchar(20) NULL AFTER `attachment`,
ADD COLUMN `date_created` datetime NULL AFTER `user_created`,
ADD COLUMN `user_updated` varchar(20) NULL AFTER `date_created`,
ADD COLUMN `date_updated` datetime NULL AFTER `user_updated`,
ADD COLUMN `status` int NULL DEFAULT 1 COMMENT '0 = non aktif, 1 = aktif' AFTER `date_updated`;

ALTER TABLE `profiling`.`profiling` 
CHANGE COLUMN `user_created` `created_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `attachment`,
CHANGE COLUMN `date_created` `created_date` datetime(0) NULL DEFAULT NULL AFTER `created_by`,
CHANGE COLUMN `user_updated` `updated_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `created_date`,
CHANGE COLUMN `date_updated` `updated_date` datetime(0) NULL DEFAULT NULL AFTER `updated_by`,
ADD COLUMN `created_ip` varchar(150) NULL AFTER `created_date`,
ADD COLUMN `updated_ip` varchar(150) NULL AFTER `updated_date`;


ALTER TABLE `profiling`.`profiling` 
CHANGE COLUMN `dignosa` `diagnosa` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `nik`;

--gak dipake-------------
CREATE TABLE `profiling`.`m_disease_header`  (
  `m_disease_h_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NULL,
  `notes` text NULL,
  `status` int NULL DEFAULT 1,
  PRIMARY KEY (`m_disease_h_id`)
);

--gak dipake-------------
CREATE TABLE `profiling`.`m_disease_detail`  (
  `m_disease_d_id` int NOT NULL AUTO_INCREMENT,
  `m_disease_h_id` int NULL,
  `name_detail` varchar(255) NULL,
  `notes_detail` varchar(255) NULL,
  `status` int NULL DEFAULT 1,
  PRIMARY KEY (`m_disease_d_id`)
);

ALTER TABLE `profiling`.`m_disease_detail` 
ADD CONSTRAINT `m_disease_d_fk1` FOREIGN KEY (`m_disease_h_id`) REFERENCES `profiling`.`m_disease_header` (`m_disease_h_id`) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `profiling`.`m_disease`  (
  `disease_id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NULL,
  `name` varchar(200) NULL,
  `notes` text NULL,
  `status` int NULL DEFAULT 1,
  PRIMARY KEY (`disease_id`)
);

CREATE TABLE `profiling`.`profiling_disease`  (
  `pd_id` int NOT NULL AUTO_INCREMENT,
  `nik` varchar(50) NULL,
  `disease_id` int,  
  PRIMARY KEY (`pd_id`)
);

ALTER TABLE `profiling`.`profiling`ADD INDEX(`nik`);

ALTER TABLE `profiling`.`profiling_disease` 
ADD CONSTRAINT `profiling_diseases_fk1` FOREIGN KEY (`nik`) REFERENCES `profiling`.`profiling` (`nik`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `profiling`.`profiling_disease` 
ADD COLUMN `other` text NULL AFTER `disease_id`;

CREATE VIEW `profiling`.`v_profiling` AS SELECT  a.*,b.nama,
	(
	SELECT GROUP_CONCAT(COALESCE(c.other, d.`name`) SEPARATOR ', ')  as name_disease from profiling_disease c 
	LEFT JOIN m_disease d on c.disease_id=d.disease_id
	where c.nik=a.nik
	) as profiling_disease
	 from profiling a left join employee b on a.nik=b.nik;


CREATE OR REPLACE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `profiling`.`v_profiling` AS SELECT  a.*,b.nama, b.title, b.organization, b.job, b.email, b.department, b.division, b.job_category, b.area,
b.directorate, b.employee_category,
	(
	SELECT GROUP_CONCAT(COALESCE(c.other, d.`name`) SEPARATOR ', ')  as name_disease from profiling_disease c 
	LEFT JOIN m_disease d on c.disease_id=d.disease_id
	where c.nik=a.nik
	) as profiling_disease
	 from profiling a left join employee b on a.nik=b.nik ;


-- 11-02-2022---------------------------------------------------------

CREATE TABLE `profiling`.`profiling_history`  (
  `profiling_id_h` int NOT NULL,
  `nik` varchar(20) NULL,
  `notes` text NULL,
  `attachment` text NULL,
  `created_by` varchar(20) NULL,
  `created_date` datetime NULL,
  `status` int NULL COMMENT '0 = non aktif, 1 = aktif',
  PRIMARY KEY (`profiling_id_h`)
);


CREATE TABLE `profiling`.`profiling_disease_history`  (
  `pd_id_h` int NOT NULL,
  `profiling_id_h` int NULL,
  `nik` varchar(20) NULL,
  `disease_id` int NULL,
  `name_disease` varchar(255) NULL,
  `other` text NULL,
  PRIMARY KEY (`pd_id_h`)
);

ALTER TABLE `profiling`.`profiling_history` 
MODIFY COLUMN `profiling_id_h` int NOT NULL AUTO_INCREMENT FIRST,
MODIFY COLUMN `status` int(11) ZEROFILL NULL DEFAULT 1 COMMENT '0 = non aktif, 1 = aktif' AFTER `created_date`;

ALTER TABLE `profiling`.`profiling_disease_history` 
MODIFY COLUMN `pd_id_h` int NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE `profiling`.`profiling_history` 
MODIFY COLUMN `created_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'dari data utama' AFTER `attachment`,
MODIFY COLUMN `created_date` datetime NULL DEFAULT NULL COMMENT 'dari data utama' AFTER `created_by`,
ADD COLUMN `created_by_insert` varchar(20) NULL AFTER `status`,
ADD COLUMN `create_date_insert` datetime NULL AFTER `created_by_insert`;

--14-02-2022----------------------------------------

CREATE TABLE `profiling`.`mcu_header`  (
  `mh_id` int NOT NULL AUTO_INCREMENT,
  `filename_ori` varchar(255) NULL,
  `filname_random` varchar(255) NULL,
  `description` text NULL,
  `created_by` varchar(20) NULL,
  `created_date` datetime NULL,
  `created_ip` varchar(150) NULL,
  PRIMARY KEY (`mh_id`)
);

CREATE TABLE `profiling`.`mcu_detail`  (
  `md_id` int NOT NULL AUTO_INCREMENT,
  `mh_id` int NULL,
  `no_lab` varchar(100) NULL,
  `nama` varchar(250) NULL,
  `nik` varchar(20) NULL,
  `tanggal_lahir` date NULL,
  `jenis_kelamin` varchar(50) NULL,
  `tanggal_mcu` varchar(255) NULL,
  `hemoglobin` decimal NULL,
  `eritrosit` decimal NULL,
  `hematokrit` decimal NULL,
  PRIMARY KEY (`md_id`)
);

ALTER TABLE `profiling`.`mcu_detail` 
ADD CONSTRAINT `mcu_d_fk1` FOREIGN KEY (`mh_id`) REFERENCES `profiling`.`mcu_header` (`mh_id`) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE `profiling`.`m_column_import_mcu`  (
  `mc_id` int NOT NULL AUTO_INCREMENT,
  `code` int NULL,
  `name` varchar(150) NULL,
  `description` varchar(250) NULL,
  PRIMARY KEY (`mc_id`)
);

ALTER TABLE `profiling`.`m_column_import_mcu` 
CHANGE COLUMN `name` `column` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `code`;


ALTER TABLE `profiling`.`mcu_detail` 
MODIFY COLUMN `hemoglobin` decimal(10, 2) NULL DEFAULT NULL AFTER `tanggal_mcu`,
MODIFY COLUMN `eritrosit` decimal(10, 2) NULL DEFAULT NULL AFTER `hemoglobin`,
MODIFY COLUMN `hematokrit` decimal(10, 2) NULL DEFAULT NULL AFTER `eritrosit`,
ADD COLUMN `mcv` decimal(10, 2) NULL AFTER `hematokrit`,
ADD COLUMN `mch` decimal(10, 2) NULL AFTER `mcv`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `mchc` decimal(10, 2) NULL AFTER `mch`,
ADD COLUMN `leukosit` decimal(10, 2) NULL AFTER `mchc`,
ADD COLUMN `basofil` decimal(10, 2) NULL AFTER `leukosit`,
ADD COLUMN `eosinofil` decimal(10, 2) NULL AFTER `basofil`,
ADD COLUMN `batang` decimal(10, 2) NULL AFTER `eosinofil`,
ADD COLUMN `segmen` decimal(10, 2) NULL AFTER `batang`,
ADD COLUMN `limfosit` decimal(10, 2) NULL AFTER `segmen`,
ADD COLUMN `monosit` decimal(10, 2) NULL AFTER `limfosit`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `trombosit` decimal(10, 2) NULL AFTER `monosit`,
ADD COLUMN `led` decimal(10, 2) NULL AFTER `trombosit`,
ADD COLUMN `warna_makroskopis` varchar(75) NULL AFTER `led`,
ADD COLUMN `kejernihan_makroskopis` varchar(75) NULL AFTER `warna_makroskopis`,
ADD COLUMN `berat_jenis_urinalisis` decimal(10, 2) NULL AFTER `kejernihan_makroskopis`,
ADD COLUMN `leukosit_esterase` varchar(50) NULL AFTER `berat_jenis_urinalisis`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `nitrit` varchar(50) NULL AFTER `leukosit_esterase`,
ADD COLUMN `ph_urinalisis` decimal(10, 2) NULL AFTER `nitrit`,
ADD COLUMN `protein` varchar(50) NULL AFTER `ph_urinalisis`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `glukosa` varchar(50) NULL AFTER `protein`,
ADD COLUMN `keton` varchar(50) NULL AFTER `glukosa`,
ADD COLUMN `urobilinogen` decimal(10, 2) NULL AFTER `keton`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `bilirubin` varchar(50) NULL AFTER `urobilinogen`,
ADD COLUMN `eritrosit_darah_samar` varchar(50) NULL AFTER `bilirubin`,
ADD COLUMN `eritrosit_urinalisis` decimal(10, 2) NULL AFTER `hemoglobin`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `leukosit__urinalisis` decimal(10, 2) NULL AFTER `eritrosit_urinalisis`,
ADD COLUMN `silinder` varchar(50) NULL AFTER `leukosit__urinalisis`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `kristal` varchar(50) NULL AFTER `silinder`,
ADD COLUMN `epitel` varchar(75) NULL AFTER `kristal`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `bakteri` varchar(50) NULL AFTER `epitel`,
ADD COLUMN `lain_lain_urinalisis` varchar(100) NULL AFTER `bakteri`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `sgot_faal_hati` decimal(10, 2) NULL AFTER `lain_lain_urinalisis`,
ADD COLUMN `sgpt_faal_hati` decimal(10, 2) NULL AFTER `sgot_faal_hati`,
ADD COLUMN `ureum` decimal(10, 2) UNSIGNED NULL AFTER `sgpt_faal_hati`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `kreatinin` decimal(10, 2) NULL AFTER `ureum`,
ADD COLUMN `asam_urat` decimal(10, 2) NULL AFTER `kreatinin`,
ADD COLUMN `gdp_diabetes` decimal(10, 2) NULL AFTER `asam_urat`,
ADD COLUMN `kolesterol_total` decimal(10, 2) NULL AFTER `gdp_diabetes`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `trigliserida` decimal(10, 2) NULL AFTER `kolesterol_total`,
ADD COLUMN `hdl` decimal(10, 2) NULL AFTER `trigliserida`,
ADD COLUMN `ldl` decimal(10, 2) NULL AFTER `hdl`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `hbsag` varchar(75) NULL AFTER `ldl`,
ADD COLUMN `anti_hbs` varchar(75) NULL AFTER `hbsag`,
ADD COLUMN `cea` varchar(75) NULL AFTER `anti_hbs`;

ALTER TABLE `profiling`.`mcu_detail` 
ADD COLUMN `psa` varchar(75) NULL AFTER `cea`,
ADD COLUMN `hba1c` varchar(75) NULL AFTER `psa`,
ADD COLUMN `ca_125` varchar(75) NULL AFTER `hba1c`,
ADD COLUMN `ekg` varchar(75) NULL AFTER `ca_125`;

ALTER TABLE `profiling`.`profiling_history` 
MODIFY COLUMN `status` int(11) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '0 = non aktif, 1 = aktif' AFTER `created_date`;

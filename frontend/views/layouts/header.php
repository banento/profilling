<?php 
use yii\helpers\Url;
$sess = Yii::$app->session;
$sess_img = $sess->get('profile_picture');
$sess_nik = $sess->get('nik');
$default_img = Url::base() .'/images/user-info.jpg';

/*if(file_exists($default_img)){
	$profile = $default_img;
}*/
if (@file_exists(Url::base() . "/images/".$sess_img)) {
	$profile = $default_img;
}else{
	$profile = $sess_img;
}
?>
  <header class="main-header" style="background:#fff">
<!-- 
  	<nav id="w0" class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
		</nav> -->
    <!-- Logo -->
    <a href="<?= Url::toRoute('home/index') ?>" class="logo">
      <!-- mini logo -->
	  <!-- <div class="logo-mini">
		  <span class="light-logo"><img src="<?= Url::base(); ?>/images/logo-light.png" alt="logo"></span>
		  <span class="dark-logo"><img src="<?= Url::base(); ?>/images/logo/telkomsel.png" alt="logo" width="50" height="50"></span>
	  </div> -->
      <!-- logo-->
      <div class="logo-lg">
		  <span class="light-logo"><img src="<?= Url::base(); ?>/images/logo-light-text.png" alt="logo"></span>
	  	  <span class="dark-logo"><img src="<?= Url::base(); ?>/images/logo/logo_telkomsel.png" alt="logo" width="155"></span>
	  </div>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
	  <div>
		  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		  </a>
      <!-- <ul class="navbar" style="list-style-type: none;">
        <li class="navbar-item"><a class="nav-link" style="color:white;" href="<?= Url::toRoute('home/index')?>">HAI DOC</a></li>
        <li class="navbar-item"><a class="nav-link" style="color:white;" href="<?= Url::toRoute('news/index')?>">NEWS</a></li>
      </ul> -->
	  </div>
      <div class="navbar-custom-menu">

        <ul class="nav navbar-nav">
		  
		
      <!-- Messages -->
      <!-- <li class="dropdown messages-menu"> -->
     
		  <!-- User Account-->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img id="user-image" src="<?= $profile ?>" class="user-image rounded-circle" alt="User Image" style="object-fit: cover;">  <!-- add by badar -->
            </a>
            <ul class="dropdown-menu animated flipInY">
              <!-- User image -->
              <li class="user-header bg-img" style="background-image: url(<?= $profile ?>)" data-overlay="3">
				  <div class="flexbox align-self-center">					  
				  	<img src="<?= $profile ?>" class="float-left rounded-circle" alt="User Image" style="object-fit: cover;position: absolute;height:50px;width:50px">					  
					<h4 class="user-name align-self-center">
					  <span><?= $sess['nama'] ?></span>
					  <small><?= $sess['email'] ?></small>
					</h4>
				  </div>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
				    <!-- <a class="dropdown-item" href="javascript:void(0)"><i class="ion ion-person"></i> My Profile</a>
					<div class="dropdown-divider"></div> -->
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="<?= Url::toRoute('login/signout') ?>"><i class="ion-log-out"></i> Logout</a>
					<div class="dropdown-divider"></div>
              </li>
            </ul>
          </li>				  
        </ul>
      </div>
    </nav>
  </header>
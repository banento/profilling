<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\components\Ldap;
use common\components\Users;
use common\models\User;
use common\components\Helper;
/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;

    private $success = 'success';
    private $failed = 'failed';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */

    public function login_new()
    {
        // conn LDAP
        // $data = Ldap::login($this->username, $this->password);
        // $check = (array) $data;

        //start data dummy==============
        $check['nama'] = 'Maxi_D_Turangan';
        $check['nik'] = 'T219012';
        $check['email'] = 'maxi_d_turangan@telkomsel.co.id';
        $check['posisi'] = 'Senior Medical and Health Services Consultant';
        $data = (object) ($check);
        //end data dummy==============


        $res["status"] = $this->failed;
        $res["message"] = "Gagal";
        $res["data"] = [];
        if(!empty($check)){
            //login by ldap
            if(!empty($data->nik)){
                $user = Employee::findOne(['username'=>$this->username]);                
                if(empty($user))
                {
                    $res["status"] = $this->success;
                    $res["message"] = "Berhasil login LDAP, tapi data employee tidak ditemukan";      
                    $res["data"] = $check;             
                    
                }else{
                    $send['person_id']= $user['person_id'];
                    $send['nik']= $data->nik;
                    $send['nama']= $user['nama'];
                    $send['email']= $data->email;
                    $send['posisi']= $data->posisi;               
                    $send['job']= $user['job'];
                    $send['department']= $user['department'];
                    $send['division']= $user['division'];
                    $send['area']= $user['area'];
                    $send['status']= $user['status'];

                    $res["data"] = $send;
                    $res["status"] = $this->success;
                    $res["message"] = "Berhasil login";  
                }
                             
            }else{
                $res["message"] = "Employee NIK Data not found.";  
            }
        }else{
            //validate password because not ldap account/incorrent ldap
            if(!YII_ENV_PROD)
            {
               $res["message"] = "Incorrect username or password.";  
                
            } else {

                $res["message"] = "Please input your domain (desktop/laptop) account."; 

            }
        }
        return $res;
    }


    public function login()
    {
        $data = Ldap::login($this->username, $this->password);
        $check = (array) $data;

            //start data dummy==============
            // $check['nama'] = 'Maxi_D_Turangan';
            // $check['nik'] = 'T219012';
            // $check['email'] = 'maxi_d_turangan@telkomsel.co.id';
            // $check['posisi'] = 'Senior Medical and Health Services Consultant';
            // $data = (object) ($check);
            // end data dummy==============

        $res["status"] = $this->failed;
        $res["message"] = "Gagal";
        $res["data"] = [];
        if(!empty($check)){
            //login by ldap
            if(!empty($data->nik)){
                $user = User::findOne(['username'=>$this->username]);
                if(empty($user))
                {
                    //check by nik 
                    $checkUser = User::findOne(['nik' => $data->nik]);
                    if(!empty($checkUser)){
                        $user = User::findOne(['nik' => $data->nik]);
                        $user->id = $this->username;
                        $user->username = $this->username;
                        $user->email = $data->email;
                    }else{
                        //add new user ldap
                        $user = new User;
                        $user->id = $this->username;
                        $user->nik = $data->nik;
                        $user->username = $this->username;
                        $user->generateAuthKey();
                        $user->generatePasswordResetToken();
                        $user->setPassword($this->generatePass());
                        $user->status = User::STATUS_ACTIVE;
                        $user->email = $data->email;
                        $user->password_hash = Yii::$app->security->generatePasswordHash($this->generatePass());
                        $user->auth_key = $data->nik;
                    }
                    
                    
                    //add to auth assignment if job category dir, vp, gm
                    if($user->save()){
                        //get data employee
                        $emp = Employee::find()->where(['nik' => $user->nik])->one();
                        if(!empty($emp)){
                            //cek job category 
                            if(
                                $emp->job_category == "Board Of Director" || 
                                $emp->job_category == "Executive Vice President" || 
                                $emp->job_category == "Senior Vice President" || 
                                $emp->job_category == "Vice President" || 
                                $emp->job_category == "General Manager" 
                            ){
                                //check apakah datanya sudah ada di auth_assignment
                                $auth = AuthAssignment::find()->where(['user_id' => $user->username])->one();
                                if(empty($auth)){
                                    if($emp->job_category == "Board Of Director"){
                                        $item_name = "directorate";
                                    }elseif($emp->job_category == "Executive Vice President" || $emp->job_category == "Senior Vice President"){
                                        $item_name = "svp_evp";
                                    }elseif($emp->job_category == "Vice President" && ($emp->supervisor->job_category == "Executive Vice President" || $emp->supervisor->job_category == "Senior Vice President")){
                                        $item_name = "vp_under_sevp";
                                    }elseif($emp->job_category == "Vice President"){
                                        $item_name = "vp_under_directorate";
                                    }else{
                                        $item_name = "gm";
                                    }

                                    //add ke auth assignment
                                    $auth_data = new AuthAssignment();
                                    $auth_data->item_name = $item_name;
                                    $auth_data->user_id = $user->username;
                                    $auth_data->save();
                                }
                            }
                        }

                    }
                    
                }else{
                    //update nik
                    $user->nik = $data->nik;
                    $user->save();

                }
             
                // return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
               
                $user = $this->getUser();                       
                $send['nik']= $user->nik;
                $send['nama']= $user->username;
                $send['auth_key']= $user->auth_key;
                $send['password_hash']= $user->password_hash;               
                $send['password_reset_token']= $user->password_reset_token;               
                $send['email']= $user->email;   
                $send['picture']= Helper::PICTURE_URL_NIK.$user->nik;   

                $res["status"] = $this->success;
                $res["message"] = "Login LDAP Berhasil";
                $res["data"] = $send;

                $session = Yii::$app->session;
                $session->set('nik', $user->nik);
            }else{
                // Yii::$app->session->setFlash('error-notif', "Employee NIK Data not found."); 
                $res["status"] = $this->failed;
                $res["message"] = "Employee NIK Data not found.";
               
            }
        }else{
            //validate password because not ldap account/incorrent ldap
            if(!YII_ENV_PROD)
            {
                $user = $this->getUser();
                if (!$user || !$user->validatePassword($this->password)) {
                    // Yii::$app->session->setFlash('error-notif', "Incorrect username or password."); 
                    $res["message"] = "Incorrect username or password.";                    
                }else{
                    return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
                }
            } else {
                // Yii::$app->session->setFlash('error-notif', "Please input your domain (desktop/laptop) account."); 
                // return false;
                $res["message"] = "Please input your domain (desktop/laptop) account.";  
            }
        }

        return $res;
    }


    public function loginWeb($data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $rememberMe = (isset($data['rememberMe'])?$data['rememberMe']:'');
        
        $user = User::getUserEmployee($username);
        if($user){
           $validatepass = Yii::$app->getSecurity()->validatePassword($password, $user[0]['password_hash']);
           if($validatepass){
                //save cookies
                if(isset($data['rememberMe'])){
                     setcookie ("loginId", $user[0]['nik'], time()+ (365 * 24 * 30)); 
                     setcookie ("isremember", $data['rememberMe'], time()+ (365 * 24 * 30)); 
                }
                else{
                    setcookie ("loginId",""); 
                }

                //save session
                $session = Yii::$app->session;
                $sess_data = ['username'=>$user[0]['username'],'nama'=>$user[0]['nama'],'nik'=>$user[0]['nik'],'isLoggedin'=>1,'is_doctor'=>$user[0]['is_doctor'],'email'=>$user[0]['email'],'profile_picture'=>($user[0]['profile_picture']!=''? 'uploads/profile/'.$user[0]['profile_picture'] :'')];
                foreach($sess_data as $k=>$v){
                    $session->set($k,$v);
                }
                Yii::$app->session->setFlash('success', "Hello, ".$user[0]['nama']." welcome!.");
                return true;
           }    
        }

        return false;

    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    protected function generatePass(){
        $length = 5;
        $characters = '123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

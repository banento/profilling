<?php 
use yii\helpers\Url;
$this->registerCssFile('@web/assets/vendor_components/datatable/datatables.min.css'); 
?>

<div class="row">
    <div class="col-12">
        <div class="box">   
            <div class="box-body">
                <div class="table-responsive">
                    <table id="table_employee" class="datatables table-bordered table-striped table-hover compact">
                    <thead>
                        <tr>
                            <th>PERSON ID</th>
                            <th>NIK</th>
                            <th>NAMA</th>
                            <th>AREA</th>
                            <th>ACTIONS</th>
                        </tr>
                    </thead>
                    </table>
                </div>
            </div>        
        </div>
    </div>
</div>

<?php $this->registerJsFile('@web/assets/vendor_components/datatable/datatables.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

<script>
var table_employee = null;
var base_url_emp = "<?= Url::base(true) ?>/";
$(document).ready(function(){
   
    if(table_employee != null){
	  table_employee.destroy();
	}
	table_employee = $('#table_employee').DataTable( {
	  "processing": true,
	  "serverSide": true,
	  "ajax": {
				"url": base_url_emp + "profiling/listemployee",
				"type": "GET",
	  },
	  "language"     : {
	      "emptyTable"  : "<span class ='label label-danger'>Data not found!</span>",  
	      "infoEmpty"   : "Data Empty",
	      "processing"  : '<div class="loader vertical-align-middle loader-circle"></div>',
	      "search"      : "_INPUT_"
	  },
	  "columns": [	       
	        { "data": "person_id", "width": "5%", "class":"text-center"},
	        { "data": "nik"},
	        { "data": "nama"},
	        { "data": "area"},
	        { 
	          "data": "nik",
	          "width": "20%",
	          "class":"text-center",
	          "render": function (data) {
	          	return `
	          			<button alt="1" class="btn-choice-employee btn btn-info waves-effect waves-l ight btn-xs" title="Click to select ${data}"><i class="fa fa-search"> Choose</i></button>`;
	          }
	        }
	  ],
	  "drawCallback": function( settings, start, end, max, total, pre ) {
            $(".DTFC_LeftBodyLiner").css('overflow','hidden');
	  },
	  "paging":         true,
	  "columnDefs": [
	      {
	      	"targets": [ 1, 3],
	        "orderable": false

	      },
	      {
	          "targets": [ 0],
	          "visible": false
	      }
	  ],
	  "scrollY"          : 520, 
	  "scrollCollapse"   : true,
	  "scrollX"         : false,
      "bAutoWidth" : true

    });

	$('input[type="search"]').attr('placeholder','Search here...').addClass('form-control input-sm m-0');


	$('#table_employee tbody').on( 'click', 'button.btn-choice-employee', function () {
			let data = table_employee.row( $(this).parents('tr') ).data();			
			set_choice(data);			
	})
	
	$('#table_employee tbody').on('dblclick', 'tr', function () {
		var data = table_employee.row( this ).data();
		set_choice(data);
	} );

	function set_choice(data)
	{
		$("#employee_h").val(data.nik);
		$("#name_employee").val(data.nama);
		$("#modal_dialog").modal("hide");
	}

})
</script>


<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    // require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php'
    // require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
                    'log',
                    'gii'
                ],
    'modules'=>[
                'gii'=>[
                    'class' => 'yii\gii\Module',
                ],
                'yii2Emoji'=>[
                    'emojiSize'=>'32',//32、64、128
                ]
            ],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute'=>'login',
    'layout'=>'template',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
			'cookieValidationKey' => 'iq-fFcdN45CvU84EyH2FjEdEo3T-68lA',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => array(
                // 'GET api/<controller:\w+>/<id:\d+>' => 'api/<controller>/<action>',
                // 'PUT,POST,DELETE api/<controller:\w+>/<action:\w+>' => 'api/<controller>/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
        
    ],
    'params' => $params,
];
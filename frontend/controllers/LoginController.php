<?php

namespace frontend\controllers;

use common\components\Users;
use Yii;
use common\models\LoginForm;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\User;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class LoginController extends Controller
{
    public $layout = "template-login";
    public $title = "Profiling - LOGIN";

    public function actionIndex()
    {

        // echo 'hai';die;
        Yii::$app->view->title = $this->title;
        return $this->render('index');
    }

    public function actionSignin()
    {
        $model = new LoginForm();
        $data = Yii::$app->request->post();

        if ($model->loginWeb($data)) {
            return $this->redirect(['home/index']);
        }
        else{
            Yii::$app->session->setFlash('error', "Error, Incorrect Username or Password!.");
            return $this->redirect(['login/index']);
        }
    }
    

    public function loginWeb($data)
    {
        $username = $data['username'];
        $password = $data['password'];
        $rememberMe = (isset($data['rememberMe'])?$data['rememberMe']:'');
        $url_api = \Yii::$app->params['url_api'];
        $client = new Client();
        $r = $client->request('POST', $url_api.'/api/login', [
            'json' => ['username' => $username,'password' => $password]
        ]);     
        $response = $r->getBody()->getContents();      
        $res = json_decode($response,true);
     
        if(isset($res['status']) && $res['status']=="success"){
           
            if($res['data']){
                $user = $res['data'];           
                //save cookies
                if($rememberMe){
                        setcookie ("loginId", $user['nik'], time()+ (365 * 24 * 30)); 
                        setcookie ("isremember", $data['rememberMe'], time()+ (365 * 24 * 30)); 
                }
                else{
                    setcookie ("loginId",""); 
                }

                // modify by badar
                
                $doctor = User::findOne(['nik' => $user['nik']]);
                $is_doctor = $doctor['is_doctor'];
                //save session
                $session = Yii::$app->session;
                $sess_data = [ 'username' => $data['username'],
                                'nama' => $user['nama'],
                                'nik'=> $user['nik'],
                                'isLoggedin' =>1,
                                'is_doctor' => $is_doctor,
                                'email' => $user['email'],
                                'profile_picture' => ($doctor['profile_picture']!=''? /*'uploads/profile/'.*/$doctor['profile_picture'] :'')
                            ];
                foreach($sess_data as $k=>$v){
                    $session->set($k,$v);
                }
                Yii::$app->session->setFlash('success', "Hello, ".$user['nama']." welcome!.");
                return true;
                // end modify by badar
                
            }

        } else {
            return false;
        }



    }

    public function actionSignout()
    {
        $session = Yii::$app->session;

        $session->removeAll();
        
        $session->destroy();

        Yii::$app->session->setFlash('success', "Sign out success!.");
        return $this->redirect(['login/index']);
    }

    public function actionPasshash()
    {
        $get = Yii::$app->request->get();
        $pass = Yii::$app->getSecurity()->generatePasswordHash($get['pass']);
        echo $pass;
    }

}

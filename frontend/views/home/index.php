<?php 

use yii\helpers\Url;
use yii\web\View;
use common\components\Helper;

?>

  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->

  <div class="content-header" style="padding-top:60px !important">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="page-title"><?= $this->title ?></h3>
        <div class="d-inline-block align-items-center">
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>            
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>

    <!-- Main content -->
     <section class="content" style="padding-top:20px !important">

     <div class="container">
      <div class="row">
        <div class="col-12">

            <!-- Default box -->
            <?php if($news){
                foreach ($news as $key=>$val){
              ?>
            

            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"><?= $val['news_title'] ?></h3>              
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-sm-3">
                    <?php if($val['news_img']): ?>
                      <img src="<?= Url::base(); ?>/<?=  ($val['news_img']) ? substr($val['news_img'], strpos($val['news_img'], 'uploads')) : ''; ?>" class="img-fluid" alt="" /></div>
                    <?php else: ?>
                    <img src="https://cdn.pixabay.com/photo/2017/06/10/07/22/news-2389226_1280.png" class="img-fluid" alt="" />
                    <?php endif; ?>
                  <div class="col-sm-9">
                    <div id="news_content" style="font-weight:600;font-style: italic;">
                      <?php
                      $jml_show = 800;
                      $link = "news/view/".$val['news_id'];
                      $length = strlen($val['news_content']);
                      $content = $val['news_content'];
                      $content = preg_replace("/<img[^>]+\>/i", "", $content);
                      $content = strip_tags($content);

                      $length = strlen($content);

                      if($length>$jml_show){
                        echo substr($content,0,$jml_show);
                      } else {
                        echo $content;
                      }
                      ?>
            

                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 pt-20">
                    <a href="<?= Url::base(); ?>/<?= $link?>" class="btn btn-info pull-right">Selengkapnya</a>
                  </div>
                </div>
               
              </div>         
              <div class="box-footer">
                <!-- Footer -->
              </div>
              
            </div>
            <?php 
            }
          }
          ?>
            <!-- /.box -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->

<input type="hidden" name="base_url" id="base_url" value="<?= Url::base(true) ?>/">

<!-- popper -->
<?php $this->registerJsFile('@web/assets/vendor_components/popper/dist/popper.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

<!-- Bootstrap 4.0-->
<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>


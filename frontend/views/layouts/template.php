<?php
use yii\bootstrap4\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<head>
	<base href="<?= Url::base(true) ?>">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/x-icon" href="<?= Url::base(); ?>/images/logo/favicon.ico">
    <?php $this->head() ?>
    <style type="text/css">
        html, body {
          font-family: 'Source Sans Pro', sans-serif;
        }
        .content{
            padding: 0px !important;
        }
        .avatar{
            width: 50px !important;
            height: 50px !important;
        }
        .media{
            padding: 0.75rem !important;
        }
        .direct-chat-text{
            margin: 5px 0 0 0 !important;
        }
        .slimScrollDiv{
            height: 380px !important;
        }
        #chat-app{
            height: 380px !important;
        }
		.main-footer{
			margin-left: 0px !important;
		}
        .blink_notif {
            animation: blinker 2s linear infinite;
        }
        .navbar-static-top{
            background-color: #dd4b39 !important;
        }

        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }
    </style>
</head>
<?php $this->beginBody() ?>
<body class="hold-transition skin-info-light fixed sidebar-mini">
<div class="wrapper">    
    <!-- header  -->
    <?php require('header.php') ?>
    
    <!-- aside and menus -->
    <?php  require('aside.php') ?>

    
    <!-- Content Wrapper. Contains page content -->
    <!-- <div class="content-wrapper" style="min-height:607px !important;"> -->
    <!-- Update full content -->
    <div class="content-wrapper">
        <div class="content">
    
    <!-- Navbar -->
    <?php // require('navbar.php') ?>


        <?= $content ?>

        </div>
    </div>

    <!-- footer -->
    <?php require('footer.php'); ?>

      <!-- modal Area -->              
  <div class="modal fade" id="modal_dialog">
	  <div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h4 id="modal_title" class="modal-title"></h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span></button>
		  </div>
		  <div class="modal-body">			
            <div id="modal_loading">
            <center><h2><i class="fa fa-spinner fa-spin"></i> Loading...</h2></center>
            </div>           
            <div id="dialog_content"></div>
		  </div>
		  <div class="modal-footer">
			<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary float-right">Save changes</button> -->
			<button type="button" class="btn btn-default float-right" data-dismiss="modal">Close</button>
		  </div>
		</div>
		<!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

<?php $this->registerJsFile('@web/js/global.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>


<?php $this->endBody() ?>
</div>

<script type="text/javascript">
<?php if (Yii::$app->session->hasFlash('success')): ?>
    $.toast({
        heading: 'Success !',
        text: '<?= Yii::$app->session->getFlash('success') ?>',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'success',
        hideAfter: 3500,
        stack: 6
    });
<?php endif; ?>

<?php if(Yii::$app->session->hasFlash('error')) : ?>
    $.toast({
        heading: 'Error !',
        text: '<?= Yii::$app->session->getFlash('error') ?>',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'error',
        hideAfter: 3500,
        stack: 6
    });
<?php endif; ?>

</script>



</body>
</html>
<?php $this->endPage();

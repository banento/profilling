const base_url = $("#base_url").val();

/*
$(function () {
    "use strict";

});*/


let table_profiling = null;

function getDataProfiling(){	
	if(table_profiling != null){
	  table_profiling.destroy();
	}
	table_profiling = $('#table_profiling').DataTable( {
	  "processing": true,
	  "serverSide": true,
	  "ajax": {
				"url": base_url + "profiling/list",
				"type": "POST",
				"data" :{
					"_csrf-frontend" : getToken(),
					"_formdata" : getFormData()
				} 
	  },
	  "language"     : {
	      "emptyTable"  : "<span class ='label label-danger'>Data not found!</span>",  
	      "infoEmpty"   : "Data Empty",
	      "processing"  : `<div class="spinner-border" role="status">
			                  <span class="sr-only">Loading...</span>
			                </div>`,
	      "search"      : "_INPUT_"
	  },
	  "columns": [	       
	        { "data": "profiling_id", "width": "5%", "class":"text-center"},
			{ 
				"data": "nik",
				"width": "20%",
				"class":"text-center",
				"render": function (data) {
					return `
							<button alt="1" class="btn-view-profiling btn btn-info waves-effect waves-light btn-xs" title="Click to view ${data}"><i class="fa fa-eye"></i></button>
							<button alt="1" class="btn-edit-profiling btn btn-warning waves-effect waves-light btn-xs" title="Click to edit ${data}"><i class="fa fa-pencil"></i></button>
							<button alt="1" class="btn-delete-profiling  btn btn-danger waves-effect waves-light btn-xs" title="Click to delete ${data}"><i class="fa fa-trash"></i></button>`;
				}
			},
	        { "data": "nik"},
	        { "data": "nama"},
	        // { "data": "profiling_disease"},
	        { 
				"data": "profiling_group",
				"render": function (data){
					console.log(data[0].type);
					var html = "dd";
					if(data){
						// html = "<table border='0'>";
						// for(var i=0; i < data.length; i++ ){
						// 	html += `<tr>
						// 		<td>${data[i].type}</td>
						// 	</tr>`;
						// }
						// html += "</table>"

						html = "<ul>";
						for(var i=0; i < data.length; i++ ){
							html += `<li>${data[i].type_view} : `;
										html += `<ul>`;
										for(var a=0; a < data[i].detail.length; a++ ){
											html += `<li>${data[i].detail[a].name} </li>`;
										}
										html += `</ul>`;
									html += `</li>`;
						}
						html += "</ul>"
					}
					 return html;
				}
			},
	        { "data": "title"},
	        { "data": "organization"},
	        { "data": "department"},
	        { "data": "division"},
	        { "data": "job"},
	        { "data": "job_category"},
	        { "data": "area"},
	        { "data": "directorate"},
	        { "data": "employee_category"},
	        { "data": "email"}	       
	  ],
	  "drawCallback": function( settings, start, end, max, total, pre ) {
            $(".DTFC_LeftBodyLiner").css('overflow','hidden');
	  },
	  "paging":         true,
	  "columnDefs": [
	      {
	      	"targets": [ 0 ],
	        "orderable": false

	      },
	      {
	          "targets": [ 0],
	          "visible": false
	      }
	  ],
	  "scrollY"          : true, 
	  "scrollCollapse"   : true,
	  "scrollX"         : true,
      "bAutoWidth" : true

    });

	// $('input[type="search"]').attr('placeholder','Search here...').addClass('form-control input-sm m-0');
	$(".dataTables_filter").hide();

}

function getToken(){
	var csrfToken = $('meta[name="csrf-token"]').attr("content");
	return csrfToken;
}

function getFormData(){	
	var formData = $('#formFilterProfiling input').serializeArray();
	return formData;
}

function exportData(){

	var url = base_url+ "export/excel";   	
	var csrfToken = $('meta[name="csrf-token"]').attr("content");

	$.ajax({
		url : url,
		type:"post",              
		dataType:"json",
		data : {_formData: $("#formFilterProfiling").serializeArray(), '_csrf-frontend' : csrfToken},
		error: function (request, status, error) {
				toast_notif(error,"warning");                       
			},
		success:function(data){                                  
			if(data && data.status=="ok"){
				var $filename = data.filename;
				var $a = $("<a>");
				$a.attr("href",data.file);
				$("body").append($a);
				$a.attr("download",$filename+".xlsx");
				$a[0].click();
				$a.remove();
			} 		
		}
	}); 
	
}

$(document).ready(function(){

  $("#opt_filter").select2({
	  placeholder : "Choose"
  })
  getDataProfiling();

  $('#btn_search').click(function() {
		// table_profiling.ajax.reload();
		getDataProfiling()
	});
	
	$('#btn_reset').click(function() {
		$(".opt-filter").val("");
		getDataProfiling();
	});
	
	$('#btn_export').click(function() {		
		exportData();
	});

  /* Create */
  $('#btn-create-profiling').on( 'click', function () {
    	window.location.href = base_url + 'profiling/create';
	});


  /* View */
	$('#table_profiling tbody').on( 'click', 'button.btn-view-profiling', function () {
			let profiling_id = table_profiling.row( $(this).parents('tr') ).data().profiling_id;
    	window.location.href = base_url + 'profiling/view/' + profiling_id;
	});


  /* Edit */
	$('#table_profiling tbody').on( 'click', 'button.btn-edit-profiling', function () {
			let profiling_id = table_profiling.row( $(this).parents('tr') ).data().profiling_id;
    	window.location.href = base_url + 'profiling/edit/' + profiling_id;
	});

  /* Delete */
	$('#table_profiling tbody').on( 'click', 'button.btn-delete-profiling', function () {

			let profiling_id = table_profiling.row( $(this).parents('tr') ).data().profiling_id;

      swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this record!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: true 
        }, function(isConfirm){   
            if (isConfirm) {     
                 $.ajax({
						      url       : base_url + 'profiling/delete/',
						      type      : 'DELETE',
						      data        : { profiling_id: profiling_id },
						      dataType : 'json',
						      success : function(result){
						        if(result.status == 'Success'){
		              		table_profiling.ajax.reload(null, false);
						          toast_notif('success', result.message);
						        }
						        else{
						          toast_notif('error', result.message);
						        }
						      },
									error	: function (xhr, status, error) {
					          toast_notif('error', xhr.responseText);
									}
						    });
            }
      });
	});
	/* End delete */

});
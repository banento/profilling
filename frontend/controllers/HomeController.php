<?php

namespace frontend\controllers;

use frontend\models\News;
use Yii;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class HomeController extends Controller
{

    public function beforeAction($action)
    {
        if(parent::beforeAction($action)){
            if (!Yii::$app->session->get('isLoggedin')){

                Yii::$app->session->setFlash('error', "Error, please sign in to start your session.");
                
                $this->redirect(['login/index']);
                return false;
            }
            
            return true;
            
        }
    }

    public $layout = "template";
    public $title = "Profiling - Home";

    public function actionIndex()
    {
        Yii::$app->view->title = $this->title;
         // $reply = ChatReply::findOne(['unique_id_msg' => $unique_id_msg]);
        //  $news = News::findAll() ;
        $query = New Query();
        $data['news']  = $query->from('news')
                ->limit(5)
                ->all();
        
        return $this->render('index', $data);
    } 
    
    public function actionIndex_old()
    {
        Yii::$app->view->title = $this->title;
        return $this->render('index_old');
    }

    public function actionView()
    {        
        Yii::$app->view->title = $this->title;
        $input = Yii::$app->request->get();

        $id = $input['id'];

        $news = News::findOne(['news_id' => $id]);

        $data = array(
                        'news_id'      => $news['news_id'],
                        'news_title'   => $news['news_title'],
                        'news_content' => $news['news_content'],
                        'news_img'     => ($news['news_img']) ? substr($news['news_img'], strpos($news['news_img'], 'uploads')) : ''
                    );

        return $this->render('view', $data);

    }
    
}

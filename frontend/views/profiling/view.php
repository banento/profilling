<?php 
use yii\helpers\Url;
?>
<div class="content-header" style="padding-top:60px !important">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="page-title"><?= $this->title ?></h3>
      <div class="d-inline-block align-items-center">
        <nav>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= Url::base() . "/home/index" ?>"><i class="mdi mdi-home-outline"></i></a></li>
            <li class="breadcrumb-item"><a href="<?= Url::base() . "/profiling" ?>">List Profiling</a></li>
            <li class="breadcrumb-item active" aria-current="page">View</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>  
  
    <!-- Main content -->
    <section class="content" style="padding-top:70px !important;">

    <div class="row">
      <div class="col-lg-4 col-12">  
        <div class="box box-inverse bg-img" style="background-image: url(<?= Url::base()  ?>/images/gallery/full/1.jpg);" data-overlay="2">
             <div class="box-body text-center pb-50">
                <a href="#">
                  <img class="avatar avatar-xxl avatar-bordered" src="<?= Url::base()  ?>/images/avatar/5.jpg" alt="">
                </a>
                <h4 class="mt-2 mb-0"><a class="hover-primary text-white" href="#"><?= $emp["nama"] ?></a></h4>             
              </div>

              <ul class="box-body flexbox flex-justified text-center" data-overlay="4">
                <li>
                  <span class="opacity-60">Title</span><br>
                  <span class="font-size-15"><?= $emp["title"] ?></span>
                </li>
              </ul>
            </div>   
      
		      <div class="box">
            <div class="box-header with-border">
              <h4 class="box-title"><strong>Notes</strong></h4>
            </div>
            <div class="box-body box-profile">            
              <div class="row" style="min-height: 200px;">				       
                <?= $profil["notes"] ?>
              </div>
            </div>       
          </div>
	  </div>

      <div class="col-12 col-lg-8">
          <div class="box">
            <div class="box-header with-border">
              <h4 class="box-title">Profile</h4>
            </div>
            <div class="box-body">           
              <div class="form-group">
                <label>NIK</label>
                <div class="row">
                    <div class="col-12">
                    <div class="input-group">
                      <div class="input-group-addon">
                      <i class="fa fa-id-card"></i>
                      </div>
                      <input type="text" class="form-control" placeholder="Nik" value="<?= $profil["nik"] ?>" readonly>
                    </div>                   
                    </div>
                </div>
              </div>             

             
              <div class="form-group">
                <label>Name</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-user-o"></i>
                  </div>
                  <input type="text" class="form-control" value="<?= $emp["nama"] ?>" readonly>
                </div>              
              </div>
              
              <div class="form-group">
                <label>Email</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-envelope-o"></i>
                  </div>
                  <input type="text" class="form-control" value="<?= $emp["email"] ?>" readonly>
                </div>              
              </div>
            
              <div class="form-group">
                <label>Organization</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-users"></i>
                  </div>
                  <input type="text" class="form-control" value="<?= $emp["organization"] ?>" readonly>
                </div>               
              </div>

              <div class="form-group">
                <label>Area</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-map-o"></i>
                  </div>
                  <input type="text" class="form-control" value="<?= $emp["area"] ?>"  readonly>
                </div>                
              </div>        
              
              <div class="form-group">
                <label>Attachment</label>
                <div class="input-group">                 
                  <?php
                    if(isset($profil['attachment']) && $profil['attachment']){ ?>
                    <a class="btn btn-primary btn-outline" href="<?= Url::base()."/profiling/download?file=".$profil['attachment'] ?>" target="_blank"> <i class="fa fa-fw fa-file"></i> <?= $profil['attachment'] ?></a>
                    <?php  } else {
                      echo "-";
                    }
                    ?>
                </div>                
              </div>         
              
              <div class="form-group">
                <label>Profiling</label>
                <!-- <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-fw fa-medkit"></i>
                  </div>
                  <input type="text" class="form-control" value="<? //= $disease ?>"  readonly>
                </div>                 -->

                <?php
                    $no=1;
                    foreach($list_disease as $label=>$val){ 
                      if($label) { ?>
                       <div class="col-lg-2"><strong><?= ucwords(str_replace("_"," ", $label)) ?></strong></div>
                       <hr>
                       <div class="col-lg-12">
                          <div class="demo-checkbox">

                       <?php foreach($val as $key=>$v){ 
                         if($v['disease_id']!=0){
                          $dis_arr = (isset($dis_xsist) && $dis_xsist) ? $dis_xsist : [];
                          $checked = (in_array($v['disease_id'],$dis_arr))? "checked" : "";
                        ?>
                          <input type="checkbox" id="<?= $label."_".$key ?>" class="filled-in chk-col-light-blue" <?= $checked ?> name="disease[]" disabled value="<?= $v['disease_id'] ?>" />
                          <label for="<?= $label."_".$key ?>"><?= ucwords(str_replace("_"," ", $v['name'])) ?></label>		
                    <?php }
                        } ?>

                          </div>
                       </div>
                   <?php }
                      }
                    ?>

                  <?php
                   $dis_arr = (isset($dis_xsist) && $dis_xsist) ? $dis_xsist : [];
                   $checked = (in_array(0,$dis_arr))? "checked" : "";  
                   $style = ($checked)? "": "display: none;";                 
                  ?>
                    <div class="col-lg-2"></div>
                       <hr>
                       <div class="col-lg-12" style="<?= $style ?>">
                          <div class="demo-checkbox">                      
                            <input type="checkbox" id="other" class="filled-in chk-col-light-blue" <?= $checked ?> name="disease[]" disabled value="0" />
                            <label for="other">Other</label>		
                          </div>
                          <textarea id="other_text" name="other_text" class="form-control col-lg-4" style="<?= $style ?>" rows="2" disabled ><?= (isset($dis_other['other']) && $dis_other['other'])? $dis_other['other']: "" ?></textarea>
                       </div>

              </div>  
            
              <div class="form-group">
                <label></label>               
                  <a href="/profiling/edit/<?= $profil["profiling_id"] ?>" class="btn btn-outline btn-success"><i class="fa fa-pencil"></i> Edit</a>
                  <a href="/profiling" class="btn btn-outline btn-warning"><i class="fa fa-reply"></i> Back</a>                         
              </div>             

            </div>           
          </div>        
      </div>
	  
    </div>

    </section>
    <!-- /.content -->
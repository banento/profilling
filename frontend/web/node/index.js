//load node module
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var fs = require('fs');
var formidable = require('formidable');
var file_type = require('file-type');

//set port for node
app.set('port',(process.env.PORT||3030));

//get url
app.get('/',function(req,res){
	res.send('Hello, Service Node chat is running...');
});

app.all('*',function(req,res,next){
	res.header('Access-Control-Allow-Origin','*');
	res.header('Access-Control-Allow-Headers','X-Requested-With');
	next();
});

app.post('/upload_files',function(req,res){
	var fileu = [];
		form = new formidable.IncomingForm();

		form.multiples = true;
		form.uploadDir = path.join(__dirname,'../tmp_uploads');

		form.on('file',function(name,file){
			if(fileu.length===3){
				fs.unlink(file.path);
				return true;
			}

			var filename = '';
			filename = Date.now() + '-' + file.name;

			var extfile = path.extname(filename);
			var ext = extfile.toLowerCase();

			if(ext!==null&&(ext==='.png'||ext==='.jpg'||ext==='.jpeg'||ext==='.gif'||ext==='.docx'||ext==='.xlsx'||ext==='.pptx'||ext==='.pdf'||ext==='.txt'||ext==='.xls'||ext==='.doc'||ext==='.ppt')){
				fs.rename(file.path,path.join(__dirname,'../uploads/'+filename),function(err,data){
					// console.log(err);
				});

				fileu.push({
					status:true,
					filename:filename,
					type:ext,
					publicPath:'uploads/'+filename
				});
			}
			else{
				fileu.push({
					status:false,
					filename:file.name,
					type:null,
					message:'Invalid file type, file type must be png, jpg, jpeg, gif, docx, xlsx, pptx, pdf, txt'
				});
				fs.unlink(file.path,function(err,data){

				});
			}
		});

		form.on('error',function(err){
			console.log('Error occurred during processing - '+ err);
		});

		form.on('end',function(){
			console.log('All the request fields have been processed');
		});

		form.parse(req,function(err,fields,files){
			res.status(200).json(fileu);
		})
});

io.on('connection',function(socket){
	var room = socket.handshake['query']['r_var'];
	var r_name = socket.handshake['query']['r_name'];

	socket.join(room);

	socket.on('chat message',function(msg){
		io.to(room).emit('chat message',msg);
	});

	socket.on('chat notif',function(chat_notif){
		io.to(room).emit('chat notif',chat_notif);
	});

	socket.on('typing',function(data){
		io.to(room).emit('display',data);
	});
});

//listen app
http.listen(app.get('port'),function(){
	console.log('Listening on * :'+app.get('port'));
});


<?php

namespace frontend\controllers;

use backend\components\RestHelper;
use frontend\models\Employee;
use frontend\models\Profiling;
use frontend\models\ProfilingDisease;
use Yii;
use yii\web\Controller;

// use PhpOffice\PhpSpreadsheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class ExportController extends Controller
{
    protected $title = "Profiling";

    private $success = 'Success';
    private $failed = 'Failed';

    public function beforeAction($action)
    {
        if(parent::beforeAction($action)){
            if (!Yii::$app->session->get('isLoggedin')){

                Yii::$app->session->setFlash('error', "Error, please sign in to start your session.");
                
                $this->redirect(['login/index']);
                return false;
            }
            
            return true;
            
        }
    }

    public function actionIndex()
    {
        Yii::$app->view->title = $this->title;
       return $this->render('index');
    }

    public function actionExcel()
    {
        
        $input = Yii::$app->request->post(); 
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $style_col = [
            'font' => ['bold' => true], // Set font nya jadi bold
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ],
            'borders' => [
                'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
                'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
                'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
                'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
            ]
        ];
        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ],
            'borders' => [
                'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
                'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
                'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
                'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
            ]
        ];

        // $sheet->setCellValue('A1', 'Hello World !');
        $sheet->setCellValue('A1', "DATA PROFILING");
        $sheet->mergeCells('A1:M1'); // Set Merge Cell pada kolom A1 sampai F1
        $sheet->getStyle('A1')->getFont()->setBold(true); // Set bold kolom A1
        $sheet->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $sheet->setCellValue('A3', "NO");
        $sheet->setCellValue('B3', "NIK");
        $sheet->setCellValue('C3', "NAME");
        $sheet->setCellValue('D3', "PROFILING");
        $sheet->setCellValue('E3', "TITLE");
        $sheet->setCellValue('F3', "ORGANIZATION");
        $sheet->setCellValue('G3', "DEPARTMENT");
        $sheet->setCellValue('H3', "DIVISION");
        $sheet->setCellValue('I3', "JOB");
        $sheet->setCellValue('J3', "JOB CATEGORY");
        $sheet->setCellValue('K3', "AREA");
        $sheet->setCellValue('L3', "DIRECTORATE");
        $sheet->setCellValue('M3', "EMAIL");


        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $sheet->getStyle('A3')->applyFromArray($style_col);
        $sheet->getStyle('B3')->applyFromArray($style_col);
        $sheet->getStyle('C3')->applyFromArray($style_col);
        $sheet->getStyle('D3')->applyFromArray($style_col);
        $sheet->getStyle('E3')->applyFromArray($style_col);
        $sheet->getStyle('F3')->applyFromArray($style_col);
        $sheet->getStyle('G3')->applyFromArray($style_col);
        $sheet->getStyle('H3')->applyFromArray($style_col);
        $sheet->getStyle('I3')->applyFromArray($style_col);
        $sheet->getStyle('J3')->applyFromArray($style_col);
        $sheet->getStyle('K3')->applyFromArray($style_col);
        $sheet->getStyle('L3')->applyFromArray($style_col);
        $sheet->getStyle('M3')->applyFromArray($style_col);

        // Set height baris ke 1, 2 dan 3        
        $sheet->getRowDimension('1')->setRowHeight(20);
        $sheet->getRowDimension('2')->setRowHeight(20);
        $sheet->getRowDimension('3')->setRowHeight(20);
        

         
        // $sql = "select * from v_profiling ";         
        // $datas = Yii::$app->db->createCommand($sql)->queryAll();
        // $where = [];
        // if($input["_formData"]){
        //     foreach($input["_formData"] as $key=> $val){
        //         if($val["value"]){
        //             $where[] = [$val["name"]=>$val["value"]];
                    
        //         }
        //     }
        // }
       
        $prof = New Profiling();
        $datas = $prof->getData($input["_formData"]);
      
        $no = 1;
        $row = 4; // Set baris pertama untuk isi tabel adalah baris ke 4      
        foreach($datas as $key=>$data) {
            $sheet->setCellValue('A' . $row, $no);
            // Khusus untuk nik. set type kolom nya jadi STRING
            $sheet->setCellValueExplicit('B' . $row, $data['nik'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('C' . $row, $data['nama']);
            $sheet->setCellValue('D' . $row, $data['profiling_disease']);
            $sheet->setCellValue('E' . $row, $data['title']);
            $sheet->setCellValue('F' . $row, $data['organization']);
            $sheet->setCellValue('G' . $row, $data['department']);
            $sheet->setCellValue('H' . $row, $data['division']);
            $sheet->setCellValue('I' . $row, $data['job']);
            $sheet->setCellValue('J' . $row, $data['job_category']);
            $sheet->setCellValue('K' . $row, $data['area']);
            $sheet->setCellValue('L' . $row, $data['directorate']);
            $sheet->setCellValue('M' . $row, $data['email']);
            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $sheet->getStyle('A' . $row)->applyFromArray($style_row);
            $sheet->getStyle('B' . $row)->applyFromArray($style_row);
            $sheet->getStyle('C' . $row)->applyFromArray($style_row);
            $sheet->getStyle('D' . $row)->applyFromArray($style_row);
            $sheet->getStyle('E' . $row)->applyFromArray($style_row);
            $sheet->getStyle('F' . $row)->applyFromArray($style_row);
            $sheet->getStyle('G' . $row)->applyFromArray($style_row);
            $sheet->getStyle('H' . $row)->applyFromArray($style_row);
            $sheet->getStyle('I' . $row)->applyFromArray($style_row);
            $sheet->getStyle('J' . $row)->applyFromArray($style_row);
            $sheet->getStyle('K' . $row)->applyFromArray($style_row);
            $sheet->getStyle('L' . $row)->applyFromArray($style_row);
            $sheet->getStyle('M' . $row)->applyFromArray($style_row);
            // $sheet->getStyle('A' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom No
            // $sheet->getStyle('B' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT); // Set text left untuk kolom NIS
            $sheet->getRowDimension($row)->setRowHeight(20); // Set height tiap row
            $no++;
            $row++;
        }
        
         // Set width kolom
        
        $sheet->getColumnDimension('A')->setWidth(5); // Set width kolom A
        $sheet->getColumnDimension('B')->setWidth(15); // Set width kolom B
        $sheet->getColumnDimension('C')->setWidth(25); // Set width kolom C
        $sheet->getColumnDimension('D')->setWidth(20); // Set width kolom D
        $sheet->getColumnDimension('E')->setWidth(15); // Set width kolom E
        $sheet->getColumnDimension('F')->setWidth(30); // Set width kolom F
        
        // Set orientasi kertas jadi LANDSCAPE
        $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        // Set judul file excel nya
        $sheet->setTitle("Data Profiling");
        // Proses file excel
        $filename = "Data Profiling";
        $writer = new Xlsx($spreadsheet);
        // $writer->save('hello world.xlsx');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
        header('Cache-Control: max-age=0');

        ob_start();
        $writer->save('php://output');
        // die(); // klau g pake ajax pke ini
        $xlsData = ob_get_contents();
        ob_end_clean();
        $result = ['status' => "ok", "filename"=> $filename,'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)];      
        // die(json_encode($result));
        RestHelper::send_response(200, $result ); 
    
    }
    
    public function actionExcel1()
    {
        
        $input = Yii::$app->request->post(); 
        /*
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');

        $writer = new Xlsx($spreadsheet);
        // $writer->save('hello world.xlsx');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="download.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
        die();
*/

        /*
        // print_r($input['_formData']);
        // die();
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
         
        //Menggunakan Model
         
        $database = Profiling::find()
        ->select('nik,notes')
        ->all();
       
        //JIka menggunakan DAO , gunakan QueryAll()
         
        
         
        // $sql = "select kode_jafung,jenis_jafung from ref_jafung"
         
        // $database = Yii::$app->db->createCommand($sql)->queryAll();
         
        
         
        $database = \yii\helpers\ArrayHelper::toArray($database);
        $worksheet->fromArray($database, null, 'A4');
         
        $writer = new Xlsx($spreadsheet);
         
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="download.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
        die();
        */

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = [
            'font' => ['bold' => true], // Set font nya jadi bold
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ],
            'borders' => [
                'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
                'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
                'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
                'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
            ]
        ];
        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ],
            'borders' => [
                'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
                'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
                'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
                'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
            ]
        ];
        $sheet->setCellValue('A1', "DATA PROFILING"); // Set kolom A1 dengan tulisan "DATA SISWA"
        $sheet->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai F1
        $sheet->getStyle('A1')->getFont()->setBold(true); // Set bold kolom A1
        $sheet->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        // Buat header tabel nya pada baris ke 3
       /*
        $sheet->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
        $sheet->setCellValue('B3', "NIK"); // Set kolom B3 dengan tulisan "NIS"
        $sheet->setCellValue('C3', "NAME"); // Set kolom C3 dengan tulisan "NAMA"
        $sheet->setCellValue('D3', "PROFILING"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
        $sheet->setCellValue('E3', "TITLE"); // Set kolom E3 dengan tulisan "TELEPON"
        $sheet->setCellValue('F3', "ORGANIZATION"); // Set kolom F3 dengan tulisan "ALAMAT"
        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $sheet->getStyle('A3')->applyFromArray($style_col);
        $sheet->getStyle('B3')->applyFromArray($style_col);
        $sheet->getStyle('C3')->applyFromArray($style_col);
        $sheet->getStyle('D3')->applyFromArray($style_col);
        $sheet->getStyle('E3')->applyFromArray($style_col);
        $sheet->getStyle('F3')->applyFromArray($style_col);
        */

        // Set height baris ke 1, 2 dan 3
        /*
        $sheet->getRowDimension('1')->setRowHeight(20);
        $sheet->getRowDimension('2')->setRowHeight(20);
        $sheet->getRowDimension('3')->setRowHeight(20);
        */
        // Buat query untuk menampilkan semua data siswa
        // $sql = mysqli_query($connect, "SELECT * FROM siswa");
        /*
        $sql = "select * from v_profiling";         
        $datas = Yii::$app->db->createCommand($sql)->queryAll();

        $no = 1; // Untuk penomoran tabel, di awal set dengan 1
        $row = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
        // while ($data = mysqli_fetch_array($sql)) { // Ambil semua data dari hasil eksekusi $sql
        foreach($datas as $key=>$data) {
            $sheet->setCellValue('A' . $row, $no);
            // Khusus untuk nik. set type kolom nya jadi STRING
            $sheet->setCellValueExplicit('B' . $row, $data['nik'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('C' . $row, $data['nama']);
            $sheet->setCellValue('D' . $row, $data['profiling_disease']);
            $sheet->setCellValue('E' . $row, $data['title']);
            $sheet->setCellValue('F' . $row, $data['organization']);
            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $sheet->getStyle('A' . $row)->applyFromArray($style_row);
            $sheet->getStyle('B' . $row)->applyFromArray($style_row);
            $sheet->getStyle('C' . $row)->applyFromArray($style_row);
            $sheet->getStyle('D' . $row)->applyFromArray($style_row);
            $sheet->getStyle('E' . $row)->applyFromArray($style_row);
            $sheet->getStyle('F' . $row)->applyFromArray($style_row);
            $sheet->getStyle('A' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom No
            $sheet->getStyle('B' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT); // Set text left untuk kolom NIS
            $sheet->getRowDimension($row)->setRowHeight(20); // Set height tiap row
            $no++; // Tambah 1 setiap kali looping
            $row++; // Tambah 1 setiap kali looping
        }
        */
        // Set width kolom
        /*
        $sheet->getColumnDimension('A')->setWidth(5); // Set width kolom A
        $sheet->getColumnDimension('B')->setWidth(15); // Set width kolom B
        $sheet->getColumnDimension('C')->setWidth(25); // Set width kolom C
        $sheet->getColumnDimension('D')->setWidth(20); // Set width kolom D
        $sheet->getColumnDimension('E')->setWidth(15); // Set width kolom E
        $sheet->getColumnDimension('F')->setWidth(30); // Set width kolom F
        
        // Set orientasi kertas jadi LANDSCAPE
        $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        // Set judul file excel nya
        $sheet->setTitle("Data Profiling");
        // Proses file excel
        */

        /*
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Data Profiling.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        */

        $writer = new Xlsx($spreadsheet);
        // $writer->save('hello world.xlsx');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="download.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    } 
    
}

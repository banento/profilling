<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "employee".
 *
 * @property string $person_id
 * @property string $nik
 * @property string $nama
 * @property string $title
 * @property string $employee_category
 * @property string $organization
 * @property string $job
 * @property string $band
 * @property string $email
 * @property string $nik_atasan
 * @property string $nama_atasan
 * @property string $section
 * @property string $department
 * @property string $division
 * @property string $bgroup
 * @property string $egroup
 * @property string $directorate
 * @property string $area
 * @property string $status
 * @property string $status_employee
 * @property string $start_date_status
 * @property string $bp
 * @property string $bi
 * @property string $edu_lvl
 * @property string $structural
 * @property string $functional
 * @property int $position_id
 * @property string $job_category
 */
class Employee extends \yii\db\ActiveRecord
{   
    public $total_emp;
    public $name_job;
    public $level_job;
		
    /**
     * @inheritdoc$primaryKey
     */
    public static function primaryKey()
    {
        return ["person_id"];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['person_id'], 'required'],
            [['position_id'], 'integer'],
            [['person_id', 'nik', 'nik_atasan', 'status_employee'], 'string', 'max' => 25],
            [['nama', 'job', 'email', 'nama_atasan'], 'string', 'max' => 128],
            [['username'], 'string', 'max' => 125],
            [['title', 'organization', 'section', 'department', 'division', 'bgroup', 'egroup', 'directorate'], 'string', 'max' => 255],
            [['employee_category'], 'string', 'max' => 15],
            [['band'], 'string', 'max' => 1],
            [['area', 'status'], 'string', 'max' => 45],
            [['bp', 'bi', 'structural', 'functional'], 'string', 'max' => 5],
            [['job_category'], 'string', 'max' => 200],
            [['person_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'nik' => 'Nik',
            'nama' => 'Nama',
            'title' => 'Title',
            'employee_category' => 'Employee Category',
            'organization' => 'Organization',
            'job' => 'Job',
            'band' => 'Band',
            'email' => 'Email',
            'nik_atasan' => 'Nik Atasan',
            'nama_atasan' => 'Nama Atasan',
            'section' => 'Section',
            'department' => 'Department',
            'division' => 'Division',
            'bgroup' => 'Bgroup',
            'egroup' => 'Egroup',
            'directorate' => 'Directorate',
            'area' => 'Area',
            'status' => 'Status',
            'status_employee' => 'Status Employee',
            'bp' => 'Bp',
            'bi' => 'Bi',
            'structural' => 'Structural',
            'functional' => 'Functional',
            'position_id' => 'Position ID',
            'job_category' => 'Job Category'
        ];
    }

    public function getSupervisor()
    {
        return $this->hasOne(Employee::className(), ['nik' => 'nik_atasan']);
    }

    public function getUserEmployee(){
        return $this->hasOne(User::className(),['nik'=>'nik']);
    }
}

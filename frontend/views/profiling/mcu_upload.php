<?php 

use abei2017\emoji\Emoji;
use yii\helpers\Url;
use yii\web\View;
use common\components\Helper;

?>

  <!-- Content Wrapper. Contains page content -->
  <!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->
   <div class="content-header" style="padding-top:60px !important">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="page-title"><?= $this->title ?></h3>
        <div class="d-inline-block align-items-center">
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?= Url::base() . "/home/index" ?>"><i class="mdi mdi-home-outline"></i></a></li>
              <li class="breadcrumb-item"><a href="<?= Url::base()."/profiling/mcu" ?>">List MCU</a></li>
              <li class="breadcrumb-item active" aria-current="page">Upload MCU</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>  

    <!-- Main content -->
    <section class="content" style="padding-top:20px !important">
      <div class="row">
        <div class="col-12">     
          <div class="box">
            <div class="box-header">
              <h4 class="box-title">Upload Medical Check Up (MCU)<br>
                <!-- <small>Bootstrap html5 editor</small> -->
              </h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form id="mcu_form" role="form">    
              <div class="form-group"> 
                <a class="btn btn-primary btn-sm btn-outline" href="<?= Url::base()."/profiling/templatedownload" ?>" target="_blank"> <i class="fa fa-file-excel-o"></i>  Template MCU</a>
              </div>

                <div class="form-group">
                  <label>File</label>
                  <div class="controls">
                    <input type="file" name="file_mcu" id="file_mcu" class="form-control" accept=".xls,.xlsx" aria-invalid="false" required> 
                    <div class="help-block"></div>                 
                  <small>Size Max : 5mb, Type : xls, xlsx</small>
                  <input type="hidden" id="act" name="act" value="<?= $act ?>">
                  <input type="hidden" id="id" name="id" value="<?= isset($id) ? $id : "" ?>">      
                  </div>
                </div>    
                
                <div class="form-group">
                  <label>Description</label>
                  <div class="input-group">
                    <textarea class="form-control" id="description" name="description" value="" placeholder="Description" required ></textarea>
                  </div>
                </div>
              
                
              <div class="pt-20">
                <button type="submit" class="btn btn-success btn-outline">
                  <i class="ti-save-alt"></i> Submit
                </button>
                <button type="reset" class="btn btn-warning btn-outline mr-1">
                  <i class="ti-trash"></i> Reset
                </button>
              </div>
              </form>
            </div>
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->



<input type="hidden" name="base_url" id="base_url" value="<?= Url::base(true) ?>/">


	<!-- popper -->
	<?php $this->registerJsFile('@web/assets/vendor_components/popper/dist/popper.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Bootstrap 4.0-->
	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Superieur Admin for demo purposes -->
	<?php $this->registerJsFile('@web/js/demo.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

  <?php $this->registerJsFile('@web/assets/vendor_components/bootstrap-select/dist/js/bootstrap-select.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  <?php $this->registerJsFile('@web/assets/vendor_components/select2/dist/js/select2.full.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  <?php $this->registerJsFile('@web/assets/vendor_components/sweetalert/sweetalert.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  <?php $this->registerJsFile('@web/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  
	<?php $this->registerJsFile('@web/js/mcu_upload.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mcu_header".
 *
 * @property int $mh_id
 * @property string|null $filename_ori
 * @property string|null $filname_random
 * @property string|null $description
 * @property string|null $created_by
 * @property string|null $created_date
 * @property string|null $created_ip
 *
 * @property McuDetail[] $mcuDetails
 */
class McuHeader extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mcu_header';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['created_date'], 'safe'],
            [['filename_ori', 'filname_random'], 'string', 'max' => 255],
            [['created_by'], 'string', 'max' => 20],
            [['created_ip'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'mh_id' => 'Mh ID',
            'filename_ori' => 'Filename Ori',
            'filname_random' => 'Filname Random',
            'description' => 'Description',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'created_ip' => 'Created Ip',
        ];
    }

    /**
     * Gets query for [[McuDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMcuDetails()
    {
        return $this->hasMany(McuDetail::className(), ['mh_id' => 'mh_id']);
    }
}

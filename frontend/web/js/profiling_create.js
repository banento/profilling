const base_url = $("#base_url").val();

$(document).ready(function(){

	$("#medical_diagnosis").select2();

	get_diagnosa();

	$("#other").on("change", ()=>{		
		if($("#other").is(":checked")){
			$("#other_text").show(200)
		} else {
			$("#other_text").hide(200)
			$("#other_text").val("")
		}
	})

		/* Ajax Save */
	$('#profiling_form').on('submit', function(e) {
	  if (!e.isDefaultPrevented()) {
	  	let form_data = new FormData($('#profiling_form')[0]);
	    $.ajax({
	      url       : base_url+ "profiling/save",
	      type      : 'POST',
	      enctype   : 'multipart/form-data',
    		data: form_data,
		   /* beforeSend  : function(){
		    								showpreloader();
		                  },*/
	      	dataType 		: 'json',
		    processData: false,
		    contentType: false,
			success	: function(result){
				if(result.status == 'Success'){
				toast_notif('success', result.message);
				setTimeout(function() {
					window.location.href = base_url + 'profiling';
					}, 1500);
				}
				else{
				toast_notif('error', result.message);
				}
			},
				error	: function (xhr, status, error) {
          			toast_notif('error', xhr.responseText);
				}
	    });
	  }
	  e.preventDefault();
	});


	
	$("#file_profiling").on("change", ()=>{	

		var filenya = $("#file_profiling")[0].files[0];
		var fname = '', fsize = 0, extnya = '';
		var arrExt 	= [".doc", ".docx", ".jpeg",".jpg",".png", ".pdf" ];
	
		if(typeof(filenya) == 'undefined'){
			console.log('undefined')
		}else{
			fname 	= filenya.name;
			fsize	= (filenya.size / 1024 / 1024).toFixed(2);
			extnya 	= fname.substr(fname.lastIndexOf(".")).toLowerCase();
		
			if(fname && $.inArray(extnya, arrExt) == -1){
				$("#file_profiling").val("");
				toast_notif('warning', "File type not allowed");			
			} 
			
			// if(fsize > 5120){ // 5mb		
			if(fsize > 5){ // 5mb
				$("#file_profiling").val("");
				toast_notif('warning', "Maximal size 5mb");			
			}
		}

	})



	function get_diagnosa()
	{
		var url = base_url+ "profiling/getdiagnosa";   
		var id = $("#id").val(); 
		var csrfToken = $('meta[name="csrf-token"]').attr("content");
	
		$.ajax({
			url : url,
			type:"post",              
			dataType:"json",
			data : {id: id, '_csrf-frontend' : csrfToken},
			error: function (request, status, error) {
					toast_notif(error,"warning");                       
				},
			success:function(data){                                  
				if(data && data.data){
					$("#medical_diagnosis").val(data.data).change();
				} 
			
			}
		}); 
	}

});
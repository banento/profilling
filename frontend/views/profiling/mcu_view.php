<?php 

use yii\helpers\Url;
use yii\web\View;
use common\components\Helper;

?>

	<?php $this->registerCssFile('@web/css/custom.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/sweetalert/sweetalert.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/datatable/datatables.min.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/select2/dist/css/select2.min.css'); ?>


  <div class="content-header" style="padding-top:60px !important">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title"><?= $this->title ?></h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?= Url::base() . "/home/index" ?>"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item"><a href="<?= Url::base() . "/profiling/mcu" ?> ">List MCU</a></li>
            				<li class="breadcrumb-item active" aria-current="page">View</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>

    <!-- Main content -->
    <section class="content p-20">
      <div class="container">
      <div class="row">
        <div class="col-12">

         <div class="box">
            <div class="box-header with-border">
              <h6 class="box-title">List <?= $mcu_h["filename_ori"] . " - ". date("d-m-Y H:i:s", strtotime($mcu_h["created_date"])) ?></h6>			  
              <button id="btn-back-mcu" class="btn btn-warning waves-effect waves-light pull-right w-150" title="Click to back"><i class="fa fa-arrow-left"></i> Back</button>
			  <input type="hidden" id="id" name="id" value="<?= $mcu_h["mh_id"] ?>">
			  <input type="hidden" id="mcol" name="mcol" value="<?= json_encode($mcol) ?>">
			</div>
            <!-- /.box-header -->
            <div class="box-body">			

							<div class="table-responsive">
							  <table id="table_mcu_view" class="datatables table-bordered table-striped table-hover display compact" style="width: 100%;">
								<thead>
								
									<?php 
										$head = "<tr>";
										// $head .= "<th>ID</th>";
										foreach($mcol as $key=>$val){
											$val["data"] = (strlen($val["data"])<=3) ? strtoupper($val["data"]) : ucwords(str_replace("_", " ", $val["data"])); 
											$head .= "<th>".$val["data"]."</th>";
										}
										$head .= "</tr>";
										echo $head;
									?>
								</thead>
							  </table>
							</div>
            </div>
        
          </div>

          
        </div>
    
      </div>
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->



<input type="hidden" name="base_url" id="base_url" value="<?= Url::base(true) ?>/">


  <!-- Datatables -->
	<?php $this->registerJsFile('@web/assets/vendor_components/datatable/datatables.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
	<!-- popper -->
	<?php $this->registerJsFile('@web/assets/vendor_components/popper/dist/popper.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Bootstrap 4.0-->
	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Sweet-Alert  -->
	<?php $this->registerJsFile('@web/assets/vendor_components/sweetalert/sweetalert.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
	<?php $this->registerJsFile('@web/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap-select/dist/js/bootstrap-select.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
    <?php $this->registerJsFile('@web/assets/vendor_components/select2/dist/js/select2.full.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
	<script>
		let column =<?= json_encode($mcol) ?>;	
		
	</script>							
	<?php $this->registerJsFile('@web/js/mcu_view_index.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<script>

/*
const base_url = $("#base_url").val();




let table_mcu_view = null;

// let column = [	       
// 	{ "data": "md_id", "width": "5%", "class":"text-center"},			
// 	{ "data": "no_lab"},
// 	{ "data": "nama"},	       
// 	{ "data": "nik"},    			
// 	{ 
// 		"data": "md_id",
// 		"width": "10%",
// 		"class":"text-center",
// 		"render": function (data) {
// 			return `<button alt="1" class="table_mcu_view_detail btn btn-info waves-effect waves-light btn-xs" title="Click to view ${data}"><i class="fa fa-eye"></i></button>`;
// 		}
// 	},
// ];
let column =<?= json_encode($mcol) ?>;
console.log(column);
function getDataMcu(){	
	if(table_mcu_view != null){
	  table_mcu_view.destroy();
	}
	table_mcu_view = $('#table_mcu_view').DataTable( {
	  "processing": true,
	  "serverSide": true,
	  "ajax": {
				"url": base_url + "profiling/mcu_list_detail",
				"type": "POST",
				"data" :{
					"_csrf-frontend" : getToken(),
					"_id" : getId(),
					// "_formdata" : getFormData()
				} 
	  },
	  "language"     : {
	      "emptyTable"  : "<span class ='label label-danger'>Data not found!</span>",  
	      "infoEmpty"   : "Data Empty",
	      "processing"  : '<div class="loader vertical-align-middle loader-circle"></div>',
	      "search"      : "_INPUT_"
	  },
	  "columns": column,
	  "drawCallback": function( settings, start, end, max, total, pre ) {
            $(".DTFC_LeftBodyLiner").css('overflow','hidden');
	  },
	  "paging":         true,
	  "columnDefs": [
	      {
	      	"targets": [ 0 ],
	        "orderable": false

	      },
	      {
	          "targets": [ 0],
	          "visible": false
	      }
	  ],
	  "scrollY"          : true, 
	  "scrollCollapse"   : true,
	  "scrollX"         : true,
      "bAutoWidth" : true

    });

	$('input[type="search"]').attr('placeholder','Search here...').addClass('form-control input-sm m-0');
	// $(".dataTables_filter").hide();

}

function getToken(){
	var csrfToken = $('meta[name="csrf-token"]').attr("content");
	return csrfToken;
}

function getId(){
	var id = $("#id").val();
	return id;
}

function getFormData(){	
	var formData = $('#formFilterProfiling input').serializeArray();
	return formData;
}

function exportData(){

	var url = base_url+ "export/excel";   	
	var csrfToken = $('meta[name="csrf-token"]').attr("content");

	$.ajax({
		url : url,
		type:"post",              
		dataType:"json",
		data : {_formData: $("#formFilterProfiling").serializeArray(), '_csrf-frontend' : csrfToken},
		error: function (request, status, error) {
				toast_notif(error,"warning");                       
			},
		success:function(data){                                  
			if(data && data.status=="ok"){
				var $filename = data.filename;
				var $a = $("<a>");
				$a.attr("href",data.file);
				$("body").append($a);
				$a.attr("download",$filename+".xlsx");
				$a[0].click();
				$a.remove();
			} 		
		}
	}); 
	
}

$(document).ready(function(){

  
  getDataMcu();

  $('#btn_search').click(function() {
		// table_mcu_view.ajax.reload();
		getDataMcu()
	});
	
	$('#btn_reset').click(function() {
		$(".opt-filter").val("");
		getDataMcu();
	});
	
	$('#btn_export').click(function() {		
		exportData();
	});

  
  $('#btn-upload-mcu').on( 'click', function () {
    	window.location.href = base_url + 'profiling/mcu_upload';
	});


  
	$('#table_mcu_view tbody').on( 'click', 'button.table_mcu_view_detail', function () {
			let mh_id = table_mcu_view.row( $(this).parents('tr') ).data().mh_id;
    	window.location.href = base_url + 'profiling/mcu_view/' + mh_id;
	});


  

});
*/
	</script>


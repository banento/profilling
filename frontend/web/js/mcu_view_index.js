const base_url = $("#base_url").val();

/*
$(function () {
    "use strict";

});*/


let table_mcu_view = null;

function getDataMcu(){	
	if(table_mcu_view != null){
	  table_mcu_view.destroy();
	}
	table_mcu_view = $('#table_mcu_view').DataTable( {
	  "processing": true,
	  "serverSide": true,
	  "ajax": {
				"url": base_url + "profiling/mcu_list_detail",
				"type": "POST",
				"data" :{
					"_csrf-frontend" : getToken(),
					"_id" : getId(),
					// "_formdata" : getFormData()
				} 
	  },
	  "language"     : {
	      "emptyTable"  : "<span class ='label label-danger'>Data not found!</span>",  
	      "infoEmpty"   : "Data Empty",
	      "processing"  : '<div class="loader vertical-align-middle loader-circle"></div>',
	      "search"      : "_INPUT_"
	  },
	  "columns": column,
	  "drawCallback": function( settings, start, end, max, total, pre ) {
            $(".DTFC_LeftBodyLiner").css('overflow','hidden');
	  },
	  "paging":         true,
	  "columnDefs": [
	      {
	      	"targets": [ 0 ],
	        "orderable": false

	      },
	      {
	          "targets": [ 0],
	          "visible": false
	      }
	  ],
	  "scrollY"          : true, 
	  "scrollCollapse"   : true,
	  "scrollX"         : true,
      "bAutoWidth" : true

    });

	$('input[type="search"]').attr('placeholder','Search here...').addClass('form-control input-sm m-0');
	// $(".dataTables_filter").hide();

}

function getToken(){
	var csrfToken = $('meta[name="csrf-token"]').attr("content");
	return csrfToken;
}

function getId(){
	var id = $("#id").val();
	return id;
}

function getFormData(){	
	var formData = $('#formFilterProfiling input').serializeArray();
	return formData;
}

function exportData(){

	var url = base_url+ "export/excel";   	
	var csrfToken = $('meta[name="csrf-token"]').attr("content");

	$.ajax({
		url : url,
		type:"post",              
		dataType:"json",
		data : {_formData: $("#formFilterProfiling").serializeArray(), '_csrf-frontend' : csrfToken},
		error: function (request, status, error) {
				toast_notif(error,"warning");                       
			},
		success:function(data){                                  
			if(data && data.status=="ok"){
				var $filename = data.filename;
				var $a = $("<a>");
				$a.attr("href",data.file);
				$("body").append($a);
				$a.attr("download",$filename+".xlsx");
				$a[0].click();
				$a.remove();
			} 		
		}
	}); 
	
}

$(document).ready(function(){

//   getColumnSet()
  getDataMcu();

  $('#btn_search').click(function() {
		// table_mcu_view.ajax.reload();
		getDataMcu()
	});
	
	$('#btn_reset').click(function() {
		$(".opt-filter").val("");
		getDataMcu();
	});
	
	$('#btn_export').click(function() {		
		exportData();
	});


  $('#btn-back-mcu').on( 'click', function () {
    	window.location.href = base_url + 'profiling/mcu';
	});


  /* View */
	$('#table_mcu_view tbody').on( 'click', 'button.table_mcu_view_detail', function () {
			let mh_id = table_mcu_view.row( $(this).parents('tr') ).data().mh_id;
    	window.location.href = base_url + 'profiling/mcu_view/' + mh_id;
	});


  /* Edit */
	// $('#table_mcu_view tbody').on( 'click', 'button.btn-edit-profiling', function () {
	// 		let mh_id = table_mcu_view.row( $(this).parents('tr') ).data().mh_id;
    // 	window.location.href = base_url + 'profiling/edit/' + mh_id;
	// });

  /* Delete */
	// $('#table_mcu_view tbody').on( 'click', 'button.btn-delete-profiling', function () {

	// 		let mh_id = table_mcu_view.row( $(this).parents('tr') ).data().mh_id;

    //   swal({   
    //         title: "Are you sure?",   
    //         text: "You will not be able to recover this record!",   
    //         type: "warning",   
    //         showCancelButton: true,   
    //         confirmButtonColor: "#DD6B55",   
    //         confirmButtonText: "Yes, delete it!",   
    //         closeOnConfirm: true 
    //     }, function(isConfirm){   
    //         if (isConfirm) {     
    //              $.ajax({
	// 					      url       : base_url + 'profiling/delete/',
	// 					      type      : 'DELETE',
	// 					      data        : { mh_id: mh_id },
	// 					      dataType : 'json',
	// 					      success : function(result){
	// 					        if(result.status == 'Success'){
	// 	              		table_mcu_view.ajax.reload(null, false);
	// 					          toast_notif('success', result.message);
	// 					        }
	// 					        else{
	// 					          toast_notif('error', result.message);
	// 					        }
	// 					      },
	// 								error	: function (xhr, status, error) {
	// 				          toast_notif('error', xhr.responseText);
	// 								}
	// 					    });
    //         }
    //   });
	// });
	/* End delete */

});
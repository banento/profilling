<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mcu_detail".
 *
 * @property int $md_id
 * @property int|null $mh_id
 * @property string|null $no_lab
 * @property string|null $nama
 * @property string|null $nik
 * @property string|null $tanggal_lahir
 * @property string|null $jenis_kelamin
 * @property string|null $tanggal_mcu
 * @property float|null $hemoglobin
 * @property float|null $eritrosit
 * @property float|null $hematokrit
 *
 * @property McuHeader $mh
 */
class McuDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mcu_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mh_id'], 'integer'],
            [['tanggal_lahir'], 'safe'],
            [['hemoglobin', 'eritrosit', 'hematokrit'], 'number'],
            [['no_lab'], 'string', 'max' => 100],
            [['nama'], 'string', 'max' => 250],
            [['nik'], 'string', 'max' => 20],
            [['jenis_kelamin'], 'string', 'max' => 50],
            [['tanggal_mcu'], 'string', 'max' => 255],
            [['mh_id'], 'exist', 'skipOnError' => true, 'targetClass' => McuHeader::className(), 'targetAttribute' => ['mh_id' => 'mh_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'md_id' => 'Md ID',
            'mh_id' => 'Mh ID',
            'no_lab' => 'No Lab',
            'nama' => 'Nama',
            'nik' => 'Nik',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'tanggal_mcu' => 'Tanggal Mcu',
            'hemoglobin' => 'Hemoglobin',
            'eritrosit' => 'Eritrosit',
            'hematokrit' => 'Hematokrit',
        ];
    }

    /**
     * Gets query for [[Mh]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMh()
    {
        return $this->hasOne(McuHeader::className(), ['mh_id' => 'mh_id']);
    }
}

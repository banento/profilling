<?php

namespace frontend\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "employee".
 *
 * @property string $person_id
 * @property string|null $nik
 * @property string|null $nama
 * @property string|null $title
 * @property string|null $employee_category
 * @property string|null $organization
 * @property string|null $job
 * @property string|null $band
 * @property string|null $email
 * @property string|null $nik_atasan
 * @property string|null $nama_atasan
 * @property string|null $section
 * @property string|null $department
 * @property string|null $division
 * @property string|null $bgroup
 * @property string|null $egroup
 * @property string|null $directorate
 * @property string|null $area
 * @property string|null $status
 * @property string|null $status_employee
 * @property string|null $bp
 * @property string|null $bi
 * @property string|null $structural
 * @property string|null $functional
 * @property int|null $position_id
 * @property string|null $job_category
 * @property string|null $username
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['person_id'], 'required'],
            [['position_id'], 'integer'],
            [['person_id', 'nik', 'nik_atasan', 'status_employee'], 'string', 'max' => 25],
            [['nama', 'job', 'email', 'nama_atasan'], 'string', 'max' => 128],
            [['title', 'organization', 'section', 'department', 'division', 'bgroup', 'egroup', 'directorate'], 'string', 'max' => 255],
            [['employee_category'], 'string', 'max' => 15],
            [['band'], 'string', 'max' => 1],
            [['area', 'status'], 'string', 'max' => 45],
            [['bp', 'bi', 'structural', 'functional'], 'string', 'max' => 5],
            [['job_category'], 'string', 'max' => 200],
            [['username'], 'string', 'max' => 125],
            [['person_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'nik' => 'Nik',
            'nama' => 'Nama',
            'title' => 'Title',
            'employee_category' => 'Employee Category',
            'organization' => 'Organization',
            'job' => 'Job',
            'band' => 'Band',
            'email' => 'Email',
            'nik_atasan' => 'Nik Atasan',
            'nama_atasan' => 'Nama Atasan',
            'section' => 'Section',
            'department' => 'Department',
            'division' => 'Division',
            'bgroup' => 'Bgroup',
            'egroup' => 'Egroup',
            'directorate' => 'Directorate',
            'area' => 'Area',
            'status' => 'Status',
            'status_employee' => 'Status Employee',
            'bp' => 'Bp',
            'bi' => 'Bi',
            'structural' => 'Structural',
            'functional' => 'Functional',
            'position_id' => 'Position ID',
            'job_category' => 'Job Category',
            'username' => 'Username',
        ];
    }

    public function UserEmployee($nik)
    {
        $query = new Query();

        $result = $query->select(['employee.nama','employee.nik','user.about','user.profile_picture'])
            ->from('employee')
            ->join('INNER JOIN','user','employee.nik = user.nik')
            ->where(['employee.nik'=>$nik])
            ->one();
        return $result;
    }
}

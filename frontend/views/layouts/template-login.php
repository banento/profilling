<?php
use yii\bootstrap4\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<head>
	<base href="<?= Url::base(true) ?>">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/x-icon" href="<?= Url::base(); ?>/images/logo/favicon.ico">
    <?php $this->head() ?>
    <style type="text/css">
        html, body {
          font-family: 'Source Sans Pro', sans-serif;
        }
    </style>
</head>
<body class="hold-transition bg-img" style="background-image: url(<?= Url::base(); ?>/images/pexels-doctor.jpg)" data-overlay="4">
<?php $this->beginBody() ?>
<!-- <div class="container"> -->
<?= $content ?>
<!-- </div> -->
<?php $this->endBody() ?>

<script type="text/javascript">
<?php if (Yii::$app->session->hasFlash('success')): ?>
    $.toast({
        heading: 'Success !',
        text: '<?= Yii::$app->session->getFlash('success') ?>',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'success',
        hideAfter: 3500,
        stack: 6
    });
<?php endif; ?>

<?php if(Yii::$app->session->hasFlash('error')) : ?>
    $.toast({
        heading: 'Error !',
        text: '<?= Yii::$app->session->getFlash('error') ?>',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'error',
        hideAfter: 3500,
        stack: 6
    });
<?php endif; ?>
</script>
</body>
</html>
<?php $this->endPage();

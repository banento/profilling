<?php

namespace common\components;

class Helper{
	const SEND_MAIL_ACTIVE = false;
	const PICTURE_URL = 'https://hcm.telkomsel.co.id/index.php?r=site%2Fpictures&pid=';
	const PICTURE_URL_NIK = 'https://hcm.telkomsel.co.id/index.php?r=site%2Ffoto&nik=';
	const OKR_URL = 'http://10.250.200.119/okr/frontend/web/index.php?r=site%2Flogin';
	
	const VALIDATE_TYPE_UPLOAD = ["ppt","pptx","pdf","xls","xlsx"];
	const VALIDATE_SIZE_UPLOAD = 2000000;
	const SECURITY_KEY = "arif.rahman12";

	const MENU_LIST = [
		'dashboard' => 'Dashboard',
		'my_okr' => 'My OKR',
		'history' => 'History',
		'create_okr' => 'Create OKR & Dependency',
		'dependency_other' => 'Dependency from Other',
		'monitoring' => 'Monitoring',
		'team_okr' => 'Team OKR',
		'okr_monitoring' => 'OKR Monitoring',
		'okr_lists' => 'Lists of OKR',
		'okr_dependency' => 'Lists of Dependency',
		'settings' => 'Settings',
		'role_menu' => 'Role Menu Management',
		'user_menu' => 'User Management',
		'unit_code_menu' => 'Units Code Management',
		'emp_menu' => 'Employee Management',
		'position_menu' => 'Position Management',
		'period_menu' => 'Period Management',
		'so_si_menu' => 'SO & SI Management',
		'represented_menu' => 'Represented OKR Management',
		'check_okr' => 'Check Create OKR',

		'okr_tree_map' => 'OKR Tree Map',
		'chatbox' => 'Chatbox',
	];

	const QUARTER_LIST = [
		'Q1' => 'Q1',
		'Q2' => 'Q2',
		'Q3' => 'Q3',
		'Q4' => 'Q4',
	];

	const STATUS_OKR_LIST = [
		'draft' => 'Draft',
		'submit' => 'Submit',
		'approved' => 'Approved',
		'rejected' => 'Rejected',
	];

	const LEVEL_OKR_LIST = [
		'corporate' => 'Corporate',
		'directorate' => 'Directorate',
		'svp_evp' => 'Senior atau Executive Vice President',
		'vp_under_sevp' => 'Vice President under SVP/EVP',
		'vp_under_directorate' => 'Vice President under Directorate',
		'gm' => 'General Manager',

		'compiler' => 'Corporate Strategy, TMO',
	];

	const LEVEL_EXPLORE_LIST = [
		'corporate' => 'Corporate',
		'directorate' => 'Directorate',
		'group'	=> 'Group',
		'gm' => 'Division'
	];

	const AREA_LIST = [
		'AREA 1' => 'AREA 1',
		'AREA 2' => 'AREA 2',
		'AREA 3' => 'AREA 3',
		'AREA 4' => 'AREA 4',
		'HEAD OFFICE' => 'HEAD OFFICE',
	];
	
	const REGION_LIST = [
		'SUMBAGUT SALES CS' => 'SUMBAGUT',
		'SUMBAGSEL SALES CS' => 'SUMBAGSEL',
		'SUMBAGTENG SALES CS' => 'SUMBAGTENG',
		'JABOTABEK SALES CS' => 'JABOTABEK',
		'JABAR SALES CS' => 'JABAR',
		'JATENG SALES CS' => 'JATENG',
		'JATIM SALES CS' => 'JATIM',
		'BALINUSRA SALES CS' => 'BALINUSRA',
		'KALIMANTAN SALES CS' => 'KALIMANTAN',
		'SULMALIRJA SALES CS' => 'SULMALIRJA',
		'PUMA SALES CS' => 'PUMA',
		'HEAD OFFICE' => 'HEAD OFFICE',
	];

	const DIRECTORATE_LIST = [
		"CEO'S Office Directorate" => "CEO'S Office Directorate",
		"Finance Directorate" => "Finance Directorate",
		"Human Capital Management Directorate" => "Human Capital Management Directorate",
		"Information Technology Directorate" => "Information Technology Directorate",
		"Marketing Directorate" => "Marketing Directorate",
		"Network Directorate" => "Network Directorate",
		"Planning and Transformation Directorate" => "Planning and Transformation Directorate",
		"Sales Directorate" => "Sales Directorate"
	];

	public static function replace_name($name,$replace)
	{
		$res = '';
		if($name!=''){
			$ex = explode(',',$name);
			if(($key=array_search($replace, $ex))!==false){
				unset($ex[$key]);
			}
			$res = implode($ex);
		}
		return $res;
	}
}

?>
<?php

namespace frontend\controllers;

use app\models\McuHeader;
use app\models\MDisease;
use app\models\ProfilingDiseaseHistory;
use app\models\ProfilingHistory;
use backend\components\RestHelper;
use backend\models\UploadForm;
use frontend\models\Employee;
use frontend\models\Profiling;
use frontend\models\ProfilingDisease;
use Yii;
use yii\web\Controller;


class ProfilingController extends Controller
{
    protected $title = "Profiling";

    private $success = 'Success';
    private $failed = 'Failed';

    public function beforeAction($action)
    {
        if(parent::beforeAction($action)){
            if (!Yii::$app->session->get('isLoggedin')){

                Yii::$app->session->setFlash('error', "Error, please sign in to start your session.");
                
                $this->redirect(['login/index']);
                return false;
            }
            
            return true;
            
        }
    }

    public function actionIndex()
    {
        Yii::$app->view->title = $this->title;
       return $this->render('index');
    }
    
    public function actionCreate()
    {
        Yii::$app->view->title = $this->title;
        $data['act'] = "add";
        $list_disease = [];
        $profil = new Profiling();
        $disease_h =  $profil->getDiseaseHeader();
        foreach($disease_h as $key=>$val){
            
            $list_disease[$val['type']] = $profil->getDiseaseDetail(["type"=>$val["type"]]);
        }
           
        $data["list_disease"] = $list_disease; 
        return $this->render('create', $data);
    } 
    
    public function actionGetemployee()
    {
     
        // return $this->render('get_employee'); //get hanya view doank
        return $this->renderAjax('get_employee');
    }

    public function actionSave()
    {

        $input = Yii::$app->request->post();       
        $act = $input['act'];
        $id = $input['id'];
        $employee = $input['employee'];       
        // $diagnosa = implode(",", $input['medical_diagnosis']);
        $diagnosa = null;
        $notes = $input['notes'];
        $nik_user = Yii::$app->session->get('nik');
        $datetime = date('Y-m-d H:i:s');
        $connection = Profiling::getDb();
        $transaction = $connection->beginTransaction();
        
        try{    

            if(strtolower($act)=="add"){
                $cek = Profiling::findOne(["nik" => $employee]);
                if($cek){
                    $result = ['status' => $this->failed, 'message' => "Employee data already exists"];
                    RestHelper::send_response(200, $result ); 
                    exit;
                }
            }

            $profil = new Profiling();
            if(strtolower($act)=="add"){
                $profil->created_by = $nik_user;
                $profil->created_date = $datetime;
                $profil->created_ip = $_SERVER['REMOTE_ADDR'];
                $msg = "Successfully create data";
            } else { //update
                $profil = Profiling::findOne(["profiling_id" => $id]);
                $msg = "Successfully update data";

                //add to history    
                $this->saveHistory($employee);                  
            }
            $profil->nik = $employee;
            $profil->diagnosa = $diagnosa;
            $profil->notes = $notes;           
            $profil->updated_by = $nik_user;
            $profil->updated_date = $datetime;
            $profil->updated_ip = $_SERVER['REMOTE_ADDR'];
          
            if (isset($_FILES['file_profiling']['name']) && $_FILES['file_profiling']['name']) {
                $model = new UploadForm();
                $model->file = \yii\web\UploadedFile::getInstanceByName('file_profiling');
             
                if ($model->validate()) {                   
                    $path_dir = "profiling";
                    $upload = $model->doupload($path_dir);
                    if ($upload !== false) {
                        
                        // file gak di hapus karena buat history
                        //  if($profil['attachment']){ //hapus file sebelumnya  
                        //      $this->deleteFile($profil['attachment']);
                        //  }
                        
                        $profil->attachment = $upload['file_name'];

                        
                    }
                }
            }
           
            $profil->save();

            $prof_dis = new ProfilingDisease();
            $prof_dis->deleteAll(["nik"=>$input['employee']]);

            if($input["disease"]){
                foreach($input["disease"] as $key=>$val){
                    $dis_d = new ProfilingDisease();
                    $dis_d->nik = $input['employee'];
                    $dis_d->disease_id = $val;
                    if($val=="000"){
                        $dis_d->other = $input["other_text"];
                    }
                    $dis_d->save();
                }
            }
           
            $transaction->commit();
            $res_status = $this->success;
            $message = $msg;

        } catch (\Exception $e) {
            $transaction->rollBack();
            $res_status = $this->failed;
            if(YII_DEBUG){
                $message = $e->getMessage();
            } else{
                $message = "Failed to process data";
            }
        }
        $result = ['status' => $res_status, 'message' => $message];

        RestHelper::send_response(200, $result ); 
    }

    function saveHistory($nik) 
    {
      try{
        if($nik){
            $nik_user = Yii::$app->session->get('nik');
            $datetime = date('Y-m-d H:i:s');
            $prof = Profiling::findOne(["nik"=>$nik]);           
            $prof_his = New ProfilingHistory();    
            $prof_his->nik =  $prof["nik"];
            $prof_his->notes = $prof["notes"] ;
            $prof_his->created_by =  $prof["created_by"] ;
            $prof_his->created_date = $prof["created_date"] ;
            $prof_his->attachment = $prof["attachment"];

            $prof_his->created_by_insert =  $nik_user ;
            $prof_his->create_date_insert = $datetime ;       
            if($prof_his->save(false)) {
                $id_h = $prof_his->profiling_id_h ;
            }

            $detail = ProfilingDisease::findAll(["nik"=>$nik]);
            if($detail){
                foreach($detail as $key=>$val){
                    $dis_d = new ProfilingDiseaseHistory();
                    $dis_d->profiling_id_h = $id_h;
                    $dis_d->nik = $val["nik"];
                    $dis_d->disease_id = $val["disease_id"];
                    $name_dis = MDisease::findOne(["disease_id"=>$val["disease_id"]]);
                    $dis_d->name_disease = (isset($name_dis["name"])) ? $name_dis["name"] : null;
                    $dis_d->other = $val["other"];                
                    $dis_d->save(false);
                }
            }
            return true;
       } else {
           return false;
       }

      }catch (\Exception $e){
        if(YII_DEBUG){
            $message = $e->getMessage();
        } else{
            $message = "Failed to process data";
        }       
        return $message;
      }
    }

    public function actionList()
    {        

        $input = Yii::$app->request->post();

        $result = array("data" => array(), "draw" => 0, "recordsFiltered" => 0, "recordsTotal" => 0 );
        $rawLength      = $input["length"];
        $start          = $input["start"];
        $search         = $input["search"];
        $order          = $input["order"];
        $columns        = $input["columns"];

        $order_clm      = $columns[$order[0]["column"]]["data"];
        $order_clm      = ($order_clm=="profiling_id") ? "updated_date" : $order_clm; //set default updated_date
        $order_dir      = $order[0]["dir"];
        $order_dir      =  ($order_clm=="updated_date") ? "desc" : $order_dir; //set default desc jika order_clm updated_date
        $draw           = $input["draw"];
        $ofsite         = $rawLength;
       
        $formfilter = (isset($input["_formdata"]) && $input["_formdata"]) ? $input["_formdata"] : [];
        $data = Profiling::get_profiling_datatable( $order_clm, $order_dir, $start, $ofsite, $search["value"], $formfilter );

        $no = $start+1;
        foreach($data["rows"] as $keyVal => $value){
            $dataRaw = array();
            foreach($columns as $k => $rawCol){
                $dataRaw[$rawCol["data"]] = "";

                if(array_key_exists($rawCol["data"], $value)){
                    $row_data = $value[$rawCol["data"]];
                    if($rawCol["data"] != "no"){
                        $dataRaw[$rawCol["data"]] = $row_data;
                    }
                }
            }

            $dataRaw["no"]      = $no;
            $dataRaw["profiling_id"] = $value['profiling_id'];

            $dataRaw["profiling_group"] = $this->getProfilingDisease($value['nik']);

            array_push($result["data"], $dataRaw);
            $no++;
        }

        $result["draw"]             =  $draw;
        $result["recordsTotal"]     =  $data["num_rows"];
        $result["recordsFiltered"]  =  $data["num_rows"];

        RestHelper::send_response(200, $result);

    }

     public function actionListemployee()
    {        

        $input = Yii::$app->request->get();

        $result = array("data" => array(), "draw" => 0, "recordsFiltered" => 0, "recordsTotal" => 0 );
        $rawLength      = $input["length"];
        $start          = $input["start"];
        $search         = $input["search"];
        $order          = $input["order"];
        $columns        = $input["columns"];

        $order_clm      = $columns[$order[0]["column"]]["data"];
        $order_dir      = $order[0]["dir"];
        $draw           = $input["draw"];
        $ofsite         = $rawLength;

        $data = Profiling::get_employee_datatable( $order_clm, $order_dir, $start, $ofsite, $search["value"] );

        $no = $start+1;
        foreach($data["rows"] as $keyVal => $value){
            $dataRaw = array();
            foreach($columns as $k => $rawCol){
                $dataRaw[$rawCol["data"]] = "";

                if(array_key_exists($rawCol["data"], $value)){
                    $row_data = $value[$rawCol["data"]];
                    if($rawCol["data"] != "no"){
                        $dataRaw[$rawCol["data"]] = $row_data;
                    }
                }
            }
           
            $dataRaw["no"]      = $no;
            $dataRaw["person_id"] = $value['person_id'];
            array_push($result["data"], $dataRaw);
            $no++;
        }

        $result["draw"]             =  $draw;
        $result["recordsTotal"]     =  $data["num_rows"];
        $result["recordsFiltered"]  =  $data["num_rows"];

        RestHelper::send_response(200, $result);

    }

    public function actionView()
    {        
        Yii::$app->view->title = $this->title;
        $input = Yii::$app->request->get();

        $id = $input['id'];

        $profil = Profiling::findOne(['profiling_id' => $id]);
        $data["profil"] = $profil;
        $data["emp"] = Employee::findOne(['nik' => $profil['nik']]);

        // $prof = new Profiling();
        // $dis_xsist = $prof->getDiseaseXsist($profil['nik']);
        // $disease ="";
        // if($dis_xsist){
        //     foreach($dis_xsist as $key=>$val){
        //         $val["name"] = ($val["name"]) ? $val["name"] : $val["other"] ;
        //         if($key==0){
        //             $disease .= $val["name"];
        //         } else {
        //             $disease .= ", ".$val["name"];
        //         }
        //     }
        // }
        // $data['disease']= $disease;

        //start get disease by group====================================
        $list_disease = [];
        $prof = new Profiling();
        $disease_h =  $prof->getDiseaseHeaderXsist($profil['nik']);
        // $where = ['a.nik' => $profil['nik']];
        foreach($disease_h as $key=>$val){            
            $where = ["a.nik" => $profil['nik'],"type"=>$val['type']];       
            $list_disease[$val['type']] = $prof->getDiseaseXsist($where);
        }
        
        $data["list_disease"] = $list_disease; 
       
        $dis_detail = ProfilingDisease::findAll(['nik' => $profil['nik']]);
        $dis_xsist = [];      
        foreach($dis_detail as $key=>$val){
            $dis_xsist[] = $val['disease_id'];           
        }
        $data["dis_xsist"]= $dis_xsist;

        $data['dis_other'] = ProfilingDisease::findOne(['nik' => $profil['nik'], 'disease_id'=>0]);
        //end get disease by group====================================

        return $this->render('view', $data);

    }

    public function actionEdit()
    {        
        Yii::$app->view->title = $this->title;
        $input = Yii::$app->request->get();

        $list_disease = [];
        $profil = new Profiling();
        $disease_h =  $profil->getDiseaseHeader();
        foreach($disease_h as $key=>$val){
            
            $list_disease[$val['type']] = $profil->getDiseaseDetail(["type"=>$val["type"]]);
        }
           
        $data["list_disease"] = $list_disease; 

        $data['act'] = "update";
        $data["id"] = $input['id'];
        $data["data"] = Profiling::findOne(['profiling_id' => $data["id"]]);
        $data["emp"] = Employee::findOne(['nik' => $data["data"]['nik']]);
        $dis_detail = ProfilingDisease::findAll(['nik' => $data["data"]['nik']]);
        $dis_xsist = [];
        foreach($dis_detail as $key=>$val){
            $dis_xsist[] = $val['disease_id'];
        }
        $data["dis_xsist"]= $dis_xsist;

        $data['dis_other'] = ProfilingDisease::findOne(['nik' => $data["data"]['nik'], 'disease_id'=>0]);
       
        return $this->render('create', $data);

    }

    public function actionGetdiagnosa()
    {
        $get = Yii::$app->request->post();
        $id = (isset($get["id"]) && $get["id"]) ? $get["id"] : "";
        $status = $this->failed ;
        $message = "Failed";
        $data = [];
        if($id){
            $profil = Profiling::findOne( ["profiling_id" => $id]);     
            $data = explode(",",$profil["diagnosa"]);
            $status = $this->success;  
            $message = "Success get data";   
        } else {
            $message = "ID Kosong";
        }
       

        $result = ['status' => $status, 'message' => $message, "data" => $data];

        RestHelper::send_response(200, $result);
    }

    public function actionDelete()
    {
        $get = Yii::$app->request->post();

        $profil = Profiling::findOne( ["profiling_id" => $get["profiling_id"]]);
    
        // if($profil['attachment']){  // gak dihapus karena buat histori
        //     $this->deleteFile($profil['attachment']);
        // }

        //add to history   
        $this->saveHistory($profil["nik"]);         

        $profil->delete();

        $result = ['status' => $this->success, 'message' => 'success delete record'];

        RestHelper::send_response(200, $result);
    }
    
    public function deleteFile($file)
    {
        if($file){            
            $path_file = Yii::getAlias('@webroot')."/uploads/profiling/".$file;          
            if(file_exists($path_file)) unlink($path_file);
            return true;
        } else {
            return false;
        }
    }

    public function actionDownload($file) 
    { 
        $path = Yii::getAlias('@webroot')."/uploads/profiling/".$file;
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path, null);
        }
    }

    public function getProfilingDisease($nik)
    {
        $list_disease = [];
        $prof = new Profiling();
        $disease_h =  $prof->getDiseaseHeaderXsist($nik);
        $xsist_other = false;
        $no = 0;
        foreach($disease_h as $key=>$val){            
            // tmbahin where type
            if($val['type']){
                $type = $val['type'];
                $where = ["a.nik" => $nik,"type"=>$val['type']];               
                $list_disease[$no]["type"] = $type;
                $list_disease[$no]["type_view"] = ucwords(str_replace("_", " ", $type));
                $list_disease[$no]["detail"] = $prof->getDiseaseXsist($where);
                $no++;
            } else {               
                $xsist_other = true;               
            }
          
        }
        if($xsist_other){
            $type = "other";
            $where = ["a.nik" => $nik,"a.disease_id"=>0]; // get disease other         
            $list_disease[$no]["type"] = $type;
            $list_disease[$no]["type_view"] = ucwords(str_replace("_", " ", $type));
            $list_disease[$no]["detail"] = $prof->getDiseaseXsist($where);
        }
      
        return $list_disease;
    }

    //start MCU====================================================================================================================================

    public function actionMcu()
    {
        Yii::$app->view->title = $this->title;
       return $this->render('mcu_index');
    }

    public function actionMcu_list()
    {        

        $input = Yii::$app->request->post();

        $result = array("data" => array(), "draw" => 0, "recordsFiltered" => 0, "recordsTotal" => 0 );
        $rawLength      = $input["length"];
        $start          = $input["start"];
        $search         = $input["search"];
        $order          = $input["order"];
        $columns        = $input["columns"];        
        $order_clm      = $columns[$order[0]["column"]]["data"];
        $order_clm      = ($order_clm=="mh_id") ? "created_date" : $order_clm; //set default created_date
        $order_dir      = $order[0]["dir"];
        $order_dir      =  ($order_clm=="created_date") ? "desc" : $order_dir; //set default desc jika order_clm created_date
        $draw           = $input["draw"];
        $ofsite         = $rawLength;

        $formfilter = (isset($input["_formdata"]) && $input["_formdata"]) ? $input["_formdata"] : [];
        $data = Profiling::get_mcu_header_datatable( $order_clm, $order_dir, $start, $ofsite, $search["value"], $formfilter );

        $no = $start+1;
        foreach($data["rows"] as $keyVal => $value){
            $dataRaw = array();
            foreach($columns as $k => $rawCol){
                $dataRaw[$rawCol["data"]] = "";

                if(array_key_exists($rawCol["data"], $value)){
                    $row_data = $value[$rawCol["data"]];
                    if($rawCol["data"] != "no"){
                        $dataRaw[$rawCol["data"]] = $row_data;
                    }
                }
            }

            $dataRaw["no"]      = $no;
            $dataRaw["mh_id"]      = $value["mh_id"];
            $dataRaw["created_date"] = ($value['created_date']) ? date("d-m-Y H:i:s", strtotime($value['created_date'])) : "";
            array_push($result["data"], $dataRaw);
            $no++;
        }

        $result["draw"]             =  $draw;
        $result["recordsTotal"]     =  $data["num_rows"];
        $result["recordsFiltered"]  =  $data["num_rows"];

        RestHelper::send_response(200, $result);

    }

    public function actionMcu_upload()
    {
        Yii::$app->view->title = $this->title;
        $data['act'] = "add";
      
        return $this->render('mcu_upload', $data);
    } 


    public function actionMcusave()
    {

        $input = Yii::$app->request->post();       
        $act = $input['act'];
        $id = $input['id'];       
        $nik_user = Yii::$app->session->get('nik');
        $datetime = date('Y-m-d H:i:s');
        $message_error = "";
        $connection = Profiling::getDb();
        $transaction = $connection->beginTransaction();
        
        try{    

            
            $mcu_h = new McuHeader();
            if(strtolower($act)=="add"){
                $mcu_h->created_by = $nik_user;
                $mcu_h->created_date = $datetime;
                $mcu_h->created_ip = $_SERVER['REMOTE_ADDR'];
                $msg = "Successfully upload data";
            } else { //update
                $mcu_h = McuHeader::findOne(["mh_id" => $id]);
                $msg = "Successfully upload data";
            }
            $mcu_h->description = $input["description"];    
                  
            $path_file = "";
            if (isset($_FILES['file_mcu']['name']) && $_FILES['file_mcu']['name']) {
                $model = new UploadForm();
                $model->file = \yii\web\UploadedFile::getInstanceByName('file_mcu');
             
                if ($model->validate()) {                   
                    $path_dir = "mcu";
                    $upload = $model->doupload_mcu($path_dir);
                    if ($upload !== false) {
                        
                        // file gak di hapus karena buat history
                        //  if($profil['attachment']){ //hapus file sebelumnya  
                        //      $this->deleteFile($profil['attachment']);
                        //  }
                        
                        $mcu_h->filname_random = $upload['file_name'];
                        $mcu_h->filename_ori = $_FILES['file_mcu']['name'];
                        // $path_file = $upload['path'];
                        
                    } else {
                        $result = ['status' => $this->failed, 'message' => "File not allowed"];
                        RestHelper::send_response(200, $result ); 
                        exit;
                    }
                }
            } else {
                $result = ['status' => $this->failed, 'message' => "File not selected"];
                RestHelper::send_response(200, $result ); 
                exit;
            }
                       
            if($mcu_h->save()) {
                $mh_id = $mcu_h->mh_id ;
            }
            
    
            //start import==================================================================
                           
            $col = New Profiling();
            $getCol = $col->getColumnImport();
            $mCol = [];
            foreach($getCol as $key=>$val){
                $mCol[$val["code"]] = $val["column"];
            }
            
            $path_file = Yii::getAlias('@webroot')."/uploads/".$path_dir."/".$upload['file_name'];
            
            $file = $path_file;
            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
            $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);
                    
           
                $num=$objPHPExcel->getSheetCount();//get number of sheets
                $sheetnames=$objPHPExcel->getSheetNames();//get sheet names
                for($i = 0 ; $i < $num ; $i++){
                    $sheet = $objPHPExcel->getSheet($i);
                    $highestRow = $sheet->getHighestRow();
                    $highestColumn = $sheet->getHighestColumn();
                          
                    for($row=8; $row<=$highestRow; ++$row){
                        $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
                       
                        $emptyRow = true;
                        foreach($rowData as $key => $val){                           
                            if($val){
                                $emptyRow = false;
                                if($val[2]){ // cek kalau kolom nama ada, maka eksekusi
                                    
                                    $data_d = [];                                   
                                    $data_d["mh_id"] = $mh_id;
                                    foreach($val as $k=>$v){ //save ke DB
                                        if($k>0){ // lngsng ngambil kolom k 1 (no lab)
                                        
                                            if(isset($mCol[$k])){
                                                $data_d[$mCol[$k]] = str_replace(",",".",$v);
                                            }
                                                                                   
                                        }
                                        
                                    }
                                   
                                    if($data_d){
                                        $col->saveMcuDetail($data_d);
                                    }

                                }
                            }
                        }
                        if($emptyRow){
                            break;
                        }
                        
                        
                    }
                }
                       
            //end import====================================================================

            $transaction->commit();
            $res_status = $this->success;
            $message = $msg;

        } catch (\Exception $e) {
            $transaction->rollBack();
            $res_status = $this->failed;
            $message = "Failed to process data";
            if(YII_DEBUG){
                $message_error = $e->getMessage();
            } 
            $message = "Failed to process data";           
        }
        $result = ['status' => $res_status, 'message' => $message, 'message_error' => $message_error];
        RestHelper::send_response(200, $result ); 
    }

    public function actionMcu_view()
    {        
        Yii::$app->view->title = $this->title;
        $input = Yii::$app->request->get();

        $data["id"] = $input['id'];
        $data["mcu_h"] = McuHeader::findOne(["mh_id" => $data["id"]]);

        $col = New Profiling();
        $getCol = $col->getColumnImport();
        $mCol = [];
        foreach($getCol as $key=>$val){
            $mCol[$key]["data"] = $val["column"];
        }
       $data["mcol"]= $mCol;
      
        return $this->render('mcu_view', $data);

    }

    public function actionMcu_list_detail()
    {        

        $input = Yii::$app->request->post();

        $result = array("data" => array(), "draw" => 0, "recordsFiltered" => 0, "recordsTotal" => 0 );
        $rawLength      = $input["length"];
        $start          = $input["start"];
        $search         = $input["search"];
        $order          = $input["order"];
        $columns        = $input["columns"];

        $order_clm      = $columns[$order[0]["column"]]["data"];
        $order_dir      = $order[0]["dir"];
        $draw           = $input["draw"];
        $ofsite         = $rawLength;

        $id = (isset($input["_id"]) && $input["_id"]) ? $input["_id"] : [];
        $formfilter = (isset($input["_formdata"]) && $input["_formdata"]) ? $input["_formdata"] : [];
        $data = Profiling::get_mcu_detail_datatable( $order_clm, $order_dir, $start, $ofsite, $search["value"], $id, $formfilter );

        $no = $start+1;
        foreach($data["rows"] as $keyVal => $value){
            $dataRaw = array();
            foreach($columns as $k => $rawCol){
                $dataRaw[$rawCol["data"]] = "";

                if(array_key_exists($rawCol["data"], $value)){
                    $row_data = $value[$rawCol["data"]];
                    if($rawCol["data"] != "no"){
                        $dataRaw[$rawCol["data"]] = $row_data;
                    }
                }
            }

            $dataRaw["no"]      = $no;
            $dataRaw["md_id"] = $value['md_id'];
            array_push($result["data"], $dataRaw);
            $no++;
        }

        $result["draw"]             =  $draw;
        $result["recordsTotal"]     =  $data["num_rows"];
        $result["recordsFiltered"]  =  $data["num_rows"];
       
        RestHelper::send_response(200, $result);

    }

    public function actionTemplatedownload() 
    { 
        $path = Yii::getAlias('@webroot')."/REKAP ALL HASIL MCU (GABUNGAN) TELKOMSEL - sample.xls";
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path, null);
        }
    }

    //end MCU==========================================================================================================================================

}

<?php 

use yii\helpers\Url;
use yii\web\View;
use common\components\Helper;

?>

	<?php $this->registerCssFile('@web/css/custom.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/sweetalert/sweetalert.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/datatable/datatables.min.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/select2/dist/css/select2.min.css'); ?>

 
  <div class="content-header" style="padding-top:60px !important">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title"><?= $this->title ?></h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?= Url::base() . "/home/index" ?>"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item active" aria-current="page">List Profiling</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>

    <!-- Main content -->
    <section class="content p-20">
      <div class="container">
      <div class="row">
        <div class="col-12">

         <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">List Profiling</h3>
              <button id="btn-create-profiling" class="btn btn-success waves-effect waves-light pull-right w-150" title="Click to create"><i class="fa fa-plus"></i> Create</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

			<!-- <div class="row">
				<div class="col-lg-12">
					sasasas
				</div>
			</div> -->

			<!-- <form class="form-horizontal"> -->
			<form role="form" id="formFilterProfiling">
              <div class="box-body">	
				<div class="row">
					<div class="col-md-3 col-12">
					<div class="form-group">
						<label>NIK</label>
							<input type="text" class="form-control opt-filter" id="nik" name="nik" value="" placeholder="NIK">
					</div>				
					</div>
					
					<div class="col-md-3 col-12">
					<div class="form-group">
						<label>Name</label>
							<input type="text" class="form-control opt-filter" name="nama" placeholder="Name">
					</div>				 
					</div>
					
					<div class="col-md-3 col-12">
					<div class="form-group">
						<label>Title</label>
							<input type="text" class="form-control opt-filter" name="title" placeholder="Title">
					</div>				
					</div>

					<div class="col-md-3 col-12">
					<div class="form-group">
						<label>Organization</label>
							<input type="text" class="form-control opt-filter" name="organization" placeholder="Organization">
					</div>				
					</div>
					
					<div class="col-md-3 col-12">
					<div class="form-group">
						<label>Department</label>
							<input type="text" class="form-control opt-filter" name="department" placeholder="department">
					</div>				
					</div>	

					<div class="col-md-3 col-12">
					<div class="form-group">
						<label>Division</label>
							<input type="text" class="form-control opt-filter" name="division" placeholder="division">
					</div>				
					</div>

					<div class="col-md-3 col-12">
					<div class="form-group">
						<label>Area</label>
							<input type="text" class="form-control opt-filter" name="area" placeholder="area">
					</div>				
					</div>	
					
					<div class="col-md-3 col-12">
					<div class="form-group">
						<label>Job</label>
							<input type="text" class="form-control opt-filter" name="job" placeholder="job">
					</div>				
					</div>	
					
					<div class="col-md-3 col-12">
					<div class="form-group">
						<label>Job Category</label>
							<input type="text" class="form-control opt-filter" name="job_category" placeholder="Job Category">
					</div>				
					</div>	
					
					<div class="col-md-3 col-12">
						<div class="form-group">
							<label>Profiling</label>
								<input type="text" class="form-control opt-filter" name="profiling_disease" placeholder="Profiling">
						</div>				
					</div>		
							
			
			  	</div>

				<div class="row">
				<div class="col-md-3 col-12">
					<div class="form-group">
						<label></label>
						<button type="button" id="btn_search" class="btn btn-info btn-sm">Search</button>
						<button type="button" id="btn_export" class="btn btn-warning btn-sm">Export</button>
						<button type="button" id="btn_reset" class="btn btn-default btn-sm">Reset</button>
					</div>				
					</div>	
				</div>

              </div>
    
            </form>

							<div class="table-responsive">
							  <table id="table_profiling" class="datatables table-bordered table-striped table-hover display compact">
								<thead>
									<tr>
										<th>ID</th>
										<th>Actions</th>
										<th>NIK</th>
										<th>Name</th>
										<th>Profiling</th>
										<th>Title</th>
										<th>Organization</th>
										<th>Department</th>
										<th>Division</th>
										<th>Job</th>
										<th>Job Category</th>
										<th>Area</th>
										<th>Directorate</th>
										<th>Employee_category</th>
										<th>Email</th>									
										
									</tr>
								</thead>
							  </table>
							</div>
            </div>
            <!-- /.box-body -->
          </div>

          
        </div>
        <!-- /.col-->
      </div>
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->



<input type="hidden" name="base_url" id="base_url" value="<?= Url::base(true) ?>/">


  <!-- Datatables -->
	<?php $this->registerJsFile('@web/assets/vendor_components/datatable/datatables.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
	<!-- popper -->
	<?php $this->registerJsFile('@web/assets/vendor_components/popper/dist/popper.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Bootstrap 4.0-->
	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Sweet-Alert  -->
	<?php $this->registerJsFile('@web/assets/vendor_components/sweetalert/sweetalert.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
	<?php $this->registerJsFile('@web/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap-select/dist/js/bootstrap-select.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
    <?php $this->registerJsFile('@web/assets/vendor_components/select2/dist/js/select2.full.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<?php $this->registerJsFile('@web/js/profiling_index.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>


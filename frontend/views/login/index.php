<?php 
use yii\helpers\Url;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
?>

	<div class="auth-2-outer row align-items-center h-p100 m-0">
		<div class="auth-2" style="overflow-y: hidden !important;">
		  <div class="auth-logo font-size-40">
			<a href="index.html" class="text-white"><b style="color:#dd4b39">Profiling</b></a>
		  </div>
		  <!-- /.login-logo -->
		  <div class="auth-body">
			<p class="auth-msg">Sign in to start your session</p>
			
            <?php $form = ActiveForm::begin(['id' => 'login-form','action'=>Url::toRoute('login/signin')]); ?>
			  <div class="form-group has-feedback">
				<input type="text" class="form-control" placeholder="Username" name="username" required="required" value="<?= (isset($_COOKIE['loginId'])?$_COOKIE['loginId']:''); ?>">
				<span class="ion ion-email form-control-feedback"></span>
			  </div>
			  <div class="form-group has-feedback">
				<input type="password" class="form-control" placeholder="Password" name="password" required="required">
				<span class="ion ion-locked form-control-feedback"></span>
			  </div>
			  <div class="row">
				<div class="col-6">
				  <div class="checkbox">
					<input type="checkbox" id="basic_checkbox_1" name="rememberMe" value="1" <?= (isset($_COOKIE['isremember'])? 'checked' : '' ); ?>>
					<label for="basic_checkbox_1">Remember Me</label>
				  </div>
				</div>
				<!-- /.col -->
				<!-- <div class="col-6">
				 <div class="fog-pwd">
					<a href="javascript:void(0)" class="text-white"><i class="ion ion-locked"></i> Forgot pwd?</a><br>
				  </div>
				</div> -->
				<!-- /.col -->
				<?php if (Yii::$app->session->hasFlash('error')): ?>
				<div class="col-12 text-center" style="color: red;">
						<?= Yii::$app->session->getFlash('error') ?>
				</div>
				<?php endif; ?>
				<div class="col-12 text-center">
				  <button type="submit" class="btn btn-block mt-10 btn-success">SIGN IN</button>
				</div>
				<!-- /.col -->
			  </div>
            <?php ActiveForm::end(); ?>

			<div class="text-center text-white">
				  <img src="<?= Url::base()?>/images/logo/logo_telkomsel.png" style="
																    width: auto;
																    height: auto;
																    margin-top: 20%;		">
			</div>

		  </div>
		</div>
	
	</div>

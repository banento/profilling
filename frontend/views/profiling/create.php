<?php 

use abei2017\emoji\Emoji;
use yii\helpers\Url;
use yii\web\View;
use common\components\Helper;

$type_dis = (isset($act) && $act=="update") ? "Update" : "Create";

?>

	<!-- bootstrap wysihtml5 - text editor -->
	<?php $this->registerCssFile('@web/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css'); ?>

  <?php $this->registerCssFile('@web/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/select2/dist/css/select2.min.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/sweetalert/sweetalert.css'); ?>
	<?php $this->registerCssFile('@web/css/custom.css'); ?>



  <!-- Content Wrapper. Contains page content -->
  <!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->
   <div class="content-header" style="padding-top:60px !important">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="page-title"><?= $this->title ?></h3>
        <div class="d-inline-block align-items-center">
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?= Url::base() . "/home/index" ?>"><i class="mdi mdi-home-outline"></i></a></li>
              <li class="breadcrumb-item"><a href="<?= Url::base() . "/profiling" ?> ">List Profiling</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?= $type_dis ?> Profiling</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>  

    <!-- Main content -->
    <section class="content" style="padding-top:20px !important">
      <div class="row">
        <div class="col-12">          
          
            

          <div class="box">
            <div class="box-header">
              <h4 class="box-title"><?= $type_dis ?> Profiling<br>               
              </h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form id="profiling_form" role="form">
                <div class="form-group">
                  <label>Employee</label>
                  <div class="input-group">
                    <input type="text" class="form-control" id="name_employee" name="name_employee" value="<?= isset($emp['nama']) ? $emp['nama'] : "" ?>" placeholder="Search..." readonly >
                    <input type="hidden" id="employee_h" name="employee" value="<?= isset($data['nik']) ? $data['nik'] : "" ?>" >
                    <span class="input-group-btn">
                      <?php if($act=="add"){ ?>
                      <button type="button" class="btn btn-info dialog-modal" data-url="<?= Url::base() ?>/profiling/getemployee" data-title="List Employee"><i class="fa fa-search" aria-hidden="true"></i></button>
                      <?php } ?>
                    </span>
                  </div>
                  <input type="hidden" id="act" name="act" value="<?= $act ?>">
                  <input type="hidden" id="id" name="id" value="<?= isset($id) ? $id : "" ?>">       
                </div>
               
                <div class="form-group">
                  <label>File</label>
                  <div class="controls">
                    <input type="file" name="file_profiling" id="file_profiling" class="form-control" accept=".jpg,.png,.doc,.docx,.pdf"aria-invalid="false"> <div class="help-block"></div>
                  <?php
                    if(isset($data['attachment']) && $data['attachment']){ ?>
                    <a href="<?= "/profiling/download?file=".$data['attachment'] ?>" target="_blank"><?= $data['attachment'] ?></a>
                  <?php  }
                  ?>
                  <small>Size Max : 5mb, Type : doc, docx, pdf, jpg, jpeg, png</small>
                  </div>
                </div>
                   
                <div class="form-group">
                  <label>Notes</label>
                  <textarea id="notes" name="notes" class="form-control col-lg-12" placeholder="Place some text here" ><?= isset($data['notes']) ? $data['notes'] : "" ?></textarea>
                </div>

                <div class="form-group">
                  <label>Profiling</label>                                    
                  <?php
                    $no=1;
                    foreach($list_disease as $label=>$val){  ?>
                       <div class="col-lg-2"><strong><?= ucwords(str_replace("_"," ", $label)) ?></strong></div>
                       <hr>
                       <div class="col-lg-12">
                          <div class="demo-checkbox">

                       <?php foreach($val as $key=>$v){ 
                          $dis_arr = (isset($dis_xsist) && $dis_xsist) ? $dis_xsist : [];
                          $checked = (in_array($v['disease_id'],$dis_arr))? "checked" : "";
                        ?>
                          <input type="checkbox" id="<?= $label."_".$key ?>" class="filled-in chk-col-light-blue" <?= $checked ?> name="disease[]" value="<?= $v['disease_id'] ?>" />
                          <label for="<?= $label."_".$key ?>"><?= ucwords(str_replace("_"," ", $v['name'])) ?></label>		
                   <?php } ?>

                          </div>
                       </div>
                   <?php }
                    ?>

                  <?php
                   $dis_arr = (isset($dis_xsist) && $dis_xsist) ? $dis_xsist : [];
                   $checked = (in_array(0,$dis_arr))? "checked" : "";  
                   $style = ($checked)? "": "display: none;";                 
                  ?>
                    <div class="col-lg-2"></div>
                       <hr>
                       <div class="col-lg-12">
                          <div class="demo-checkbox">                      
                            <input type="checkbox" id="other" class="filled-in chk-col-light-blue" <?= $checked ?> name="disease[]" value="0" />
                            <label for="other">Other</label>		
                          </div>
                          <textarea id="other_text" name="other_text" class="form-control col-lg-4" style="<?= $style ?>" rows="2"><?= (isset($dis_other['other']) && $dis_other['other'])? $dis_other['other']: "" ?></textarea>
                       </div>                  
                 
                </div>
              <div class="pt-20">
                <button type="submit" class="btn btn-success btn-outline">
                  <i class="ti-save-alt"></i> Submit
                </button>
                <button type="reset" class="btn btn-warning btn-outline mr-1">
                  <i class="ti-trash"></i> Reset
                </button>
              </div>
              </form>
            </div>
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->



<input type="hidden" name="base_url" id="base_url" value="<?= Url::base(true) ?>/">


	<!-- popper -->
	<?php $this->registerJsFile('@web/assets/vendor_components/popper/dist/popper.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Bootstrap 4.0-->
	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Superieur Admin for demo purposes -->
	<?php $this->registerJsFile('@web/js/demo.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- CK Editor -->
	<?php //$this->registerJsFile('@web/assets/vendor_components/ckeditor/ckeditor.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Bootstrap WYSIHTML5 -->
	<?php $this->registerJsFile('@web/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Superieur Admin for editor -->
  
  <?php $this->registerJsFile('@web/assets/vendor_components/bootstrap-select/dist/js/bootstrap-select.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  <?php $this->registerJsFile('@web/assets/vendor_components/select2/dist/js/select2.full.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  <?php $this->registerJsFile('@web/assets/vendor_components/sweetalert/sweetalert.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  <?php $this->registerJsFile('@web/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
  
	<?php $this->registerJsFile('@web/js/profiling_create.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

  <script>
    // $(document).ready(()=>{
      // $(document).ready(function(){

      // $(".select2").select2();
      // $('.select2').select2();

    // })
  </script>

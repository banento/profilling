<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profiling_disease_history".
 *
 * @property int $pd_id_h
 * @property int|null $profiling_id_h
 * @property string|null $nik
 * @property int|null $disease_id
 * @property string|null $name_disease
 * @property string|null $other
 */
class ProfilingDiseaseHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profiling_disease_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pd_id_h'], 'required'],
            [['pd_id_h', 'profiling_id_h', 'disease_id'], 'integer'],
            [['other'], 'string'],
            [['nik'], 'string', 'max' => 20],
            [['name_disease'], 'string', 'max' => 255],
            [['pd_id_h'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pd_id_h' => 'Pd Id H',
            'profiling_id_h' => 'Profiling Id H',
            'nik' => 'Nik',
            'disease_id' => 'Disease ID',
            'name_disease' => 'Name Disease',
            'other' => 'Other',
        ];
    }
}

<?php

namespace frontend\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "profiling".
 *
 * @property int $profiling_id
 * @property string|null $nik
 * @property string|null $diagnosa
 * @property string|null $notes
 * @property string|null $attachment
 * @property string|null $created_by
 * @property string|null $created_date
 * @property string|null $created_ip
 * @property string|null $updated_by
 * @property string|null $updated_date
 * @property string|null $updated_ip
 * @property int|null $status 0 = non aktif, 1 = aktif
 */
class Profiling extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profiling';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notes', 'attachment'], 'string'],
            [['created_date', 'updated_date'], 'safe'],
            [['status'], 'integer'],
            [['nik', 'created_by', 'updated_by'], 'string', 'max' => 20],
            [['diagnosa'], 'string', 'max' => 100],
            [['created_ip', 'updated_ip'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'profiling_id' => 'Profiling ID',
            'nik' => 'Nik',
            'diagnosa' => 'Diagnosa',
            'notes' => 'Notes',
            'attachment' => 'Attachment',
            'created_by' => 'Created By',
            'created_date' => 'Created Date',
            'created_ip' => 'Created Ip',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
            'updated_ip' => 'Updated Ip',
            'status' => 'Status',
        ];
    }

    function get_profiling_datatable_01($order_clm, $order_dir, $start, $ofsite, $search, $formfilter=[]){

        $sql = "SELECT  * from v_profiling a where 1=1";

        if (!empty($search))
        {
            $search = strtolower($search);
            $sql .=" AND ( lower(a.nik) LIKE '%" . $search . "%' or lower(a.nama) LIKE '%" . $search . "%' or lower(a.profiling_disease) LIKE '%" . $search . "%') ";
        }

        if($formfilter) {
            $fil_where_start = " AND ( " ;
            $fil_where_end = " ) " ;
            $file_where_val = "";
            $no = 0;
            foreach($formfilter as $key=> $val){
                if($val["value"]){
                    if($no==0) {
                        $file_where_val .="  lower(".$val['name'].") LIKE '%" . strtolower($val["value"]) . "%' ";
                    } else {
                        $file_where_val .=" and lower(".$val['name'].") LIKE '%" . strtolower($val["value"]) . "%' ";
                    }
                    $no++;
                }
            }
            if($file_where_val){
                $sql .= $fil_where_start.$file_where_val.$fil_where_end;
            }
        }


        $data = Yii::$app->db->createCommand($sql)->queryAll();
        $totalFiltered = count($data);

        $sql .= " ORDER BY $order_clm $order_dir LIMIT " . $start . " ," . $ofsite. " ";

        $result = Yii::$app->db->createCommand($sql)->queryAll();

        return ['num_rows' => $totalFiltered, 'rows' => $result];

    }
    
    function get_profiling_datatable($order_clm, $order_dir, $start, $ofsite, $search, $formfilter=[]){

        /*
        $sql = "SELECT  * from v_profiling a where 1=1";
        $params = false;
        // if (!empty($search))
        // {
        //     $params = [":search" => $search];
        //     $search = strtolower($search);
        //     $sql .=" AND ( lower(a.nik) LIKE '%:search%' or lower(a.nama) LIKE '%:search%' or lower(a.profiling_disease) LIKE '%:search%') ";
        // }

        if($formfilter) {
            $fil_where_start = " AND ( " ;
            $fil_where_end = " ) " ;
            $file_where_val = "";
            $no = 0;
            foreach($formfilter as $key=> $val){
                if($val["value"]){
                    $params = [':value' => strtolower("%".$val["value"]."%")];
                    if($no==0) {
                        $file_where_val .="  lower(".$val['name'].") LIKE :value";
                    } else {
                        $file_where_val .=" and lower(".$val['name'].") LIKE :value";
                    }
                    $no++;
                }
            }
            if($file_where_val){
                $sql .= $fil_where_start.$file_where_val.$fil_where_end;
            }
        }


        $data = Yii::$app->db->createCommand($sql)->bindValues($params)->queryAll();
        $totalFiltered = count($data);

        $sql .= " ORDER BY $order_clm $order_dir LIMIT " . $start . " ," . $ofsite. " ";

        $result = Yii::$app->db->createCommand($sql)->queryAll();

        return ['num_rows' => $totalFiltered, 'rows' => $result];
        */

        $where = "1=1";
        $sql_where = "";
        if($formfilter) {
            $fil_where_start = " AND ( " ;
            $fil_where_end = " ) " ;
            $file_where_val = "";
            $no = 0;
            foreach($formfilter as $key=> $val){
                if($val["value"]){                   
                    if($no==0) {
                        $file_where_val .="  lower(".$val['name'].") LIKE '%" . strtolower($val["value"]) . "%' ";
                    } else {
                        $file_where_val .=" and lower(".$val['name'].") LIKE '%" . strtolower($val["value"]) . "%' ";
                    }
                    $no++;
                }
            }
            if($file_where_val){
                $sql_where = $fil_where_start.$file_where_val.$fil_where_end;
            }
        }
       
        if($sql_where){
            $where .= $sql_where;
        }

        $query = new Query();
        $data = $query->select("*")
                ->from('v_profiling')                
                ->where($where)               
                ->orderBy($order_clm." ".$order_dir)
                ->limit($ofsite)
                ->offset($start)
                ->all();

        $total =  (new Query())->select("count(*) as total")->from('v_profiling')->where($where)->one();
        $totalFiltered = (isset($total["total"]) && $total["total"]) ? $total["total"] : 0;
        $result = $data;
        return ['num_rows' => $totalFiltered, 'rows' => $result];

    }
    
    function get_employee_datatable($order_clm, $order_dir, $start, $ofsite, $search){
        
        $where = "1=1";
        if (!empty($search))
        {
            $search = strtolower($search);
            $where .=" AND ( lower(nik) LIKE '%" . $search . "%' or lower(nama) LIKE '%" . $search . "%') ";
        }
        $query = new Query();
        $data = $query->select("*")
                ->from('employee')                
                ->where($where)               
                ->orderBy($order_clm." ".$order_dir)
                ->limit($ofsite)
                ->offset($start)
                ->all();

        $result = $data;

        $total =  (new Query())->select("count(*) as total")->from('employee')->where($where)->one();
        $totalFiltered = (isset($total["total"]) && $total["total"]) ? $total["total"] : 0;

        return ['num_rows' => $totalFiltered, 'rows' => $result];

    }

    public function getDiseaseHeader($where_opt=[])
    {
        $query = new Query();
        $where = "1=1";
        if($where_opt){
            $where = $where_opt;
        }
        $rows = $query->select("type")
                ->from('m_disease')                
                ->where($where)
                ->groupBy("type")
                ->orderBy('disease_id ASC')
                ->all();
        return $rows ;
    }
    
    public function getDiseaseDetail($where_opt=[])
    {
        $query = new Query();
        $where = "1=1";
        if($where_opt){
            $where = $where_opt;
        }
        $rows = $query->select("*")
                ->from('m_disease')                
                ->where($where)
                ->orderBy('disease_id ASC')
                ->all();
        return $rows ;
    }

    public function getDiseaseXsist($where_set=[])
    {

        $query = new Query();
    
        $where_d = "1=1";
        if($where_set){
            $where_d = $where_set;
        }

        $rows = $query->select("a.disease_id,
                        a.nik,
                        COALESCE(a.other,b.`name`) as `name`, a.other")
                ->from('profiling_disease a')
                ->join('LEFT JOIN', 'm_disease b', 'a.disease_id = b.disease_id')               
                ->where($where_d)                
                ->orderBy('a.disease_id ASC')
                ->all();
        return $rows ;
    }

    public function getDiseaseHeaderXsist($nik="")
    {

        $query = new Query();
        $where = "1=1";
        if($nik){
            $where = ['a.nik' => $nik];
        }
        $rows = $query->select("b.type")
                ->from('profiling_disease a')
                ->join('LEFT JOIN', 'm_disease b', 'a.disease_id = b.disease_id')               
                ->where($where)
                ->groupBy('b.type')
                ->orderBy('a.disease_id ASC')
                ->all();
        return $rows ;
    }

    public function getData($where=[])
    {

        // $sql = "SELECT  * from v_profiling a where 1=1";
        // if($where) {
        //     $fil_where_start = " AND ( " ;
        //     $fil_where_end = " ) " ;
        //     $file_where_val = "";
        //     $no = 0;
        //     foreach($where as $key=> $val){
        //         if($val["value"]){
        //             if($no==0) {
        //                 $file_where_val .="  lower(".$val['name'].") LIKE '%" . strtolower($val["value"]) . "%' ";
        //             } else {
        //                 $file_where_val .=" and lower(".$val['name'].") LIKE '%" . strtolower($val["value"]) . "%' ";
        //             }
        //             $no++;
        //         }
        //     }
        //     if($file_where_val){
        //         $sql .= $fil_where_start.$file_where_val.$fil_where_end;
        //     }
        // }


        // $data = Yii::$app->db->createCommand($sql)->queryAll();

        $where_set = "1=1";
        $sql_where = "";

        if($where) {
            $fil_where_start = " AND ( " ;
            $fil_where_end = " ) " ;
            $file_where_val = "";
            $no = 0;
            foreach($where as $key=> $val){
                if($val["value"]){
                    if($no==0) {
                        $file_where_val .="  lower(".$val['name'].") LIKE '%" . strtolower($val["value"]) . "%' ";
                    } else {
                        $file_where_val .=" and lower(".$val['name'].") LIKE '%" . strtolower($val["value"]) . "%' ";
                    }
                    $no++;
                }
            }
            if($file_where_val){
                $sql_where = $fil_where_start.$file_where_val.$fil_where_end;
            }
        }

        if($sql_where){
            $where_set .= $sql_where;
        }
        $query = new Query();
        $data = $query->select("*")
                ->from('v_profiling')                
                ->where($where_set)   
                ->all();

        return $data ;
    }

    public function getColumnImport()
    {
        $query = new Query();
        $where = "1=1";       
        $rows = $query->select("*")
                ->from('m_column_import_mcu')                   
                ->where($where)               
                ->orderBy('code ASC')
                ->all();
        return $rows ;
    }

    public function saveMcuDetail($data, $id="")
    {
        $connection 		= $this->db;        
				
		$connection->createCommand()->insert('mcu_detail', $data)->execute();
        return true;
    }

    function get_mcu_header_datatable($order_clm, $order_dir, $start, $ofsite, $search, $formfilter=[]){
                  
        $where = "1=1";       
        if (!empty($search))
        {
            $search = strtolower($search);
            $where .=" AND ( lower(filename_ori) LIKE '%" . $search . "%' or lower(description) LIKE '%" . $search . "%' or lower(created_date) LIKE '%" . $search . "%') ";
        }

        $query = new Query();
        $data = $query->select("*")
                ->from('mcu_header')                
                ->where($where)               
                ->orderBy($order_clm." ".$order_dir)
                ->limit($ofsite)
                ->offset($start)
                ->all();

        $result = $data;

        $total =  (new Query())->select("count(*) as total")->from('mcu_header')->where($where)->one();
        $totalFiltered = (isset($total["total"]) && $total["total"]) ? $total["total"] : 0;

        return ['num_rows' => $totalFiltered, 'rows' => $result];

    }
    
    function get_mcu_detail_datatable($order_clm, $order_dir, $start, $ofsite, $search, $id=[], $formfilter=[]){

        // $sql = "SELECT  * from mcu_detail a where 1=1";

        // if($id){
        //     $sql .= " AND a.mh_id =". $id;
        // }

        // if (!empty($search))
        // {
        //     $search = strtolower($search);
        //     $sql .=" AND ( lower(a.no_lab) LIKE '%" . $search . "%' or lower(a.nama) LIKE '%" . $search . "%' or lower(a.nik) LIKE '%" . $search . "%') ";
        // }

        // if($formfilter) {
        //     $fil_where_start = " AND ( " ;
        //     $fil_where_end = " ) " ;
        //     $file_where_val = "";
        //     $no = 0;
        //     foreach($formfilter as $key=> $val){
        //         if($val["value"]){
        //             if($no==0) {
        //                 $file_where_val .="  lower(".$val['name'].") LIKE '%" . strtolower($val["value"]) . "%' ";
        //             } else {
        //                 $file_where_val .=" and lower(".$val['name'].") LIKE '%" . strtolower($val["value"]) . "%' ";
        //             }
        //             $no++;
        //         }
        //     }
        //     if($file_where_val){
        //         $sql .= $fil_where_start.$file_where_val.$fil_where_end;
        //     }
        // }


        // $data = Yii::$app->db->createCommand($sql)->queryAll();
        // $totalFiltered = count($data);

        // $sql .= " ORDER BY $order_clm $order_dir LIMIT " . $start . " ," . $ofsite. " ";

        // $result = Yii::$app->db->createCommand($sql)->queryAll();
        
        $where = "1=1";
         if($id){
            $where .= " AND mh_id =". $id;
        }
        if (!empty($search))
        {
            $search = strtolower($search);
            $where .=" AND ( lower(no_lab) LIKE '%" . $search . "%' or lower(nama) LIKE '%" . $search . "%' or lower(nik) LIKE '%" . $search . "%') ";
        }
        $query = new Query();
        $data = $query->select("*")
                ->from('mcu_detail')                
                ->where($where)               
                ->orderBy($order_clm." ".$order_dir)
                ->limit($ofsite)
                ->offset($start)
                ->all();

        $result = $data;

        $total =  (new Query())->select("count(*) as total")->from('mcu_detail')->where($where)->one();
        $totalFiltered = (isset($total["total"]) && $total["total"]) ? $total["total"] : 0;

        return ['num_rows' => $totalFiltered, 'rows' => $result];

    }

}

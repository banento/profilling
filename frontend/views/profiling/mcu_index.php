<?php 

use yii\helpers\Url;
use yii\web\View;
use common\components\Helper;

?>

	<?php $this->registerCssFile('@web/css/custom.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/sweetalert/sweetalert.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/datatable/datatables.min.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css'); ?>
	<?php $this->registerCssFile('@web/assets/vendor_components/select2/dist/css/select2.min.css'); ?>

  <div class="content-header" style="padding-top:60px !important">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title"><?= $this->title ?></h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?= Url::base() . "/home/index" ?>"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item active" aria-current="page">List MCU</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>

    <!-- Main content -->
    <section class="content p-20">
      <div class="container">
      <div class="row">
        <div class="col-12">

         <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">List Medical Check Up (MCU)</h3>
              <button id="btn-upload-mcu" class="btn btn-success waves-effect waves-light pull-right w-150" title="Click to create"><i class="fa fa-plus"></i> Upload</button>
            </div>
      
            <div class="box-body">		

				<div class="table-responsive">
					<table id="table_mcu" class="datatables table-bordered table-striped table-hover display compact" style="width: 100%;">
					<thead>
						<tr>
							<th>ID</th>										
							<th>File</th>
							<th>Description</th>										
							<th>Date Upload</th>										
							<th>Action</th>	
						</tr>
					</thead>
					</table>
				</div>
            </div>
            <!-- /.box-body -->
          </div>

          
        </div>
        <!-- /.col-->
      </div>
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->



<input type="hidden" name="base_url" id="base_url" value="<?= Url::base(true) ?>/">


  <!-- Datatables -->
	<?php $this->registerJsFile('@web/assets/vendor_components/datatable/datatables.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
	<!-- popper -->
	<?php $this->registerJsFile('@web/assets/vendor_components/popper/dist/popper.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Bootstrap 4.0-->
	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<!-- Sweet-Alert  -->
	<?php $this->registerJsFile('@web/assets/vendor_components/sweetalert/sweetalert.min.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
	<?php $this->registerJsFile('@web/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<?php $this->registerJsFile('@web/assets/vendor_components/bootstrap-select/dist/js/bootstrap-select.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>
    <?php $this->registerJsFile('@web/assets/vendor_components/select2/dist/js/select2.full.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

	<?php $this->registerJsFile('@web/js/mcu_index.js',['depends' => [\yii\web\JqueryAsset::class]]); ?>

